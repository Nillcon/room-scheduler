import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInLayoutComponent } from '@Layouts/sign-in-layout/sign-in-layout.component';
import { MainLayoutComponent } from '@Layouts/main-layout/main-layout.component';
import { NotFoundLayoutComponent } from '@Layouts/not-found-layout/not-found-layout.component';
import { VisitorLayoutComponent } from '@Layouts/visitor-layout/visitor-layout.component';
import { AppPagesEnum } from './app-pages';

const routes: Routes = [
    {
        path: AppPagesEnum.Index,
        component: MainLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: '@Layouts/main-layout/main-layout.module#MainLayoutModule'
            },
        ]
    },
    {
        path: AppPagesEnum.Visitor,
        component: VisitorLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: '@Layouts/visitor-layout/visitor-layout.module#VisitorLayoutModule'
            }
        ]
    },
    {
        path: AppPagesEnum.SignIn,
        component: SignInLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: '@Layouts/sign-in-layout/sign-in-layout.module#SignInLayoutModule'
            }
        ]
    },
    {
        path: AppPagesEnum.NotFound,
        component: NotFoundLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: '@Layouts/not-found-layout/not-found-layout.module#NotFoundLayoutModule'
            }
        ]
    },
    {
        path: '',
        redirectTo: AppPagesEnum.Index,
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: '404',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
