import { Component, OnInit } from '@angular/core';
import { IMenuItem } from '@Layouts/visitor-layout/interfaces';
import { VisitorMenuItemsArray } from '@Layouts/visitor-layout/shared/visitor-menu-items.array';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { AppPagesEnum } from '@App/app-pages';

@Component({
  selector: 'side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

    public VisitorMenuItemsArray: IMenuItem[] = VisitorMenuItemsArray;

    constructor (
        private navigateService: NavigationService
    ) { }

    public ngOnInit (): void {
    }

    public logIn (): void {
        this.navigateService.navigate(AppPagesEnum.SignIn);
    }

}
