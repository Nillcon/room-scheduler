import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarPageComponent } from './calendar-page.component';
import { CalendarLayoutRoutingModule } from './calendar-page.routing';

import { EventModule } from '@App/Features/event';
import { VisitorCalendarComponent } from './components/visitor-calendar/visitor-calendar.component';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';

@NgModule({
    declarations: [
        CalendarPageComponent,
        VisitorCalendarComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FeaturesModule,

        CalendarLayoutRoutingModule,

        EventModule
    ]
})
export class CalendarPageModule { }
