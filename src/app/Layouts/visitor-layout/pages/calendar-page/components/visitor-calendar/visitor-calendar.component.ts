import { Component, OnInit } from '@angular/core';
import { IEvent, EventService } from '@App/Features/event';
import { IRoom } from '@App/Features/room';
import { DateService } from '@Core/root/date/date.service';
import { tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';
import { RoomService } from '@App/Features/room/room.service';

@Component({
  selector: 'app-visitor-calendar',
  templateUrl: './visitor-calendar.component.html',
  styleUrls: ['./visitor-calendar.component.scss']
})
export class VisitorCalendarComponent implements OnInit {

    public events: IEvent[];
    public rooms: IRoom[];

    public currentDate: Date;

    public fromDate: Date;
    public toDate: Date;

    constructor (
        private eventService: EventService,
        private roomService: RoomService,
        private dateService: DateService
    ) { }

    public ngOnInit (): void {
        this.updateAllData()
            .subscribe();
    }

    public updateAllData (): Observable<any> {
        this.initCurrentDateRange();

        return this.updateEvents(this.fromDate, this.toDate)
            .pipe(
                switchMap(() => this.updateRooms()),
            );
    }

    public updateEvents (from?: Date, to?: Date): Observable<IEvent[]> {
        return this.eventService.getByFilters(from, to)
            .pipe(
                tap(events =>  this.events = events)
            );
    }

    public updateRooms (): Observable<IRoom[]> {
        return this.roomService.getAll()
            .pipe(
                tap(rooms => {
                    this.rooms = rooms;
                }),
            );
    }

    public onDateRangeChanged (event: IDateRange): void {
        if (event.from.toDateString() === this.fromDate.toDateString()) {
            if (event.to.toDateString() === this.toDate.toDateString()) {
                return;
            }
        }

        this.fromDate = event.from;
        this.toDate = event.to;

        this.updateEvents(event.from, event.to)
            .subscribe();
    }

    private initCurrentDateRange (): void {
        this.fromDate = this.dateService.getDateWithoutTime(new Date());

        this.toDate = this.fromDate;
    }

}
