import { IMenuItem } from '../interfaces';
import { VisitorLayoutPagesEnum } from '../visitor-layout-pages';

export const VisitorMenuItemsArray: IMenuItem[] = [
    {
        name: 'Calendar',
        iconClass: 'fas fa-calendar-alt',
        link: VisitorLayoutPagesEnum.Index
    }
];
