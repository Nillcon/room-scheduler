import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisitorLayoutComponent } from './visitor-layout.component';
import { VisitorLayoutRoutingModule } from './visitor-layout.routing';

import { MatSidenavModule } from '@angular/material';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { SharedModule } from '@App/app-shared.module';
import { VisitorLayoutGuard } from './guards/visitor-layout.guard';

@NgModule({
    declarations: [
        VisitorLayoutComponent,
        SideMenuComponent
    ],
    imports: [
        CommonModule,
        SharedModule,

        VisitorLayoutRoutingModule,

        MatSidenavModule
    ],
    providers: [
        VisitorLayoutGuard
    ]
})
export class VisitorLayoutModule { }
