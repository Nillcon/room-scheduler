import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisitorLayoutPagesEnum } from './visitor-layout-pages';
import { VisitorLayoutGuard } from './guards/visitor-layout.guard';

export const routes: Routes = [
    {
        path: VisitorLayoutPagesEnum.Index,
        loadChildren: '@Layouts/visitor-layout/pages/calendar-page/calendar-page.module#CalendarPageModule',
        canActivate: [
            VisitorLayoutGuard
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class VisitorLayoutRoutingModule {}
