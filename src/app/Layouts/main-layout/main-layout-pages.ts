export enum MainLayoutPagesEnum {
    Main = 'Calendar',
    Users = 'Users',
    Equipment = 'Equipment',
    Events = 'Events',
    Rooms = 'Rooms',
    EmailTemplates = 'EmailTemplates',
    Services = 'Services',
    Reports = 'Reports',
    Logs = 'Logs',
    Invoices = 'Invoices'
}
