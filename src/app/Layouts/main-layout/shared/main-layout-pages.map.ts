import { IPage } from '@Core/root/navigation/interfaces/page.interface';
import { MainLayoutPagesEnum as PagesEnum } from '../main-layout-pages';
import { RoleEnum } from '@App/Features/role/shared/role.enum';

export const MainLayoutPagesMap = new Map<PagesEnum, IPage>()
    .set(
        PagesEnum.Main,
        {
            id: PagesEnum.Main,
            name: 'Main',
            iconClass: 'fas fa-home'
        }
    )
    .set(
        PagesEnum.Rooms,
        {
            id: PagesEnum.Rooms,
            name: 'Rooms',
            iconClass: 'fas fa-door-open',
            roles: [RoleEnum.SuperUser, RoleEnum.Approver]
        }
    )
    .set(
        PagesEnum.Equipment,
        {
            id: PagesEnum.Equipment,
            name: 'Equipment',
            iconClass: 'fas fa-couch',
            roles: [RoleEnum.SuperUser, RoleEnum.Approver]
        }
    )
    .set(
        PagesEnum.Events,
        {
            id: PagesEnum.Events,
            name: 'Events',
            iconClass: 'far fa-calendar-check'
        }
    )
    .set(
        PagesEnum.Users,
        {
            id: PagesEnum.Users,
            name: 'Users',
            iconClass: 'fas fa-user-friends',
            roles: [RoleEnum.SuperUser]
        }
    )
    .set(
        PagesEnum.EmailTemplates,
        {
            id: PagesEnum.EmailTemplates,
            name: 'Email Templates',
            iconClass: 'fas fa-envelope',
            roles: [RoleEnum.SuperUser]
        }
    )
    .set(
        PagesEnum.Services,
        {
            id: PagesEnum.Services,
            name: 'Services',
            iconClass: 'fas fa-concierge-bell',
            roles: [RoleEnum.SuperUser, RoleEnum.Approver]
        }
    )
    .set(
        PagesEnum.Reports,
        {
            id: PagesEnum.Reports,
            name: 'Reports',
            iconClass: 'fas fa-file-excel'
        }
    )
    .set(
        PagesEnum.Logs,
        {
            id: PagesEnum.Logs,
            name: 'Logs',
            iconClass: 'fas fa-receipt',
            roles: [RoleEnum.SuperUser]
        }
    )
    .set(
        PagesEnum.Invoices,
        {
            id: PagesEnum.Invoices,
            name: 'Invoices',
            iconClass: 'fas fa-file-invoice-dollar',
            roles: [RoleEnum.SuperUser, RoleEnum.Approver]
        }
    );
