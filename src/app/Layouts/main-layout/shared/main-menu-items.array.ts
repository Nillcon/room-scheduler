import { IMenuItem } from '../interfaces/menu-item.interface';
import { MainLayoutPagesEnum as PagesEnum } from '@Layouts/main-layout/main-layout-pages';
import { MainLayoutPagesMap as PagesMap } from './main-layout-pages.map';

function initPages (...pages: PagesEnum[]): IMenuItem[] {
    const result: IMenuItem[] = [];

    pages.forEach(page => {
        result.push({
            name: PagesMap.get(page).name,
            link: page,
            iconClass: PagesMap.get(page).iconClass,
            roles: PagesMap.get(page).roles
        });
    });

    return result;
}

export const MainMenuItemsArray: IMenuItem[] = initPages(
    PagesEnum.Main,
    PagesEnum.Users,
    PagesEnum.Events,
    PagesEnum.Rooms,
    PagesEnum.Equipment,
    PagesEnum.Services,
    PagesEnum.EmailTemplates,
    PagesEnum.Reports,
    PagesEnum.Logs,
    PagesEnum.Invoices
);
