import { Component, OnInit } from '@angular/core';
import { MainMenuItemsArray } from '@Layouts/main-layout/shared/main-menu-items.array';
import { IMenuItem } from './interfaces/menu-item.interface';
import { IUser, UserService } from '@App/Features/user';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

    public MainMenuItemsArray: IMenuItem[] = MainMenuItemsArray;

    public userData$: Observable<IUser>;

    constructor (
        private userService: UserService
    ) { }

    public ngOnInit (): void {
        this.initUserDataObservable();
    }

    public initUserDataObservable (): void {
        this.userData$ = this.userService.Data$;
    }

}
