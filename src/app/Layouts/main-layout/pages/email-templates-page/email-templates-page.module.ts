import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailTemplatesPageComponent } from './email-templates-page.component';
import { EmailTemplatesPageRoutingModule } from './email-templates-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { EmailTemplateCreationWindowComponent } from './components/email-template-creation-window/email-template-creation-window.component';
import { EmailTemplatesTableComponent } from './components/email-templates-table/email-templates-table.component';
import { EmailTemplateEditingWindowComponent } from './components/email-template-editing-window/email-template-editing-window.component';
import { EmailTemplatesPageGuard } from './guards/email-templates-page.guard';

@NgModule({
    declarations: [
        EmailTemplatesPageComponent,
        EmailTemplatesTableComponent,
        EmailTemplateCreationWindowComponent,
        EmailTemplateEditingWindowComponent
    ],
    imports: [
        CommonModule,

        EmailTemplatesPageRoutingModule,
        SharedModule,
        FeaturesModule,
        MainLayoutSharedModule
    ],
    entryComponents: [
        EmailTemplateCreationWindowComponent,
        EmailTemplateEditingWindowComponent
    ],
    providers: [
        EmailTemplatesPageGuard
    ]
})
export class EmailTemplatesPageModule { }
