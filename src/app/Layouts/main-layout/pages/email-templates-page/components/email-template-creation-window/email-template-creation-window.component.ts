import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { IEmailTemplate } from '@App/Features/email-template';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription } from 'rxjs';
import { EmailTemplateService } from '@App/Features/email-template/email-template.service';

@Component({
    selector: 'app-email-template-creation-window',
    templateUrl: './email-template-creation-window.component.html',
    styleUrls: ['./email-template-creation-window.component.scss']
})
export class EmailTemplateCreationWindowComponent implements OnInit {

    public emailTemplateForm: FormGroupTypeSafe<Partial<IEmailTemplate>>;
    public creationEmailTemplateSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<EmailTemplateCreationWindowComponent>,
        private emailTemplateService: EmailTemplateService
    ) {}

    public ngOnInit (): void {}

    public createTemplate (): void {
        this.creationEmailTemplateSubscription = this.emailTemplateService.create(this.emailTemplateForm.value)
            .subscribe(() => {
                this.dialogRef.close(true);
            });
    }

    public onEmailTemplateFormInit (form: FormGroupTypeSafe<Partial<IEmailTemplate>>): void {
        this.emailTemplateForm = form;
    }

    public close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
