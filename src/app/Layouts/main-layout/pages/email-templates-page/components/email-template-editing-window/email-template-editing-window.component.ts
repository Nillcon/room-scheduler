import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IEmailTemplate } from '@App/Features/email-template';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription } from 'rxjs';
import { EmailTemplateService } from '@App/Features/email-template/email-template.service';

@Component({
  selector: 'app-email-template-editing-window',
  templateUrl: './email-template-editing-window.component.html',
  styleUrls: ['./email-template-editing-window.component.scss']
})
export class EmailTemplateEditingWindowComponent implements OnInit {

    public emailTemplateForm: FormGroupTypeSafe<Partial<IEmailTemplate>>;
    public saveEmailTemplateSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<EmailTemplateEditingWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public emailTemplate: IEmailTemplate,
        private emailTemplateService: EmailTemplateService
    ) {}

    public ngOnInit (): void {}

    public onEmailTemplateFormInit (form: FormGroupTypeSafe<Partial<IEmailTemplate>>): void {
        this.emailTemplateForm = form;
    }

    public saveTemplate (): void {
       this.saveEmailTemplateSubscription = this.emailTemplateService.edit(this.emailTemplate.id, this.emailTemplateForm.value)
            .subscribe(() => {
                this.close(true);
            });
    }

    public close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
