import { Component, OnInit, Injector } from '@angular/core';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { MatDialog } from '@angular/material';
import { IEmailTemplate } from '@App/Features/email-template';
import { NotificationService } from '@Core/root/notification/notification.service';
import { switchMap, tap, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EmailTemplateEditingWindowComponent } from '../email-template-editing-window/email-template-editing-window.component';
import { EmailTemplateCreationWindowComponent } from '../email-template-creation-window/email-template-creation-window.component';
import { EmailTemplateService } from '@App/Features/email-template/email-template.service';

@Component({
  selector: 'app-email-templates-table',
  templateUrl: './email-templates-table.component.html',
  styleUrls: ['./email-templates-table.component.scss']
})
export class EmailTemplatesTableComponent implements OnInit {

    public emailTemplates: IEmailTemplate[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private emailTemplateService: EmailTemplateService,
        private notifyService: NotificationService
    ) { }

    public ngOnInit (): void {
        this.updateEmailTemplates()
            .subscribe();
    }

    public updateEmailTemplates (): Observable<any> {
        return this.emailTemplateService.getAll()
            .pipe(
                tap(res => this.emailTemplates = res)
            );
    }

    public openCreateTemplateWindow (): Observable<any> {
        const dialog = this.dialog.open(EmailTemplateCreationWindowComponent, {});

        return dialog.afterClosed();
    }

    public openEditingTemplateWindow (template: IEmailTemplate): Observable<any> {
        const dialog = this.dialog.open(EmailTemplateEditingWindowComponent, {
            data: template
        });

        return dialog.afterClosed();
    }

    public onCreateEmailTemplate (): void {
        const dialog = this.openCreateTemplateWindow()
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateEmailTemplates())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    public onEditEmailTemplate (template: IEmailTemplate): void {
        this.emailTemplateService.get(template.id)
            .pipe(
                switchMap(res => this.openEditingTemplateWindow(res)),
                filter(res => !!res),
                switchMap(() => this.updateEmailTemplates())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    @NeedsConfirmation('Are you really want to delete the email template?', 'Confirm deleting')
    public onDeleteEmailTemplate (template: IEmailTemplate): void {
        this.emailTemplateService.delete(template.id)
            .pipe(
               switchMap(() => this.updateEmailTemplates())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }
}
