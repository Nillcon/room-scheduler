import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailTemplatesPageComponent } from './email-templates-page.component';
import { EmailTemplatesPageGuard } from './guards/email-templates-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: EmailTemplatesPageComponent,
        canActivate: [EmailTemplatesPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class EmailTemplatesPageRoutingModule {}
