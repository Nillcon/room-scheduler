import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { MainLayoutPagesEnum } from '@Layouts/main-layout/main-layout-pages';
import { AppPagesEnum } from '@App/app-pages';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RoomService } from '@App/Features/room/room.service';

export class DetailsPageCorrectRoomGuard implements CanActivate {
    constructor (
        private roomService: RoomService,
        private navigationService: NavigationService
    ) {}

    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const roomId: string = route.queryParams['roomId'];

        return this.roomService.checkValid(roomId)
            .pipe(
               tap(res => {
                   if (!res) {
                       this.navigationService.navigate(`${AppPagesEnum.Index}/${MainLayoutPagesEnum.Rooms}`);
                   }
               })
            );
    }
}
