import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '@App/Features/user';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { RoleEnum } from '@App/Features/role/shared/role.enum';
import { MainLayoutPagesEnum } from '@Layouts/main-layout/main-layout-pages';
import { AppPagesEnum } from '@App/app-pages';
import { MainLayoutPagesMap } from '@Layouts/main-layout/shared/main-layout-pages.map';

export class DetailsPageGuard implements CanActivate {
    constructor (
        private userService: UserService,
        private navigationService: NavigationService
    ) {}

    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const pageRoles: RoleEnum[] = MainLayoutPagesMap.get(MainLayoutPagesEnum.Rooms).roles || [];

        if (this.userService.includesOneOfRoles(pageRoles)) {
            return true;
        } else {
            this.navigationService.navigate(`${AppPagesEnum.Index}/${MainLayoutPagesEnum.Rooms}`);
            return false;
        }
    }
}
