import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomsPageRoutingModule } from './rooms-page.routing';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { DetailsPageGuard } from './guards/details-page.guard';
import { DetailsPageCorrectRoomGuard } from './guards/details-page-correct-room.guard';

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,

        RoomsPageRoutingModule,
        MainLayoutSharedModule,
    ],
    providers: [
        DetailsPageGuard,
        DetailsPageCorrectRoomGuard
    ],
})
export class RoomsPageModule { }
