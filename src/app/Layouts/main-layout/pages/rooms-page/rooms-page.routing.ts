import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoomsPagesEnum } from './rooms-pages';
import { DetailsPageGuard } from './guards/details-page.guard';
import { DetailsPageCorrectRoomGuard } from './guards/details-page-correct-room.guard';

export const routes: Routes = [
    {
        path: RoomsPagesEnum.Index,
        loadChildren: '@Layouts/main-layout/pages/rooms-page/pages/index-page/index-page.module#IndexPageModule'
    },
    {
        path: RoomsPagesEnum.Details,
        loadChildren: '@Layouts/main-layout/pages/rooms-page/pages/details-page/details-page.module#DetailsPageModule',
        canActivate: [
            DetailsPageGuard,
            DetailsPageCorrectRoomGuard
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class RoomsPageRoutingModule {}
