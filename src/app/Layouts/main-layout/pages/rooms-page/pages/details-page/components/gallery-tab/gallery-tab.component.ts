import { Component, OnInit, Input } from '@angular/core';
import { IsString } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces';
import { MenuItemsMap } from '../../shared/menu-items.map';
import { MenuItemsEnum } from '../../shared/menu-items.enum';
import { UrlService } from '@Core/root/url/url.service';

@ClassValidation()
@Component({
    selector: 'gallery-tab',
    templateUrl: './gallery-tab.component.html',
    styleUrls: ['./gallery-tab.component.scss']
})
export class GalleryTabComponent implements OnInit {

    public roomId: string;

    public currentTabData: IBlockMenuItem;

    constructor (
        private urlService: UrlService,
    ) {}

    public ngOnInit (): void {
        this.roomId = this.getRoomId();

        this.updateTabData();
    }

    public updateTabData (): void {
        this.currentTabData = MenuItemsMap.get(MenuItemsEnum.GalleryTab);
    }

    private getRoomId (): string {
        return this.urlService.getParameter('roomId');
    }

}
