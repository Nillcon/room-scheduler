import { Component, OnInit } from '@angular/core';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IEquipment } from '@App/Features/equipment';
import { NotificationService } from '@Core/root/notification/notification.service';
import { IRoomEquipment } from '@App/Features/room/interfaces/room-equipment.interface';
import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces';
import { MenuItemsMap } from '../../shared/menu-items.map';
import { MenuItemsEnum } from '../../shared/menu-items.enum';
import { UrlService } from '@Core/root/url/url.service';
import { RoomService } from '@App/Features/room/room.service';
import { EquipmentService } from '@App/Features/equipment/equipment.service';
import { tap } from 'rxjs/operators';

@ClassValidation()
@Component({
  selector: 'equipment-tab',
  templateUrl: './equipment-tab.component.html',
  styleUrls: ['./equipment-tab.component.scss']
})
export class EquipmentTabComponent implements OnInit {

    public roomId: string;

    public currentTabData: IBlockMenuItem;

    public roomEquipment: IRoomEquipment[];
    public equipment: IEquipment[];

    constructor (
        private roomService: RoomService,
        private equipmentService: EquipmentService,
        private notifyService: NotificationService,
        private urlService: UrlService,
    ) {}

    public ngOnInit (): void {
        this.roomId = this.getRoomId();

        this.updateTabData();

        this.updateRoomEquipment();
        this.updateEquipment();
    }

    public updateTabData (): void {
        this.currentTabData = MenuItemsMap.get(MenuItemsEnum.EquipmentTab);
    }

    public updateRoomEquipment (): void {
        this.roomService.getEquipment(this.roomId)
            .subscribe(res => this.roomEquipment = res);
    }

    public updateEquipment (): void {
        this.equipmentService.getAll()
            .subscribe(res => this.equipment = res);
    }

    public saveRoomEquipment (): void {
        this.roomService.setEquipment(this.roomId, this.roomEquipment)
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    private getRoomId (): string {
        return this.urlService.getParameter('roomId');
    }

}
