import { Component, OnInit } from '@angular/core';
import { IUser, UserService } from '@App/Features/user';
import { Subscription, Observable } from 'rxjs';
import { NotificationService } from '@Core/root/notification/notification.service';
import { IRoomApprover } from '@App/Features/room/interfaces/room-approver.interface';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces';
import { MenuItemsMap } from '../../shared/menu-items.map';
import { MenuItemsEnum } from '../../shared/menu-items.enum';
import { UrlService } from '@Core/root/url/url.service';
import { tap, filter, switchMap } from 'rxjs/operators';
import { RoomService } from '@App/Features/room/room.service';

@ClassValidation()
@Component({
  selector: 'approve-tab',
  templateUrl: './approve-tab.component.html',
  styleUrls: ['./approve-tab.component.scss']
})
export class ApproveTabComponent implements OnInit {

    public roomId: string;

    public currentTabData: IBlockMenuItem;

    public roomGroups: string[] = [];
    public roomApprovers: IRoomApprover;

    public saveRoomApproversSubscription: Subscription;

    constructor (
        private roomService: RoomService,
        private userService: UserService,
        private notifyService: NotificationService,
        private urlService: UrlService,
    ) {}

    public ngOnInit (): void {
        this.roomId = this.getRoomId();

        this.updateTabData();

        this.updateGroupNames()
            .pipe(
                tap(res => this.roomGroups = res),
                filter(res => !res.length),
                switchMap(res => this.updateRoomApprovers())
            )
            .subscribe();
    }

    public updateRoomApprovers (): Observable<IRoomApprover> {
        return this.roomService.getApprovers(this.roomId)
            .pipe(
                tap(res => this.roomApprovers = res)
            );
    }

    public updateGroupNames (): Observable<string[]> {
        return this.roomService.getGroupNamesWithApprovers(this.roomId)
            .pipe(
                tap(res => this.roomGroups = res)
            );
    }

    public updateAutocomplete = (phrase: string = ''): Observable<Partial<IUser>[]> => {
        return this.userService.search(phrase);
    }

    public updateTabData (): void {
        this.currentTabData = MenuItemsMap.get(MenuItemsEnum.ApproversTab);
    }

    public saveRoomApprovers (): void {
        this.saveRoomApproversSubscription = this.roomService.setApprovers(
            this.roomId,
            {
                isAutoApproved: this.roomApprovers.isAutoApproved,
                approvers: this.roomApprovers.approvers
            }
        )
            .subscribe(() => {
                this.notifyService.success({});

                if (this.roomApprovers.isAutoApproved) {
                    this.roomApprovers.approvers = [];
                }
            });
    }

    private getRoomId (): string {
        return this.urlService.getParameter('roomId');
    }

}
