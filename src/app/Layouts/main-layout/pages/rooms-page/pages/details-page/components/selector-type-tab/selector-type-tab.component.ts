import { Component, OnInit } from '@angular/core';
import { NotificationService } from '@Core/root/notification/notification.service';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces';
import { MenuItemsMap } from '../../shared/menu-items.map';
import { MenuItemsEnum } from '../../shared/menu-items.enum';
import { UrlService } from '@Core/root/url/url.service';
import { RoomService } from '@App/Features/room/room.service';

@ClassValidation()
@Component({
  selector: 'selector-type-tab',
  templateUrl: './selector-type-tab.component.html',
  styleUrls: ['./selector-type-tab.component.scss']
})
export class SelectorTypeTabComponent implements OnInit {

    public roomId: string;

    public currentTabData: IBlockMenuItem;

    public roomTypes: number[] = [];

    public saveRoomLayoutsSubscription: Subscription;

    constructor (
        private roomService: RoomService,
        private notifyService: NotificationService,
        private urlService: UrlService,
    ) { }

    public ngOnInit (): void {
        this.roomId = this.getRoomId();

        this.updateTabData();
        this.updateRoomLayouts();
    }

    public updateRoomLayouts (): void {
        this.roomService.getLayouts(this.roomId)
            .pipe(
                map(res => res.value)
            )
            .subscribe(res => this.roomTypes = res);
    }

    public updateTabData (): void {
        this.currentTabData = MenuItemsMap.get(MenuItemsEnum.TypeSelectorTab);
    }

    public saveRoomLayouts (): void {
        this.saveRoomLayoutsSubscription = this.roomService.saveLayouts(this.roomId, this.roomTypes)
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    private getRoomId (): string {
        return this.urlService.getParameter('roomId');
    }

}
