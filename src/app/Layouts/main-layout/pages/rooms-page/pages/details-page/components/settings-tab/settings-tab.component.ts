import { Component, OnInit } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRoomSettingsForm } from '@App/Features/room/interfaces/room-settings-form.interface';
import { Subscription } from 'rxjs';
import { NotificationService } from '@Core/root/notification/notification.service';
import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces';
import { MenuItemsMap } from '../../shared/menu-items.map';
import { MenuItemsEnum } from '../../shared/menu-items.enum';
import { UrlService } from '@Core/root/url/url.service';
import { RoomService } from '@App/Features/room/room.service';

@Component({
    selector: 'settings-tab',
    templateUrl: './settings-tab.component.html',
    styleUrls: ['./settings-tab.component.scss']
})
export class SettingsTabComponent implements OnInit {

    public roomId: string;

    public currentTabData: IBlockMenuItem;

    public roomSettings: Partial<IRoomSettingsForm>;

    public settingsFormGroup: FormGroupTypeSafe<IRoomSettingsForm>;

    public savingSubscription: Subscription;

    constructor (
        private roomService: RoomService,
        private notificationService: NotificationService,
        private urlService: UrlService,
    ) {}

    public ngOnInit (): void {
        this.roomId = this.getRoomId();

        this.updateTabData();
        this.updateSettings();
    }

    public updateSettings (): void {
        this.roomService.getSettings(this.roomId)
            .subscribe((settings) => {
                this.roomSettings = settings;
            });
    }

    public updateTabData (): void {
        this.currentTabData = MenuItemsMap.get(MenuItemsEnum.SettingsTab);
    }

    public saveSettings (): void {
        if (this.settingsFormGroup.valid) {
            this.savingSubscription = this.roomService.editSettings(this.roomId, this.settingsFormGroup.value)
                .subscribe(() => {
                    this.notificationService.success({ text: 'Room settings saved!' });
                });
        } else {
            this.notificationService.error({ text: 'Form is not valid' });
        }
    }

    public onSettingsFormInit (formGroup: FormGroupTypeSafe<IRoomSettingsForm>): void {
        this.settingsFormGroup = formGroup;
    }

    private getRoomId (): string {
        return this.urlService.getParameter('roomId');
    }

}
