import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuItemsMap } from './shared/menu-items.map';
import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces/block-menu-item.interface';
import { UrlService } from '@Core/root/url/url.service';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { IBreadcrumbsItem } from '@Modules/UI/breabcrumbs/interfaces/breadcrumbs-item.interface';

@Component({
    selector: 'app-details-page',
    templateUrl: './details-page.component.html',
    styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit, OnDestroy {

    public roomId: string;
    public roomName: string;

    public MenuItems = [...MenuItemsMap.values()];

    public breadCrumbsData: IBreadcrumbsItem[];

    constructor (
        private urlService: UrlService,
        private navigationService: NavigationService
    ) { }

    public ngOnInit (): void  {
        this.roomId = this.getRoomId();
        this.roomName = this.getRoomName();

        this.initBreadCrumbsData();
    }

    public ngOnDestroy (): void {
        this.urlService.clearParameters();
    }

    public redirectToRoomsIndex (): void {
        // FIXME: change url
        this.navigationService.navigate(`Main/Rooms`, false, {});
    }

    private getRoomId (): string {
        return this.urlService.getParameter('roomId');
    }

    private getRoomName (): string {
        return this.urlService.getParameter('roomName');
    }

    private initBreadCrumbsData (): void {
        this.breadCrumbsData = [
            {
                name: 'Rooms',
                link: 'Main/Rooms',
                parameters: {}
            },
            {
                name: this.roomName
            }
        ];
    }

}
