import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsPageComponent } from './details-page.component';
import { ApproveTabComponent } from './components/approve-tab/approve-tab.component';
import { EquipmentTabComponent } from './components/equipment-tab/equipment-tab.component';
import { GalleryTabComponent } from './components/gallery-tab/gallery-tab.component';
import { SettingsTabComponent } from './components/settings-tab/settings-tab.component';
import { SelectorTypeTabComponent } from './components/selector-type-tab/selector-type-tab.component';

export const routes: Routes = [
    {
        path: '',
        component: DetailsPageComponent,
        children: [
            {
                path: 'Types',
                component: SelectorTypeTabComponent
            },
            {
                path: 'Approve',
                component: ApproveTabComponent
            },
            {
                path: 'Equipment',
                component: EquipmentTabComponent
            },
            {
                path: 'Gallery',
                component: GalleryTabComponent
            },
            {
                path: 'Setting',
                component: SettingsTabComponent
            },
            {
                path: '',
                redirectTo: 'Types',
                pathMatch: 'full'
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DetailsPageRoutingModule {}
