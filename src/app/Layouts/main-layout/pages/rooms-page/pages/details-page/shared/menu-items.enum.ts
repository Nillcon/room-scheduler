export enum MenuItemsEnum {
    TypeSelectorTab = 1,
    EquipmentTab,
    GalleryTab,
    ApproversTab,
    SettingsTab
}
