import { IBlockMenuItem } from '@Modules/UI/block-menu/interfaces';
import { MenuItemsEnum } from './menu-items.enum';
import { RoleEnum } from '@App/Features/role/shared/role.enum';

export const MenuItemsMap: Map<MenuItemsEnum, IBlockMenuItem> = new Map<MenuItemsEnum, IBlockMenuItem>()
    .set(MenuItemsEnum.TypeSelectorTab, {
        id: MenuItemsEnum.TypeSelectorTab,
        name: 'Types',
        iconClass: 'fas fa-th',
        link: '/Main/Rooms/Details/Types'
    })
    .set(MenuItemsEnum.EquipmentTab, {
        id: MenuItemsEnum.EquipmentTab,
        name: 'Equipment',
        iconClass: 'fas fa-couch',
        link: '/Main/Rooms/Details/Equipment'
    })
    .set(MenuItemsEnum.GalleryTab, {
        id: MenuItemsEnum.GalleryTab,
        name: 'Gallery',
        iconClass: 'fas fa-images',
        link: '/Main/Rooms/Details/Gallery'
    })
    .set(MenuItemsEnum.ApproversTab, {
        id: MenuItemsEnum.ApproversTab,
        name: 'Approvers',
        iconClass: 'fas fa-user-check',
        forbiddenFor: [RoleEnum.Approver],
        link: '/Main/Rooms/Details/Approve'
    })
    .set(MenuItemsEnum.SettingsTab, {
        id: MenuItemsEnum.SettingsTab,
        name: 'Settings',
        iconClass: 'fas fa-cog',
        link: '/Main/Rooms/Details/Setting'
    });
