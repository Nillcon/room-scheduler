import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsPageComponent } from './details-page.component';
import { DetailsPageRoutingModule } from './details-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { BlockMenuModule } from '@Modules/UI/block-menu/block-menu.module';
import { BreabcrumbsModule } from '@Modules/UI/breabcrumbs/breabcrumbs.module';
import { SelectorTypeTabComponent } from './components/selector-type-tab/selector-type-tab.component';
import { EquipmentTabComponent } from './components/equipment-tab/equipment-tab.component';
import { GalleryTabComponent } from './components/gallery-tab/gallery-tab.component';
import { ApproveTabComponent } from './components/approve-tab/approve-tab.component';
import { SettingsTabComponent } from './components/settings-tab/settings-tab.component';

@NgModule({
    declarations: [
        DetailsPageComponent,
        SelectorTypeTabComponent,
        EquipmentTabComponent,
        GalleryTabComponent,
        ApproveTabComponent,
        SettingsTabComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MainLayoutSharedModule,
        FeaturesModule,

        DetailsPageRoutingModule,

        BlockMenuModule,
        BreabcrumbsModule
    ]
})
export class DetailsPageModule { }
