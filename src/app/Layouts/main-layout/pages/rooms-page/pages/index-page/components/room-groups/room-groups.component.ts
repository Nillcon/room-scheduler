import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { RoomGroupAssignersListComponent } from '../room-group-assigners-list/room-group-assigners-list.component';
import { MatDialog } from '@angular/material';
import { RoomGroupCreationWindowComponent } from '../room-group-creation-window/room-group-creation-window.component';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-room-groups',
  templateUrl: './room-groups.component.html',
  styleUrls: ['./room-groups.component.scss']
})
export class RoomGroupsComponent implements OnInit {

    @ViewChild('RoomGroupAssignersList', { static: false })
    private roomGroupAssignersListComponent: RoomGroupAssignersListComponent;

    constructor (
        public injector: Injector,
        private dialog: MatDialog
    ) {}

    public ngOnInit (): void {}

    public onAddButtonClick (): void {
        this.dialog.open(RoomGroupCreationWindowComponent, {
            width: '500px'
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                tap(() => this.roomGroupAssignersListComponent.updateRoomGroups())
            )
            .subscribe();
    }

}
