import { Component, OnInit, Injector } from '@angular/core';
import { MatBottomSheet } from '@angular/material';

import { NavigationService } from '@Core/root/navigation/navigation.service';
import { NotificationService } from '@Core/root/notification/notification.service';

import { tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { IOnEquipmentTypeGroupChipClick } from '@App/Features/room/interfaces/on-equipment-type-group-chip-click-event.interface';
import { IRoom, IOnChangeRoomColorEvent } from '@App/Features/room';

import { RoomEquipmentGroupsBottomSheetComponent } from '../room-equipment-groups-bottom-sheet/room-equipment-groups-bottom-sheet.component';
import { UserService } from '@App/Features/user';
import { RoleEnum } from '@App/Features/role/shared/role.enum';
import { RoomService } from '@App/Features/room/room.service';

@Component({
    selector: 'app-rooms-list',
    templateUrl: './rooms-list.component.html',
    styleUrls: ['./rooms-list.component.scss']
})
export class RoomsListComponent implements OnInit {

    public roomArray: IRoom[];

    public isRoomCardClickable: boolean    = false;
    public isColorSelectorEnabled: boolean = false;

    constructor (
        public injector: Injector,
        private bottomSheet: MatBottomSheet,
        private userService: UserService,
        private roomService: RoomService,
        private navigationService: NavigationService,
        private notificationService: NotificationService
    ) {}

    public ngOnInit (): void {
        this.initRoomCardClickableState();
        this.initRoomColorChangerVisibleState();

        this.updateRooms()
            .subscribe();
    }

    public updateRooms (): Observable<IRoom[]> {
        return this.roomService.getAll()
            .pipe(
                tap(rooms => {
                    this.roomArray = rooms;
                })
            );
    }

    public onRoomChangeColor (event: IOnChangeRoomColorEvent): void {
        this.roomService.changeColor(event.room.id, event.color.id)
            .pipe(
                switchMap(() => {
                    return this.updateRooms();
                }),
                tap(() => {
                    this.notificationService.success({ text: 'Room color changed!' });
                })
            )
            .subscribe();
    }

    public onRoomEquipmentTypeGroupClick (event: IOnEquipmentTypeGroupChipClick): void {
        this.bottomSheet.open(RoomEquipmentGroupsBottomSheetComponent, {
            data: event
        });
    }

    public onRoomClick (room: IRoom): void {
        this.navigationService.navigate(
            `Main/Rooms/Details`,
            false,
            {
                roomId: room.id,
                roomName: room.name
            }
        );
    }

    private initRoomCardClickableState (): void {
        this.isRoomCardClickable = this.userService.includesOneOfRoles([
            RoleEnum.SuperUser,
            RoleEnum.Approver
        ]);
    }

    private initRoomColorChangerVisibleState (): void {
        this.isColorSelectorEnabled = this.userService.includesRole(RoleEnum.SuperUser);
    }

}
