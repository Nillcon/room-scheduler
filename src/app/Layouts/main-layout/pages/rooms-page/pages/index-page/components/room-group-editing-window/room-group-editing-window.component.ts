import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IsInt } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IRoomGroup } from '@App/Features/room-group';
import { NotificationService } from '@Core/root/notification/notification.service';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RoomGroupService } from '@App/Features/room-group/room-group.service';

@ClassValidation()
@Component({
    selector: 'app-room-group-editing-window',
    templateUrl: './room-group-editing-window.component.html',
    styleUrls: ['./room-group-editing-window.component.scss']
})
export class RoomGroupEditingWindowComponent implements OnInit {

    @IsInt()
    public roomGroupId: number;

    public roomGroup: IRoomGroup;

    public savingSubscription: Subscription;

    private roomGroupFormGroup: FormGroupTypeSafe<Partial<IRoomGroup>>;

    constructor (
        private dialogRef: MatDialogRef<RoomGroupEditingWindowComponent>,
        @Inject(MAT_DIALOG_DATA) roomGroupId: number,
        private roomGroupService: RoomGroupService,
        private notifyService: NotificationService
    ) {
        this.roomGroupId = roomGroupId;
    }

    public ngOnInit (): void {
        this.updateRoomGroup();
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onRoomGroupFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IRoomGroup>>): void {
        this.roomGroupFormGroup = formGroup;
    }

    public updateRoomGroup (): void {
        this.roomGroupService.get(this.roomGroupId)
            .subscribe((roomGroup) => {
                this.roomGroup = roomGroup;
            });
    }

    public editRoomGroup (): void {
        if (this.roomGroupFormGroup.valid) {
            this.savingSubscription = this.roomGroupService.edit(this.roomGroupId, this.roomGroupFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Room group saved!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.roomGroupFormGroup.markAllAsTouched();
        }
    }

}
