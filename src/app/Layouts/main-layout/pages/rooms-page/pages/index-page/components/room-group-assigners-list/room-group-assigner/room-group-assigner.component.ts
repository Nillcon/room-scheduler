import { Component, OnInit, Input, Output, EventEmitter, Injector } from '@angular/core';
import { IRoomGroup } from '@App/Features/room-group';
import { catchError, flatMap, map, toArray, filter, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { MatDialog } from '@angular/material';
import { RoomGroupApproversAssignWindowComponent } from '../../room-group-approvers-assign-window/room-group-approvers-assign-window.component';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { RoomGroupService } from '@App/Features/room-group/room-group.service';
import { RoomService } from '@App/Features/room/room.service';
import { RoomGroupEditingWindowComponent } from '../../room-group-editing-window/room-group-editing-window.component';

@ClassValidation()
@Component({
    selector: 'app-room-group-assigner',
    templateUrl: './room-group-assigner.component.html',
    styleUrls: ['./room-group-assigner.component.scss']
})
export class RoomGroupAssignerComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public roomGroup: IRoomGroup;

    @Output() public OnSomeChanges: EventEmitter<boolean> = new EventEmitter();

    public assignedRooms: string[];
    public autocompleteRooms: string[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private roomGroupService: RoomGroupService,
        private roomService: RoomService
    ) {}

    public ngOnInit (): void {
        this.assignedRooms = this.roomGroup.rooms;
    }

    public updateAutocomplete (): void {
        this.roomService.getAll()
            .pipe(
                flatMap(rooms => rooms),
                map(room => {
                    return room.name;
                }),
                toArray()
            )
            .subscribe(rooms => {
                this.autocompleteRooms = rooms;
            });
    }

    public assignRoom (roomName: string): void {
        this.roomGroupService.assignRoom(this.roomGroup.id, roomName)
            .pipe(
                catchError((error) => {
                    this.removeRoomFromTagbox(roomName);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public deAssignRoom (roomName: string): void {
        this.roomGroupService.deAssignRoom(this.roomGroup.id, roomName)
            .pipe(
                catchError((error) => {
                    this.addRoomToTagbox(roomName);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public clearRooms (): void {
        this.roomGroupService.clearAllRoomAssigns(this.roomGroup.id)
            .subscribe();
    }

    public onFocus (): void {
        if (!this.autocompleteRooms) {
            this.updateAutocomplete();
        }
    }

    public onRoomGroupEdit (roomGroup: IRoomGroup): void {
        this.dialog.open(RoomGroupEditingWindowComponent, {
            width: '500px',
            data: roomGroup.id
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                tap(() => this.OnSomeChanges.next(true))
            )
            .subscribe();
    }

    public onRoomGroupChangeApprover (roomGroup: IRoomGroup): void {
        this.dialog.open(RoomGroupApproversAssignWindowComponent, {
            width: '500px',
            data: roomGroup
        });
    }

    @NeedsConfirmation('Do you really want to delete the room group?', 'Confirm deleting')
    public onRoomGroupDelete (roomGroup: IRoomGroup): void {
        this.roomGroupService.delete(roomGroup.id)
            .pipe(
                tap(() => this.OnSomeChanges.next(true))
            )
            .subscribe();
    }

    private addRoomToTagbox (room: string): void {
        this.assignedRooms.push(room);
    }

    private removeRoomFromTagbox (room: string): void {
        this.assignedRooms = this.assignedRooms.filter(currRoom => {
            return currRoom !== room;
        });
    }

}
