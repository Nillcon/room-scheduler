import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRoomGroup } from '@App/Features/room-group';
import { NotificationService } from '@Core/root/notification/notification.service';
import { tap } from 'rxjs/operators';
import { RoomGroupService } from '@App/Features/room-group/room-group.service';

@Component({
    selector: 'app-room-group-creation-window',
    templateUrl: './room-group-creation-window.component.html',
    styleUrls: ['./room-group-creation-window.component.scss']
})
export class RoomGroupCreationWindowComponent implements OnInit {

    public creationSubscription: Subscription;

    private roomGroupFormGroup: FormGroupTypeSafe<Partial<IRoomGroup>>;

    constructor (
        private dialogRef: MatDialogRef<RoomGroupCreationWindowComponent>,
        private roomGroupService: RoomGroupService,
        private notifyService: NotificationService
    ) {}

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onRoomGroupFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IRoomGroup>>): void {
        this.roomGroupFormGroup = formGroup;
    }

    public createRoomGroup (): void {
        if (this.roomGroupFormGroup.valid) {
            this.creationSubscription = this.roomGroupService.create(this.roomGroupFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Room group created!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.roomGroupFormGroup.markAllAsTouched();
        }
    }

}
