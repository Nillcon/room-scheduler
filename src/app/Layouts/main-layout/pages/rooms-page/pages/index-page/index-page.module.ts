import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexPageComponent } from './index-page.component';
import { RoomEquipmentGroupsBottomSheetComponent } from './components/room-equipment-groups-bottom-sheet/room-equipment-groups-bottom-sheet.component';
import { RoomsListComponent } from './components/rooms-list/rooms-list.component';
import { RoomGroupsComponent } from './components/room-groups/room-groups.component';
import { RoomGroupApproversAssignWindowComponent } from './components/room-group-approvers-assign-window/room-group-approvers-assign-window.component';
import { RoomGroupAssignersListComponent } from './components/room-group-assigners-list/room-group-assigners-list.component';
import { RoomGroupCreationWindowComponent } from './components/room-group-creation-window/room-group-creation-window.component';
import { RoomGroupEditingWindowComponent } from './components/room-group-editing-window/room-group-editing-window.component';
import { RoomGroupAssignerComponent } from './components/room-group-assigners-list/room-group-assigner/room-group-assigner.component';
import { ApproverAssignerComponent } from './components/room-group-approvers-assign-window/approver-assigner/approver-assigner.component';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesSharedModule } from '@Features/features-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { IndexPageRoutingModule } from './index-page.routing';

@NgModule({
    declarations: [
        IndexPageComponent,
        RoomEquipmentGroupsBottomSheetComponent,
        RoomsListComponent,
        RoomGroupsComponent,
        RoomGroupApproversAssignWindowComponent,
        RoomGroupAssignersListComponent,
        RoomGroupCreationWindowComponent,
        RoomGroupEditingWindowComponent,
        RoomGroupAssignerComponent,
        ApproverAssignerComponent,
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule,
        IndexPageRoutingModule,

        FeaturesModule,
        MainLayoutSharedModule,
    ],
    entryComponents: [
        RoomEquipmentGroupsBottomSheetComponent,
        RoomGroupApproversAssignWindowComponent,
        RoomGroupCreationWindowComponent,
        RoomGroupEditingWindowComponent
    ]
})
export class IndexPageModule { }
