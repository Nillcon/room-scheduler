import { Component, OnInit, Input } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { UserService } from '@App/Features/user';
import { IUser } from '@App/Features/user';
import { IRoomGroup } from '@App/Features/room-group';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IsNotEmptyObject } from 'class-validator';
import { RoomGroupService } from '@App/Features/room-group/room-group.service';

@ClassValidation()
@Component({
    selector: 'app-approver-assigner',
    templateUrl: './approver-assigner.component.html',
    styleUrls: ['./approver-assigner.component.scss']
})
export class ApproverAssignerComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public roomGroup: IRoomGroup;

    public assignedApprovers: Partial<IUser>[] = [];

    constructor (
        private roomGroupService: RoomGroupService,
        private userService: UserService
    ) {}

    public ngOnInit (): void {
        this.updateAssignedApprovers();
        this.updateAutocomplete();
    }

    public updateAssignedApprovers (): void {
        this.roomGroupService.get(this.roomGroup.id)
            .subscribe(roomGroup => {
                this.assignedApprovers = roomGroup.approvers;
            });
    }

    public updateAutocomplete = (phrase: string = ''): Observable<Partial<IUser>[]> => {
        return this.userService.search(phrase);
    }

    public assignApprover (approver: Partial<IUser>): void {
        this.roomGroupService.assignApprover(this.roomGroup.id, approver.login)
            .pipe(
                catchError((error) => {
                    this.removeApproverFromTagbox(approver);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public deAssignApprover (approver: Partial<IUser>): void {
        this.roomGroupService.deAssignApprover(this.roomGroup.id, approver.login)
            .pipe(
                catchError((error) => {
                    this.addApproverToTagbox(approver);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public clearApprovers (): void {
        this.roomGroupService.clearAllApproverAssigns(this.roomGroup.id)
            .subscribe();
    }

    private addApproverToTagbox (approver: Partial<IUser>): void {
        this.assignedApprovers.push(approver);
    }

    private removeApproverFromTagbox (approver: Partial<IUser>): void {
        this.assignedApprovers = this.assignedApprovers.filter(currApprover => {
            return currApprover.login !== approver.login;
        });
    }

}
