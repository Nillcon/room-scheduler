import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { RoleEnum } from '@Features/role/shared/role.enum';
import { RoomsPageTabEnum } from '../../shared/rooms-page-tab.enum';
import { StorageKey } from '@Core/root/storage/shared/storage-key';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit {

    public readonly roleEnum: typeof RoleEnum = RoleEnum;

    public openedTab: RoomsPageTabEnum;

    constructor (
        private storageService: LocalStorageService
    ) { }

    public ngOnInit (): void {
        this.initOpenedTab();
    }

    public initOpenedTab (): void {
        const openedTab: RoomsPageTabEnum = this.storageService.get<RoomsPageTabEnum>(StorageKey.Rooms_Page_Opened_Tab);

        if (openedTab) {
            this.openedTab = openedTab;
        } else {
            this.openedTab = RoomsPageTabEnum.RoomsList;
        }
    }

    public onSelectedTabChange (): void {
        this.saveOpenedTab(this.openedTab);
    }

    public saveOpenedTab (openedTab: RoomsPageTabEnum): void {
        this.storageService.set(StorageKey.Rooms_Page_Opened_Tab, openedTab);
    }

}
