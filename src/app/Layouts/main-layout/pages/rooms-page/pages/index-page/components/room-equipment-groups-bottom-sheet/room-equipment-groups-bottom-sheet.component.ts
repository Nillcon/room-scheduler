import { Component, OnInit, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { IOnEquipmentTypeGroupChipClick } from '@App/Features/room/interfaces/on-equipment-type-group-chip-click-event.interface';
import { IRoom } from '@App/Features/room';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IEquipmentTypeGroup } from '@App/Features/equipment/interfaces/equipment-type-group.interface';

@ClassValidation()
@Component({
    selector: 'app-room-equipment-groups-bottom-sheet',
    templateUrl: './room-equipment-groups-bottom-sheet.component.html',
    styleUrls: ['./room-equipment-groups-bottom-sheet.component.scss']
})
export class RoomEquipmentGroupsBottomSheetComponent implements OnInit {

    @IsNotEmptyObject()
    public room: IRoom;

    @IsNotEmptyObject()
    public equipmentTypeGroup: IEquipmentTypeGroup;

    constructor (
        @Inject(MAT_BOTTOM_SHEET_DATA) equipmentTypeGroupChipClickEvent: IOnEquipmentTypeGroupChipClick
    ) {
        this.room = equipmentTypeGroupChipClickEvent.room;
        this.equipmentTypeGroup = equipmentTypeGroupChipClickEvent.equipmentTypeGroup;
    }

    public ngOnInit (): void {}

}
