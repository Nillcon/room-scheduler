import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IRoomGroup } from '@App/Features/room-group';

@ClassValidation()
@Component({
    selector: 'app-room-group-approvers-assign-window',
    templateUrl: './room-group-approvers-assign-window.component.html',
    styleUrls: ['./room-group-approvers-assign-window.component.scss']
})
export class RoomGroupApproversAssignWindowComponent implements OnInit {

    @IsNotEmptyObject()
    public roomGroup: IRoomGroup;

    constructor (
        private dialogRef: MatDialogRef<RoomGroupApproversAssignWindowComponent>,
        @Inject(MAT_DIALOG_DATA) roomGroup: IRoomGroup
    ) {
        this.roomGroup = roomGroup;
    }

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

}
