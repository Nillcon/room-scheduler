import { Component, OnInit } from '@angular/core';
import { IRoomGroup } from '@App/Features/room-group';
import { RoomGroupService } from '@App/Features/room-group/room-group.service';

@Component({
    selector: 'app-room-group-assigners-list',
    templateUrl: './room-group-assigners-list.component.html',
    styleUrls: ['./room-group-assigners-list.component.scss']
})
export class RoomGroupAssignersListComponent implements OnInit {

    public roomGroups: IRoomGroup[];

    constructor (
        private roomGroupService: RoomGroupService
    ) {}

    public ngOnInit (): void {
        this.updateRoomGroups();
    }

    public updateRoomGroups (): void {
        this.roomGroupService.getAll()
            .subscribe(roomGroups => {
                this.roomGroups = roomGroups;
            });
    }

    public trackByFunction (index: number, item: IRoomGroup): number {
        return item.id;
    }

}
