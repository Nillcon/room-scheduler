import { Component, OnInit } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import DevExpress from 'devextreme/bundles/dx.all';
import { ActionLogService } from '@App/Features/action-log/action-log.service';

@Component({
    selector: 'app-action-logs-table',
    templateUrl: './action-logs-table.component.html',
    styleUrls: ['./action-logs-table.component.scss']
})
export class ActionLogsTableComponent implements OnInit {

    public dataSource: CustomStore;

    constructor (
        private actionLogService: ActionLogService
    ) {}

    public ngOnInit (): void {
        this.createDataSource();
    }

    public createDataSource (): void {
        this.dataSource = new CustomStore({
            load: (options: DevExpress.data.LoadOptions) => {
                return this.actionLogService.getAllWithOData(options)
                    .toPromise();
            }
        });
    }

}
