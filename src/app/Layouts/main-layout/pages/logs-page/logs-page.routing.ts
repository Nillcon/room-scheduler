import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogsPageComponent } from './logs-page.component';
import { LogsPageGuard } from './guards/logs-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: LogsPageComponent,
        canActivate: [LogsPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class LogsPageRoutingModule {}
