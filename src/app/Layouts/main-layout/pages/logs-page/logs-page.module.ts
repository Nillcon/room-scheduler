import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsPageComponent } from './logs-page.component';
import { LogsPageRoutingModule } from './logs-page.routing';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { ActionLogsTableComponent } from './components/action-logs-table/action-logs-table.component';
import { FeaturesModule } from '@Features/features.module';
import { LogsPageGuard } from './guards/logs-page.guard';

@NgModule({
    declarations: [
        LogsPageComponent,
        ActionLogsTableComponent
    ],
    imports: [
        CommonModule,

        MainLayoutSharedModule,
        FeaturesModule,

        LogsPageRoutingModule
    ],
    providers: [
        LogsPageGuard
    ]
})
export class LogsPageModule {}
