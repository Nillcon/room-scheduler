import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IUser, UserService } from '@App/Features/user';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IsInt } from 'class-validator';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';

@ClassValidation()
@Component({
    selector: 'app-user-editing-window',
    templateUrl: './user-editing-window.component.html',
    styleUrls: ['./user-editing-window.component.scss']
})
export class UserEditingWindowComponent implements OnInit {

    @IsInt()
    public userId: number;

    public user: IUser;

    public savingSubscription: Subscription;

    private userFormGroup: FormGroupTypeSafe<Partial<IUser>>;

    constructor (
        @Inject(MAT_DIALOG_DATA) userId: number,
        private dialogRef: MatDialogRef<UserEditingWindowComponent>,
        private userService: UserService,
        private notifyService: NotificationService
    ) {
        this.userId = userId;
    }

    public ngOnInit (): void {
        this.updateUser();
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onUserFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IUser>>): void {
        this.userFormGroup = formGroup;
    }

    public updateUser (): void {
        this.userService.get(this.userId)
            .subscribe(userData => {
                this.user = userData;
            });
    }

    public editUser (): void {
        if (this.userFormGroup.valid) {
            this.savingSubscription = this.userService.edit(this.user.id, this.userFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'User data updated!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.userFormGroup.markAllAsTouched();
        }
    }

}
