import { Component, OnInit, Input } from '@angular/core';
import { IRole } from '@App/Features/role';
import { RoleService } from '@App/Features/role/role.service';
import { ISvc } from '@App/Features/svc';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IsNotEmptyObject, IsArray } from 'class-validator';

@ClassValidation()
@Component({
    selector: 'app-role-svc-assigner',
    templateUrl: './role-svc-assigner.component.html',
    styleUrls: ['./role-svc-assigner.component.scss']
})
export class RoleSvcAssignerComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public role: IRole;

    @Input()
    @IsArray()
    public svcArray: ISvc[];

    public assignedSvcArray: number[] = [];

    constructor (
        private roleService: RoleService
    ) {}

    public ngOnInit (): void {
        this.updateAssignedServices();
    }

    public updateAssignedServices (): void {
        this.roleService.getServices(this.role.id)
            .subscribe((services) => {
                this.assignedSvcArray = services;
            });
    }

    public assignSvc (svcId: number): void {
        this.roleService.assignService(this.role.id, svcId)
            .pipe(
                catchError((error) => {
                    this.removeSvcFromTagbox(svcId);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public deAssignSvc (svcId: number): void {
        this.roleService.deassignService(this.role.id, svcId)
            .pipe(
                catchError((error) => {
                    this.addSvcToTagbox(svcId);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public clearSvc (roleId: number): void {
        this.roleService.clearServiceAssignments(roleId)
            .subscribe();
    }

    private addSvcToTagbox (svcId: number): void {
        this.assignedSvcArray.push(svcId);
    }

    private removeSvcFromTagbox (svcId: number): void {
        this.assignedSvcArray = this.assignedSvcArray.filter(currSvc => {
            return currSvc !== svcId;
        });
    }

}
