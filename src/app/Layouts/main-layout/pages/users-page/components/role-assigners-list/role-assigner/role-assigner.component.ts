import { Component, OnInit, Input } from '@angular/core';
import { IsInt } from 'class-validator';
import { IUser, UserService } from '@App/Features/user';
import { tap, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { RoleService } from '@App/Features/role/role.service';
import { MatDialog } from '@angular/material';
import { RoleServicesAssignWindowComponent } from '../role-services-assign-window/role-services-assign-window.component';
import { IRole } from '@App/Features/role';

@Component({
    selector: 'app-role-assigner',
    templateUrl: './role-assigner.component.html',
    styleUrls: ['./role-assigner.component.scss']
})
export class RoleAssignerComponent implements OnInit {

    @Input()
    @IsInt()
    public roleId: number;

    @Input() public assignedUsers: Partial<IUser>[];

    public autocompleteUsers: Partial<IUser>[] = [];

    constructor (
        private dialog: MatDialog,
        private roleService: RoleService,
        private userService: UserService
    ) {}

    public ngOnInit (): void {}

    public assignUser (user: IUser): void {
        this.roleService.assignUser(this.roleId, user)
            .pipe(
                tap(() => {
                    this.updateAutocomplete();
                }),
                catchError((error) => {
                    this.removeUserFromTagbox(user);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public deAssignUser (user: IUser): void {
        this.roleService.deassignUser(this.roleId, user)
            .pipe(
                catchError((error) => {
                    this.addUserToTagbox(user);

                    return throwError(error);
                })
            )
            .subscribe();
    }

    public clearUsers (roleId: number): void {
        this.roleService.clearUserAssignments(roleId)
            .subscribe();
    }

    public updateAutocomplete = (phrase: string = ''): Observable<Partial<IUser>[]> => {
        return this.userService.search(phrase);
    }

    public openChangeSvcWindow (role: IRole): Observable<boolean> {
        return this.dialog.open(
            RoleServicesAssignWindowComponent,
            {
                width: '400px',
                data: role
            }
        )
        .afterClosed();
    }

    public onChangeSvcButtonClick (role: IRole): void {
        this.openChangeSvcWindow(role)
            .subscribe();
    }

    private addUserToTagbox (user: IUser): void {
        this.assignedUsers.push(user);
    }

    private removeUserFromTagbox (user: IUser): void {
        this.assignedUsers = this.assignedUsers.filter(currUser => {
            return currUser.login !== user.login;
        });
    }

}
