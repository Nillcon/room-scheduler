import { Component, OnInit } from '@angular/core';
import { RoleEnum } from '@App/Features/role';
import { RoleService } from '@App/Features/role/role.service';
import { IUser } from '@App/Features/user';

@Component({
    selector: 'app-role-assigners-list',
    templateUrl: './role-assigners-list.component.html',
    styleUrls: ['./role-assigners-list.component.scss']
})
export class RoleAssignersListComponent implements OnInit {

    public roleMap: Map<RoleEnum, Partial<IUser>[]>;

    constructor (
        private roleService: RoleService
    ) {}

    public ngOnInit (): void {
        this.updateRoles();
    }

    public updateRoles (): void {
        this.roleService.getUsersByAllRoles()
            .subscribe(roles => {
                this.roleMap = roles;
            });
    }

}
