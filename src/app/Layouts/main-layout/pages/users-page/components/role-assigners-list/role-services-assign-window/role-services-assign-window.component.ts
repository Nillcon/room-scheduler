import { Component, OnInit, Inject } from '@angular/core';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IsNotEmptyObject } from 'class-validator';
import { ISvc } from '@App/Features/svc';
import { IRole } from '@App/Features/role';
import { SvcService } from '@App/Features/svc/svc.service';

@ClassValidation()
@Component({
    selector: 'app-role-services-assign-window',
    templateUrl: './role-services-assign-window.component.html',
    styleUrls: ['./role-services-assign-window.component.scss']
})
export class RoleServicesAssignWindowComponent implements OnInit {

    @IsNotEmptyObject()
    public role: IRole;

    public svcArray: ISvc[];

    constructor (
        private dialogRef: MatDialogRef<RoleServicesAssignWindowComponent>,
        @Inject(MAT_DIALOG_DATA) role: IRole,
        private svcService: SvcService
    ) {
        this.role = role;
    }

    public ngOnInit (): void {
        this.updateServices();
    }

    public updateServices (): void {
        this.svcService.getAll()
            .subscribe((svcArray) => {
                this.svcArray = svcArray;
            });
    }

    public close (result: boolean = false): void {
        this.dialogRef.close(result);
    }

}
