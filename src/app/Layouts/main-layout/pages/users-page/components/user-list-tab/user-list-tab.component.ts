import { Component, OnInit, Injector } from '@angular/core';
import { IUser, UserService } from '@App/Features/user';
import { Observable } from 'rxjs';
import { tap, filter, switchMap } from 'rxjs/operators';
import { UserChangePasswordWindowComponent } from '../user-change-password-window/user-change-password-window.component';
import { UserCreationWindowComponent } from '../user-creation-window/user-creation-window.component';
import { UserEditingWindowComponent } from '../user-editing-window/user-editing-window.component';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-user-list-tab',
  templateUrl: './user-list-tab.component.html',
  styleUrls: ['./user-list-tab.component.scss']
})
export class UserListTabComponent implements OnInit {

    public users: IUser[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private userService: UserService
    ) { }

    public ngOnInit (): void {
        this.updateUsers()
            .subscribe();
    }

    public updateUsers (): Observable<IUser[]> {
        return this.userService.getAll()
            .pipe(
                tap(data => {
                    this.users = data;
                })
            );
    }

    public onAddButtonClick (): void {
        this.dialog.open(UserCreationWindowComponent, {
            width: '500px'
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                switchMap(() => {
                    return this.updateUsers();
                })
            )
            .subscribe();
    }

    public onUserChangePassword (user: IUser): void {
        this.dialog.open(UserChangePasswordWindowComponent, {
            width: '400px',
            data: user
        });
    }

    public onUserEdit (user: IUser): void {
        this.dialog.open(UserEditingWindowComponent,
            {
                width: '500px',
                data: user.id
            })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                switchMap(() => {
                    return this.updateUsers();
                })
            )
            .subscribe();
    }

    @NeedsConfirmation('Do you really want to delete user?', 'Confirm deleting')
    public onUserDelete (user: IUser): void {
        this.userService.delete(user.id)
            .pipe(
                switchMap(() => {
                    return this.updateUsers();
                })
            )
            .subscribe();
    }

}
