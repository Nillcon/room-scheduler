import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IUser, IChangePasswordForm, UserService } from '@App/Features/user';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription } from 'rxjs';
import { NotificationService } from '@Core/root/notification/notification.service';
import { tap } from 'rxjs/operators';

@ClassValidation()
@Component({
    selector: 'app-user-change-password-window',
    templateUrl: './user-change-password-window.component.html',
    styleUrls: ['./user-change-password-window.component.scss']
})
export class UserChangePasswordWindowComponent implements OnInit {

    @IsNotEmptyObject()
    public user: IUser;

    public savingSubscription: Subscription;

    private changePasswordFormGroup: FormGroupTypeSafe<IChangePasswordForm>;

    constructor (
        @Inject(MAT_DIALOG_DATA) userData: IUser,
        private dialogRef: MatDialogRef<UserChangePasswordWindowComponent>,
        private notifyService: NotificationService,
        private userService: UserService
    ) {
        this.user = userData;
    }

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onChangePasswordFormInit (form: FormGroupTypeSafe<IChangePasswordForm>): void {
        this.changePasswordFormGroup = form;
    }

    public changePassword (): void {
        if (this.changePasswordFormGroup.valid) {
            this.savingSubscription = this.userService.changePassword(this.user.id, this.changePasswordFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Password changed!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.changePasswordFormGroup.markAllAsTouched();
        }
    }

}
