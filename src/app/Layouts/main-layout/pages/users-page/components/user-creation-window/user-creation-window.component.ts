import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { IUser } from '@App/Features/user/interfaces/user.interface';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { tap } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';
import { UserService } from '@App/Features/user';

@Component({
    selector: 'app-user-creation-window',
    templateUrl: './user-creation-window.component.html',
    styleUrls: ['./user-creation-window.component.scss']
})
export class UserCreationWindowComponent implements OnInit {

    public creationSubscription: Subscription;

    private userFormGroup: FormGroupTypeSafe<Partial<IUser>>;

    constructor (
        private dialogRef: MatDialogRef<UserCreationWindowComponent>,
        private userService: UserService,
        private notifyService: NotificationService
    ) {}

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onUserFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IUser>>): void {
        this.userFormGroup = formGroup;
    }

    public createUser (): void {
        if (this.userFormGroup.valid) {
            this.creationSubscription = this.userService.create(this.userFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'User created!' });
                        this.close(true);
                    }),
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.userFormGroup.markAllAsTouched();
        }
    }

}
