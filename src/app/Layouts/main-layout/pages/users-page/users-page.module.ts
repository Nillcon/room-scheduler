import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersPageComponent } from './users-page.component';
import { SharedModule } from '@App/app-shared.module';
import { UsersPageRoutingModule } from './users-page.routing';
import { FeaturesModule } from '@Features/features.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { UserCreationWindowComponent } from './components/user-creation-window/user-creation-window.component';
import { UserChangePasswordWindowComponent } from './components/user-change-password-window/user-change-password-window.component';
import { UserEditingWindowComponent } from './components/user-editing-window/user-editing-window.component';
import { UserListTabComponent } from './components/user-list-tab/user-list-tab.component';

import { RoleAssignerComponent } from './components/role-assigners-list/role-assigner/role-assigner.component';
import { RoleAssignersListComponent } from './components/role-assigners-list/role-assigners-list.component';
import { RoleServicesAssignWindowComponent } from './components/role-assigners-list/role-services-assign-window/role-services-assign-window.component';
import { RoleSvcAssignerComponent } from './components/role-assigners-list/role-services-assign-window/role-svc-assigner/role-svc-assigner.component';
import { UsersPageGuard } from './guards/users-page.guard';

@NgModule({
    declarations: [
        UsersPageComponent,
        UserCreationWindowComponent,
        UserEditingWindowComponent,
        UserChangePasswordWindowComponent,
        UserListTabComponent,

        RoleAssignerComponent,
        RoleAssignersListComponent,
        RoleServicesAssignWindowComponent,
        RoleSvcAssignerComponent
    ],
    imports: [
        CommonModule,

        FeaturesModule,
        SharedModule,

        UsersPageRoutingModule,
        MainLayoutSharedModule
    ],
    entryComponents: [
        UserCreationWindowComponent,
        UserEditingWindowComponent,
        UserChangePasswordWindowComponent,
        RoleServicesAssignWindowComponent
    ],
    providers: [
        UsersPageGuard
    ]
})
export class UsersPageModule {}
