import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersPageComponent } from './users-page.component';
import { UsersPageGuard } from './guards/users-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: UsersPageComponent,
        canActivate: [UsersPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class UsersPageRoutingModule {}
