import { Component, OnInit, Injector } from '@angular/core';
import { UserPageTabEnum } from './shared/user-page-tab.enum';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';

@Component({
    selector: 'app-users-page',
    templateUrl: './users-page.component.html',
    styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

    public openedTab: UserPageTabEnum;

    constructor (
        private storageService: LocalStorageService
    ) {}

    public ngOnInit (): void {
        this.initOpenedTab();
    }

    public initOpenedTab (): void {
        const openedTab: UserPageTabEnum = this.storageService.get<UserPageTabEnum>(StorageKey.User_Page_Opened_Tab);

        if (openedTab) {
            this.openedTab = openedTab;
        } else {
            this.openedTab = UserPageTabEnum.UsersList;
        }
    }

    public onSelectedTabChange (): void {
        this.saveOpenedTab(this.openedTab);
    }

    public saveOpenedTab (openedTab: UserPageTabEnum): void {
        this.storageService.set(StorageKey.User_Page_Opened_Tab, openedTab);
    }
}
