import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentPageComponent } from './equipment-page.component';
import { EquipmentPageRoutingModule } from './equipment-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { EquipmentCreationWindowComponent } from './components/equipment-creation-window/equipment-creation-window.component';
import { EquipmentEditingWindowComponent } from './components/equipment-editing-window/equipment-editing-window.component';
import { EquipmentPageGuard } from './guards/equipment-page.guard';

@NgModule({
    declarations: [
        EquipmentPageComponent,
        EquipmentCreationWindowComponent,
        EquipmentEditingWindowComponent
    ],
    imports: [
        CommonModule,
        SharedModule,

        EquipmentPageRoutingModule,
        SharedModule,
        FeaturesModule,
        MainLayoutSharedModule
    ],
    entryComponents: [
        EquipmentCreationWindowComponent,
        EquipmentEditingWindowComponent
    ],
    providers: [
        EquipmentPageGuard
    ]
})
export class EquipmentPageModule { }
