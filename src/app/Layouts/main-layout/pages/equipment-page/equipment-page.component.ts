import { Component, OnInit, Injector } from '@angular/core';
import { IEquipment } from '@Features/equipment';
import { MatDialog } from '@angular/material';
import { EquipmentCreationWindowComponent } from './components/equipment-creation-window/equipment-creation-window.component';
import { filter, tap } from 'rxjs/operators';
import { EquipmentEditingWindowComponent } from './components/equipment-editing-window/equipment-editing-window.component';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { EquipmentService } from '@Features/equipment/equipment.service';

@Component({
    selector: 'app-equipment-page',
    templateUrl: './equipment-page.component.html',
    styleUrls: ['./equipment-page.component.scss']
})
export class EquipmentPageComponent implements OnInit {

    public equipment: IEquipment[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private equipmentService: EquipmentService
    ) {}

    public ngOnInit (): void {
        this.updateEquipment();
    }

    public updateEquipment (): void {
        this.equipmentService.getAll()
            .subscribe(res => this.equipment = res);
    }

    public onAddButtonClick (): void {
        this.dialog.open(EquipmentCreationWindowComponent, {
            width: '500px'
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                tap(() => this.updateEquipment())
            )
            .subscribe();
    }

    public onEquipmentEdit (equipment: IEquipment): void {
        this.dialog.open(EquipmentEditingWindowComponent, {
            width: '500px',
            data: equipment.id
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                tap(() => this.updateEquipment())
            )
            .subscribe();
    }

    @NeedsConfirmation('Do you really want to delete the equipment?', 'Confirm deleting')
    public onEquipmentDelete (equipment: IEquipment): void {
        this.equipmentService.delete(equipment.id)
            .pipe(
                tap(() => this.updateEquipment())
            )
            .subscribe();
    }

}
