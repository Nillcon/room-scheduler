import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EquipmentPageComponent } from './equipment-page.component';
import { EquipmentPageGuard } from './guards/equipment-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: EquipmentPageComponent,
        canActivate: [EquipmentPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class EquipmentPageRoutingModule {}
