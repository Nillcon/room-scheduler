import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { tap } from 'rxjs/operators';
import { IEquipment } from '@App/Features/equipment';
import { NotificationService } from '@Core/root/notification/notification.service';
import { EquipmentService } from '@App/Features/equipment/equipment.service';

@Component({
    selector: 'app-equipment-creation-window',
    templateUrl: './equipment-creation-window.component.html',
    styleUrls: ['./equipment-creation-window.component.scss']
})
export class EquipmentCreationWindowComponent implements OnInit {

    public creationSubscription: Subscription;

    private equipmentFormGroup: FormGroupTypeSafe<Partial<IEquipment>>;

    constructor (
        private dialogRef: MatDialogRef<EquipmentCreationWindowComponent>,
        private equipmentService: EquipmentService,
        private notifyService: NotificationService
    ) {}

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onEquipmentFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IEquipment>>): void {
        this.equipmentFormGroup = formGroup;
    }

    public createEquipment (): void {
        if (this.equipmentFormGroup.valid) {
            this.creationSubscription = this.equipmentService.create(this.equipmentFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Equipment created!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
        }
    }

}
