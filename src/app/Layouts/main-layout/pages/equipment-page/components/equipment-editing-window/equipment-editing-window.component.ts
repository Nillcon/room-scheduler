import { Component, OnInit, Inject } from '@angular/core';
import { IEquipment } from '@App/Features/equipment';
import { IsInt } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NotificationService } from '@Core/root/notification/notification.service';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { EquipmentService } from '@App/Features/equipment/equipment.service';

@ClassValidation()
@Component({
    selector: 'app-equipment-editing-window',
    templateUrl: './equipment-editing-window.component.html',
    styleUrls: ['./equipment-editing-window.component.scss']
})
export class EquipmentEditingWindowComponent implements OnInit {

    @IsInt()
    public equipmentId: number;

    public equipment: IEquipment;

    public savingSubscription: Subscription;

    private equipmentFormGroup: FormGroupTypeSafe<Partial<IEquipment>>;

    constructor (
        @Inject(MAT_DIALOG_DATA) equipmentId: number,
        private dialogRef: MatDialogRef<EquipmentEditingWindowComponent>,
        private notifyService: NotificationService,
        private equipmentService: EquipmentService
    ) {
        this.equipmentId = equipmentId;
    }

    public ngOnInit (): void {
        this.updateEquipment();
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onEquipmentFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IEquipment>>): void {
        this.equipmentFormGroup = formGroup;
    }

    public updateEquipment (): void {
        this.equipmentService.get(this.equipmentId)
            .subscribe((equipment) => {
                this.equipment = equipment;
            });
    }

    public editEquipment (): void {
        if (this.equipmentFormGroup.valid) {
            this.savingSubscription = this.equipmentService.edit(this.equipmentId, this.equipmentFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Equipment saved!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
        }
    }

}
