import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsPageComponent } from './reports-page.component';
import { ReportsPageRoutingModule } from './reports-page.routing';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { RepotsListComponent } from './components/repots-list/repots-list.component';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { ReportDownloadWindowComponent } from './components/report-download-window/report-download-window.component';
import { ReportsPageGuard } from './guards/reports-page.guard';

@NgModule({
    declarations: [
        ReportsPageComponent,
        RepotsListComponent,
        ReportDownloadWindowComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FeaturesModule,

        MainLayoutSharedModule,

        ReportsPageRoutingModule
    ],
    entryComponents: [
        ReportDownloadWindowComponent
    ],
    providers: [
        ReportsPageGuard
    ]
})
export class ReportsPageModule { }
