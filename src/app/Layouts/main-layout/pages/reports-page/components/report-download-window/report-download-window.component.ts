import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IReport } from '@App/Features/report/interfaces/report.interface';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';
import { Observable, of } from 'rxjs';
import { tap, delayWhen } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';
import { DateService } from '@Core/root/date/date.service';
import { ReportService } from '@App/Features/report/report.service';

@Component({
    selector: 'app-report-download-window',
    templateUrl: './report-download-window.component.html',
    styleUrls: ['./report-download-window.component.scss']
})
export class ReportDownloadWindowComponent implements OnInit {

    public dateRange: IDateRange;

    constructor (
        public dialogRef: MatDialogRef<ReportDownloadWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public report: IReport,
        private reportService: ReportService,
        private notificationService: NotificationService,
        private dateService: DateService
    ) {}

    public ngOnInit (): void {
        this.initDateRange();
    }

    public downloadReport (): Observable<Blob> {
        return this.reportService.download(this.report.type, this.dateRange);
    }

    public onDownloadClick (): void {
        of(
            this.notificationService.infoToast({
                title: 'Download',
                text: 'Report is downloading...',
                duration: Infinity
            })
        )
            .pipe(
                delayWhen(() => {
                    return this.downloadReport();
                }),
                tap((notification) => {
                    notification.Close();
                    this.close(true);
                })
            )
            .subscribe();
    }

    private initDateRange (): void {
        this.dateRange = {
            from: new Date(),
            to: this.dateService.plusDays(new Date(), 7)
        };
    }

    private close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
