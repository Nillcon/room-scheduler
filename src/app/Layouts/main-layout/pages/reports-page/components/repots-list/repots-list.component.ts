import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IReport } from '@App/Features/report/interfaces/report.interface';
import { tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { ReportDownloadWindowComponent } from '../report-download-window/report-download-window.component';
import { ReportService } from '@App/Features/report/report.service';

@Component({
  selector: 'app-repots-list',
  templateUrl: './repots-list.component.html',
  styleUrls: ['./repots-list.component.scss']
})
export class RepotsListComponent implements OnInit {

    public reports: IReport[];

    constructor (
        private dialog: MatDialog,
        private reportService: ReportService
    ) {}

    public ngOnInit (): void {
        this.updateReports()
            .subscribe();
    }

    public updateReports (): Observable<IReport[]> {
        return this.reportService.getAll()
            .pipe(
                tap(res => this.reports = res)
            );
    }

    public openReportDownloadWindow (report: IReport): Observable<any> {
        const dialog = this.dialog.open(ReportDownloadWindowComponent, {
            data: report
        });

        return dialog.afterClosed();
    }

    public onReportListSelected (report: IReport): void {
        this.openReportDownloadWindow(report)
            .subscribe();
    }

}
