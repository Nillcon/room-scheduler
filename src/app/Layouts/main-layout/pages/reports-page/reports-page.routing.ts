import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsPageComponent } from './reports-page.component';
import { ReportsPageGuard } from './guards/reports-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: ReportsPageComponent,
        canActivate: [ReportsPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReportsPageRoutingModule {}
