import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsPageComponent } from './events-page.component';
import { EventsPageGuard } from './guards/events-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: EventsPageComponent,
        canActivate: [EventsPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class EventsPageRoutingModule {}
