import { IRequestEvent } from '@App/Features/request-event';
import { KeyValue } from '@angular/common';
import { IEquipment } from '@App/Features/equipment';

export interface IRequestEventEquipmentDetails {
    requestEvent: IRequestEvent;
    equipmentGroup: KeyValue<number, IEquipment[]>;
}
