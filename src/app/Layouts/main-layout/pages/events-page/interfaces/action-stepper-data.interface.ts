import { IRoomBookingForm } from '@App/Features/room/interfaces/booking-form.interface';
import { IRequestEventGeneralInfoForm } from '@App/Features/request-event';

export interface IActionStepperData {
    bookingFormData: Partial<IRoomBookingForm>;
    selectedRoomIds: string[];
    selectedServiceIds: number[];
    generalInfoFormData: Partial<IRequestEventGeneralInfoForm>;
}
