import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsPageComponent } from './events-page.component';
import { EventsPageRoutingModule } from './events-page.routing';
import { FeaturesModule } from '@Features/features.module';
import { SharedModule } from '@App/app-shared.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { EventsTableComponent } from './components/events-tab/events-table/events-table.component';
import { EventCacheStatusComponent } from './components/event-cache-status/event-cache-status.component';
import { EventsTabComponent } from './components/events-tab/events-tab.component';
import { RequestEventsTabComponent } from './components/request-events-tab/request-events-tab.component';
import { RequestEventCreationWindowComponent } from './components/request-events-tab/request-event-creation-window/request-event-creation-window.component';
import { RequestEventActionStepperComponent } from './components/request-events-tab/request-event-action-stepper/request-event-action-stepper.component';
import { FilteredRoomsListSelectorComponent } from './components/request-events-tab/request-event-action-stepper/filtered-rooms-list-selector/filtered-rooms-list-selector.component';
import { SummaryInfoComponent } from './components/request-events-tab/request-event-action-stepper/summary-info/summary-info.component';
import { RequestEventEditingWindowComponent } from './components/request-events-tab/request-event-editing-window/request-event-editing-window.component';
import { SvcListSelectorComponent } from './components/request-events-tab/request-event-action-stepper/svc-list-selector/svc-list-selector.component';
import { MatStepperModule } from '@angular/material';
import { RequestEventChangePriceWindowComponent } from './components/request-events-tab/request-event-change-price-window/request-event-change-price-window.component';
import { RequestEventsListTableComponent } from './components/request-events-tab/request-events-list-table/request-events-list-table.component';
import { InvoicesPriceTableComponent } from './components/request-events-tab/request-event-change-price-window/invoices-price-table/invoices-price-table.component';
import { EventsPageGuard } from './guards/events-page.guard';
import { RequestEventCloneWindowComponent } from './components/request-events-tab/request-event-clone-window/request-event-clone-window.component';
import { EquipmentDetailsBottomSheetComponent } from './components/request-events-tab/request-events-list-table/equipment-details-bottom-sheet/equipment-details-bottom-sheet.component';
import { RequestEventApprovingWindowComponent } from './components/request-events-tab/request-event-approving-window/request-event-approving-window.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';
import { RoomDetailsApprovingWindowComponent } from './components/request-events-tab/room-details-approving-window/room-details-approving-window.component';

@NgModule({
    declarations: [
        EventsPageComponent,
        EventsTableComponent,
        EventCacheStatusComponent,
        EventsTabComponent,
        RequestEventsTabComponent,

        RequestEventCreationWindowComponent,
        RequestEventEditingWindowComponent,
        RequestEventActionStepperComponent,
        FilteredRoomsListSelectorComponent,
        SummaryInfoComponent,
        SvcListSelectorComponent,
        RequestEventChangePriceWindowComponent,
        RequestEventsListTableComponent,
        InvoicesPriceTableComponent,
        RequestEventCloneWindowComponent,
        EquipmentDetailsBottomSheetComponent,
        RequestEventApprovingWindowComponent,
        RoomDetailsApprovingWindowComponent
    ],
    imports: [
        CommonModule,

        EventsPageRoutingModule,

        SharedModule,
        FeaturesModule,
        FeaturesSharedModule,
        MainLayoutSharedModule,

        MatStepperModule
    ],
    entryComponents: [
        RequestEventCreationWindowComponent,
        RequestEventEditingWindowComponent,
        RequestEventCloneWindowComponent,
        RequestEventChangePriceWindowComponent,
        EquipmentDetailsBottomSheetComponent,
        RequestEventApprovingWindowComponent,
        RoomDetailsApprovingWindowComponent
    ],
    providers: [
        EventsPageGuard
    ]
})
export class EventsPageModule { }
