import { Component, OnInit, Inject } from '@angular/core';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { IEquipment } from '@App/Features/equipment';
import { IRequestEventEquipmentDetails } from '@Layouts/main-layout/pages/events-page/interfaces/request-event-equpments-details.interface';
import { IRequestEvent } from '@App/Features/request-event';
import { KeyValue } from '@angular/common';

@Component({
  selector: 'app-equipment-details-bottom-sheet',
  templateUrl: './equipment-details-bottom-sheet.component.html',
  styleUrls: ['./equipment-details-bottom-sheet.component.scss']
})
export class EquipmentDetailsBottomSheetComponent implements OnInit {

    public requestEvent: IRequestEvent;
    public requestEventEquipment: KeyValue<number, IEquipment[]>;

    constructor (
        @Inject(MAT_BOTTOM_SHEET_DATA) requestEventEquipmentDetails: IRequestEventEquipmentDetails
    ) {
        this.requestEvent = requestEventEquipmentDetails.requestEvent;
        this.requestEventEquipment = requestEventEquipmentDetails.equipmentGroup;
    }

    public ngOnInit (): void {
    }

}
