import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRequestEventApprovingForm } from '@App/Features/request-event/interfaces/approving-form.interface';
import { NotificationService } from '@Core/root/notification/notification.service';
import { tap } from 'rxjs/operators';
import { RequestEventService } from '@App/Features/request-event/request-event.service';

@Component({
    selector: 'app-request-event-approving-window',
    templateUrl: './request-event-approving-window.component.html',
    styleUrls: ['./request-event-approving-window.component.scss']
})
export class RequestEventApprovingWindowComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<IRequestEventApprovingForm>;

    public savingSubscription: Subscription;

    constructor (
        @Inject(MAT_DIALOG_DATA) public requestEventId: number,
        private dialogRef: MatDialogRef<RequestEventApprovingWindowComponent>,
        private requestEventService: RequestEventService,
        private notificationService: NotificationService
    ) {}

    public ngOnInit (): void {}

    public save (): void {
        if (this.formGroup.valid) {
            this.savingSubscription = this.requestEventService.changeStatus(
                this.requestEventId,
                this.formGroup.value.status
            )
                .pipe(
                    tap(() => {
                        this.close(true);
                        this.notificationService.success({});
                    })
                )
                .subscribe();
        } else {
            this.notificationService.error({ text: 'Form isn\'t valid!' });
        }
    }

    public onFormGroupInit (formGroup: FormGroupTypeSafe<IRequestEventApprovingForm>): void {
        this.formGroup = formGroup;
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

}
