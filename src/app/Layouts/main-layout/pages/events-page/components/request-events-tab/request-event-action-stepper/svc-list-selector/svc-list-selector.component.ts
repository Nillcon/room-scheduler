import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ISvc } from '@App/Features/svc/interfaces/svc.interface';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';

@ClassValidation()
@Component({
    selector: 'svc-list-selector',
    templateUrl: './svc-list-selector.component.html',
    styleUrls: ['./svc-list-selector.component.scss']
})
export class SvcListSelectorComponent implements OnInit {

    @Input()
    @IsArray()
    public svcArray: ISvc[] = [];

    @Input()
    @IsArray()
    public selectedSvcIds: number[] = [];

    @Output() public OnSvcSelectionChange: EventEmitter<ISvc[]> = new EventEmitter();

    public selectedSvcArray: ISvc[] = [];

    constructor () {}

    public ngOnInit (): void {}

    public onSvcSelectionChanged (selectedSvcArray: ISvc[]): void {
        this.selectedSvcArray = selectedSvcArray;
        this.OnSvcSelectionChange.emit(this.selectedSvcArray);
    }

}
