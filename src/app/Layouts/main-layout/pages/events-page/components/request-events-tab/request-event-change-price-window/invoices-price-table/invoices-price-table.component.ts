import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { IsNumber } from 'class-validator';
import { tap, switchMap } from 'rxjs/operators';
import { ISvc } from '@App/Features/svc/interfaces/svc.interface';
import { IInvoiceService } from '@App/Features/request-event/interfaces/invoice-service.interface';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { SvcService } from '@App/Features/svc/svc.service';
import { RequestEventService } from '@App/Features/request-event/request-event.service';

@ClassValidation()
@Component({
  selector: 'app-invoices-price-table',
  templateUrl: './invoices-price-table.component.html',
  styleUrls: ['./invoices-price-table.component.scss']
})
export class InvoicesPriceTableComponent implements OnInit {

    @Input()
    @IsNumber()
    public requestEventId: number;

    @Output()
    public OnSelection: EventEmitter<ISvc[]> = new EventEmitter();

    @Output()
    public OnInvoiceServiceUpdated: EventEmitter<ISvc> = new EventEmitter();

    public invoiceServiceArray: Partial<IInvoiceService>[];
    public svcArray: ISvc[];

    constructor (
        private requestEventService: RequestEventService,
        private svcService: SvcService
    ) {}

    public ngOnInit (): void {
        this.updateServices()
            .pipe(
                switchMap(() => {
                    return this.updateInvoiceServices();
                })
            )
            .subscribe();
    }

    public updateInvoiceServices (): Observable<IInvoiceService[]> {
        return this.requestEventService.getInvoiceServices(this.requestEventId)
            .pipe(
                tap((invoiceServices) => {
                    this.invoiceServiceArray = invoiceServices;
                })
            );
    }

    public updateServices (): Observable<ISvc[]> {
        return this.svcService.getAll()
            .pipe(
                tap((svcArray) => {
                    this.svcArray = svcArray;
                })
            );
    }

}
