import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { IRequestEvent, ListTableComponent } from '@App/Features/request-event';
import { MatDialog, MatBottomSheet } from '@angular/material';
import { NotificationService } from '@Core/root/notification/notification.service';
import { RequestEventEditingWindowComponent } from '../request-event-editing-window/request-event-editing-window.component';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { tap, filter, switchMap, delayWhen } from 'rxjs/operators';
import { RequestEventChangePriceWindowComponent } from '../request-event-change-price-window/request-event-change-price-window.component';
import { UrlService } from '@Core/root/url/url.service';
import { EventsPageQueryVariableEnum } from '../../../shared/events-page-query-variable.enum';
import { UserService } from '@App/Features/user/user.service';
import { RoleEnum } from '@App/Features/role/shared/role.enum';
import { Observable, of } from 'rxjs';
import { RequestEventCloneWindowComponent } from '../request-event-clone-window/request-event-clone-window.component';
import { EquipmentDetailsBottomSheetComponent } from './equipment-details-bottom-sheet/equipment-details-bottom-sheet.component';
import { IRequestEventEquipmentDetails } from '../../../interfaces/request-event-equpments-details.interface';
import { RequestEventService } from '@App/Features/request-event/request-event.service';
import { IRequestEventDetailsRoom } from '@Features/request-event/interfaces/request-event-details-room.interface';
import { RoomDetailsApprovingWindowComponent } from '../room-details-approving-window/room-details-approving-window.component';
import { ConfirmationWindowService } from '@Modules/UI/confirmation-window/confirmation-window.service';
import { RequestEventTypes } from '@Features/request-event/shared/request-event-types.enum';
import { RequestEventApprovingWindowComponent } from '../request-event-approving-window/request-event-approving-window.component';
import { ToastMessage } from 'angular-bootstrap-toasts/lib/Models/toast-message.models';

@Component({
    selector: 'app-request-events-list-table',
    templateUrl: './request-events-list-table.component.html',
    styleUrls: ['./request-events-list-table.component.scss']
})
export class RequestEventsListTableComponent implements OnInit {

    @ViewChild('requestEventsList', { static: false })
    public requestEventsList: ListTableComponent;

    public requestEventArray: IRequestEvent[];

    public isShowPriceChangeButton: boolean = false;

    public itemIdFromUrl: number;

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private confirmationWindow: ConfirmationWindowService,
        private userService: UserService,
        private requestEventService: RequestEventService,
        private notifyService: NotificationService,
        private urlService: UrlService,
        private bottomSheet: MatBottomSheet
    ) {}

    public ngOnInit (): void {
        this.updateRequestEvents()
            .subscribe();

        this.initItemIdFromUrl();
        this.initPriceChangeButtonVisibleState();
    }

    public updateRequestEvents (): Observable<IRequestEvent[]> {
        return this.requestEventService.getAll()
            .pipe(
                tap(res => this.requestEventArray = res)
            );
    }

    public getDetailsFunction = (requestEventId: number): Observable<IRequestEventDetailsRoom[]> => {
        return this.requestEventService.getDetails(requestEventId);
    }

    public openApprovingWindow (requestEvent: IRequestEvent): Observable<boolean> {
        return this.dialog.open(RequestEventApprovingWindowComponent, {
            width: '500px',
            data: requestEvent.id
        })
        .afterClosed();
    }

    public openRoomApprovingWindow (requestEventId: number, roomId: string): Observable<boolean> {
        return this.dialog.open(RoomDetailsApprovingWindowComponent, {
            width: '500px',
            data: {
                requestEventId: requestEventId,
                roomId: roomId
            }
        })
        .afterClosed();
    }

    public openPriceWindow (requestEvent: IRequestEvent): Observable<boolean> {
        return this.dialog.open(RequestEventChangePriceWindowComponent, {
            width: '800px',
            data: requestEvent.id
        })
        .afterClosed();
    }

    public openCloneWindow (requestEvent: IRequestEvent): Observable<boolean> {
        return this.dialog.open(RequestEventCloneWindowComponent, {
            width: '95%',
            height: '95%',
            disableClose: true,
            data: requestEvent.id
        })
        .afterClosed();
    }

    public openEditWindow (requestEvent: IRequestEvent): Observable<boolean> {
        return this.dialog.open(RequestEventEditingWindowComponent, {
            width: '95%',
            height: '95%',
            disableClose: true,
            data: requestEvent.id
        })
        .afterClosed();
    }

    public openEquipmentBottomSheet (requestEventEquipmentDetails: IRequestEventEquipmentDetails): Observable<boolean> {
        return this.bottomSheet.open(EquipmentDetailsBottomSheetComponent, {
            data: requestEventEquipmentDetails
        })
        .afterDismissed();
    }

    public showApprovingToast (): Observable<ToastMessage> {
        return of(
            this.notifyService.infoToast({
                title: 'Processing...',
                text: 'Changing status for request event...',
                duration: Infinity
            })
        );
    }

    public onChangePriceButtonClick (requestEvent: IRequestEvent): void {
        this.openPriceWindow(requestEvent)
            .pipe(
                filter((result) => result === true),
                delayWhen(() => this.updateRequestEvents())
            )
            .subscribe();
    }

    public onApproveButtonClick (requestEvent: IRequestEvent): void {
        const title: string = this.getRequestEventApprovingTitle(requestEvent);
        const description: string = this.getRequestEventApprovingDescription(requestEvent);

        this.confirmationWindow.show(description, title)
            .pipe(
                filter((result) => !!result),
                switchMap(() => this.showApprovingToast()),
                delayWhen(() => this.invertRequestEventApproving(requestEvent)),
                delayWhen(() => this.updateRequestEvents()),
                tap((toast) => toast.Close())
            )
            .subscribe();
    }

    public onRoomApproveButtonClick (data: {requestEvent: IRequestEvent, room: IRequestEventDetailsRoom}): void {
        const title: string = this.getRequestEventApprovingRoomTitle(data.room);
        const description: string = this.getRequestEventApprovingRoomDescription(data.room);

        this.confirmationWindow.show(description, title)
            .pipe(
                filter((result) => !!result),
                switchMap(() => this.showApprovingToast()),
                delayWhen(() => this.invertRoomApproving(data.room, data.requestEvent)),
                delayWhen(() => this.updateRequestEvents()),
                tap((toast) => toast.Close())
            )
            .subscribe();
    }

    public onRequestEventEquipmentClick (requestEventEquipmentDetails: IRequestEventEquipmentDetails): void {
        this.openEquipmentBottomSheet(requestEventEquipmentDetails);
    }

    public onRequestEventClone (requestEvent: IRequestEvent): void {
        this.openCloneWindow(requestEvent)
            .pipe(
                filter((result) => result === true),
                switchMap(() => this.updateRequestEvents())
            )
            .subscribe();
    }

    public onRequestEventEdit (requestEvent: IRequestEvent): void {
        this.openEditWindow(requestEvent)
            .pipe(
                filter((result) => result === true),
                switchMap(() => this.updateRequestEvents())
            )
            .subscribe();
    }

    public approveRequestEvent (requestEventId: number): Observable<any> {
        return this.requestEventService.changeStatus(requestEventId, RequestEventTypes.Approved);
    }

    public disapproveRequestEvent (requestEventId: number): Observable<any> {
        return this.requestEventService.changeStatus(requestEventId, RequestEventTypes.Cancelled);
    }

    public approveRoom (roomId: string, requestEventId: number): Observable<any> {
        return this.requestEventService.changeRoomStatus(roomId, requestEventId, RequestEventTypes.Approved);
    }

    public disapproveRoom (roomId: string, requestEventId: number): Observable<any> {
        return this.requestEventService.changeRoomStatus(roomId, requestEventId, RequestEventTypes.Cancelled);
    }

    public invertRequestEventApproving (requestEvent: IRequestEvent): Observable<any> {
        if (requestEvent.status === RequestEventTypes.Approved) {
            return this.disapproveRequestEvent(requestEvent.id);
        } else {
            return this.approveRequestEvent(requestEvent.id);
        }
    }

    public invertRoomApproving (room: IRequestEventDetailsRoom, requestEvent: IRequestEvent): Observable<any> {
        if (room.status === RequestEventTypes.Approved) {
            return this.disapproveRoom(room.roomId, requestEvent.id);
        } else {
            return this.approveRoom(room.roomId, requestEvent.id);
        }
    }

    @NeedsConfirmation()
    public deleteRequestEvent (requestEvent: IRequestEvent): void {
        this.requestEventService.delete(requestEvent.id)
            .pipe(
                switchMap(() => this.updateRequestEvents()),
                tap(() => this.notifyService.success({}))
            )
            .subscribe();
    }

    private initItemIdFromUrl (): void {
        const itemIdFromUrl: number = +this.urlService.getParameter(EventsPageQueryVariableEnum.RequestEventId);

        if (itemIdFromUrl) {
            this.itemIdFromUrl = itemIdFromUrl;
        }
    }

    private initPriceChangeButtonVisibleState (): void {
        this.isShowPriceChangeButton = this.userService.includesRole(RoleEnum.Approver);
    }

    private getRequestEventApprovingTitle (requestEvent: IRequestEvent): string {
        const action: string = (requestEvent.status === RequestEventTypes.Approved) ? 'Disapprove' : 'Approve';
        return `${action} request event`;
    }

    private getRequestEventApprovingDescription (requestEvent: IRequestEvent): string {
        const action: string = (requestEvent.status === RequestEventTypes.Approved) ? 'disapprove' : 'approve';
        return `Do you really want ${action} request event?`;
    }

    private getRequestEventApprovingRoomTitle (room: IRequestEventDetailsRoom): string {
        const action: string = (room.status === RequestEventTypes.Approved) ? 'Disapprove' : 'Approve';
        return `${action} room`;
    }

    private getRequestEventApprovingRoomDescription (room: IRequestEventDetailsRoom): string {
        const action: string = (room.status === RequestEventTypes.Approved) ? 'disapprove' : 'approve';
        return `Do you really want ${action} room?`;
    }

}
