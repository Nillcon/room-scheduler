import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef, Output, EventEmitter, Input } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRoomBookingForm } from '@App/Features/room/interfaces/booking-form.interface';
import { Subscription, Observable } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { IRequestEventGeneralInfoForm } from '@App/Features/request-event/interfaces/general-info-form.interface';
import { IRoom } from '@App/Features/room';
import { ISvc } from '@App/Features/svc';
import { MatHorizontalStepper } from '@angular/material';
import { IEquipment } from '@App/Features/equipment';
import { tap, switchMap } from 'rxjs/operators';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { ActionStepperStepEnum } from '../shared/action-stepper-step.enum';
import { IActionStepperData } from '../../../interfaces/action-stepper-data.interface';
import { SvcService } from '@App/Features/svc/svc.service';
import { RoomService } from '@App/Features/room/room.service';
import { EquipmentService } from '@App/Features/equipment/equipment.service';

@ClassValidation()
@AutoUnsubscribe()
@Component({
    selector: 'request-event-action-stepper',
    templateUrl: './request-event-action-stepper.component.html',
    styleUrls: ['./request-event-action-stepper.component.scss']
})
export class RequestEventActionStepperComponent implements OnInit, OnDestroy {

    @Input()
    public bookingFormData: Partial<IRoomBookingForm>;

    @Input()
    @IsArray()
    public selectedRoomIds: string[] = [];

    @Input()
    @IsArray()
    public selectedSvcIds: number[]  = [];

    @Input()
    public generalInfoFormData: Partial<IRequestEventGeneralInfoForm>;

    @Output()
    public OnRruleSummaryChange: EventEmitter<string> = new EventEmitter();

    @Output()
    public OnStepChange: EventEmitter<ActionStepperStepEnum> = new EventEmitter();

    @Output()
    public OnDataChange: EventEmitter<IActionStepperData> = new EventEmitter();

    @Output()
    public OnBookingFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRoomBookingForm>>> = new EventEmitter();

    @Output()
    public OnGeneralInfoFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>> = new EventEmitter();

    public bookingFormGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>;
    public isBookingFormValid: boolean;
    public rruleSummary: string;

    public isRoomsStepCompleted: boolean = false;

    public generalInfoFormGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>;
    public isGeneralInfoFormValid: boolean;

    public data: IActionStepperData;

    public currentStepIndex: ActionStepperStepEnum          = ActionStepperStepEnum.Booking;
    public readonly stepsEnum: typeof ActionStepperStepEnum = ActionStepperStepEnum;

    public equipmentArray: IEquipment[] = [];
    public roomArray: IRoom[];
    public svcArray: ISvc[];

    @ViewChild('MatStepper', { static: false })
    private matStepperComponent: MatHorizontalStepper;

    private bookingFormChangesSubscription: Subscription;
    private generalInfoFormChangesSubscription: Subscription;

    constructor (
        private changeDetectorRef: ChangeDetectorRef,
        private equipmentService: EquipmentService,
        private roomService: RoomService,
        private svcService: SvcService
    ) {}

    public ngOnInit (): void {
        this.updateEquipmentArray()
            .pipe(
                switchMap(() => this.updateRoomArray()),
                switchMap(() => this.updateSvcArray())
            )
            .subscribe();
    }

    public ngOnDestroy (): void {}

    public updateEquipmentArray (): Observable<IEquipment[]> {
        return this.equipmentService.getAll()
            .pipe(
                tap((equipment) => this.equipmentArray = equipment)
            );
    }

    public updateRoomArray (): Observable<IRoom[]> {
        return this.roomService.getAll()
            .pipe(
                tap((rooms) => this.roomArray = rooms)
            );
    }

    public updateSvcArray (): Observable<ISvc[]> {
        return this.svcService.getAllForClients()
            .pipe(
                tap((svcArr) => this.svcArray = svcArr)
            );
    }

    public onBookingFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>): void {
        this.bookingFormGroup = formGroup;

        if (this.bookingFormChangesSubscription) {
            this.bookingFormChangesSubscription.unsubscribe();
        }

        this.bookingFormChangesSubscription = this.bookingFormGroup.statusChanges
            .subscribe(() => {
                this.isBookingFormValid = this.bookingFormGroup.valid;
                this.changeDetectorRef.detectChanges();

                this.emitData();
            });

        setTimeout(() => {
            this.isBookingFormValid = formGroup.valid;
            this.emitData();
        });

        this.OnBookingFormInit.emit(this.bookingFormGroup);
    }

    public onGeneralInformationFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>): void {
        this.generalInfoFormGroup = formGroup;

        if (this.generalInfoFormChangesSubscription) {
            this.generalInfoFormChangesSubscription.unsubscribe();
        }

        this.generalInfoFormChangesSubscription = this.generalInfoFormGroup.statusChanges
            .subscribe(() => {
                this.isGeneralInfoFormValid = this.generalInfoFormGroup.valid;
                this.changeDetectorRef.detectChanges();

                this.emitData();
            });

        setTimeout(() => {
            this.isGeneralInfoFormValid = formGroup.valid;
            this.emitData();
        });

        this.OnGeneralInfoFormInit.emit(this.generalInfoFormGroup);
    }

    public onRruleSummaryChanged (rruleSummary: string): void {
        this.rruleSummary = rruleSummary;
        this.OnRruleSummaryChange.emit(this.rruleSummary);
    }

    public onRoomsSelectionChanged (rooms: IRoom[]): void {
        this.selectedRoomIds = rooms.map((room) => {
            return room.id;
        });
        this.isRoomsStepCompleted = (this.selectedRoomIds.length > 0);
        this.changeDetectorRef.detectChanges();

        this.emitData();
    }

    public onSvcSelectionChanged (svcArray: ISvc[]): void {
        this.selectedSvcIds = svcArray.map((svc) => {
            return svc.id;
        });
        this.changeDetectorRef.detectChanges();

        this.emitData();
    }

    public emitData (): void {
        this.data = {
            bookingFormData: this.bookingFormGroup.value,
            selectedRoomIds: this.getSelectedRoomIdsBasedRoomArray(),
            selectedServiceIds: this.selectedSvcIds,
            generalInfoFormData: this.generalInfoFormGroup.value
        };

        this.OnDataChange.emit(this.data);
    }

    public nextStep (): void {
        this.matStepperComponent.next();
    }

    public previousStep (): void {
        this.matStepperComponent.previous();
    }

    public goToStep (stepIndex: ActionStepperStepEnum): void {
        this.matStepperComponent.selectedIndex = stepIndex;
    }

    public onStepChange (event: StepperSelectionEvent): void {
        this.currentStepIndex = event.selectedIndex;
        this.OnStepChange.emit(this.currentStepIndex);
    }

    private getSelectedRoomIdsBasedRoomArray (): string[] {
        const roomArrayIds: string[] = (this.roomArray || []).map((currRoom) => {
            return currRoom.id;
        });

        return this.selectedRoomIds.filter((currRoomId) => {
            return roomArrayIds.includes(currRoomId);
        });
    }

}
