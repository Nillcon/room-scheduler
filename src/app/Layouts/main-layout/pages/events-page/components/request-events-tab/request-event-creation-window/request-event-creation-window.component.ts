import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ActionStepperStepEnum } from '../shared/action-stepper-step.enum';
import { Subscription, forkJoin, Observable } from 'rxjs';
import { IRequestEventGeneralInfoForm } from '@App/Features/request-event';
import { IActionStepperData } from '../../../interfaces/action-stepper-data.interface';
import { tap, delayWhen } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRoomBookingForm } from '@App/Features/room/interfaces/booking-form.interface';
import { RequestEventActionStepperComponent } from '../request-event-action-stepper/request-event-action-stepper.component';
import { RequestEventTypes } from '@App/Features/request-event/shared/request-event-types.enum';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { RequestEventService } from '@App/Features/request-event/request-event.service';

@Component({
    selector: 'app-request-event-creation-window',
    templateUrl: './request-event-creation-window.component.html',
    styleUrls: ['./request-event-creation-window.component.scss']
})
export class RequestEventCreationWindowComponent implements OnInit {

    public currentStep: ActionStepperStepEnum = ActionStepperStepEnum.Booking;
    public readonly stepsEnum: typeof ActionStepperStepEnum = ActionStepperStepEnum;

    public data: IActionStepperData;

    public creationSubscription: Subscription;

    @ViewChild('ActionStepper', { static: false })
    private actionStepperComponent: RequestEventActionStepperComponent;

    private bookingFormGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>;
    private generalInfoFormGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>;

    constructor (
        public injector: Injector,
        private dialogRef: MatDialogRef<RequestEventCreationWindowComponent>,
        private requestEventService: RequestEventService,
        private notificationService: NotificationService
    ) {}

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onMainButtonClick (): void {
        if (this.currentStep === ActionStepperStepEnum.GeneralInfo) {
            this.onCreateButtonClick();
        } else {
            this.nextStep();
        }
    }

    public onSecondaryButtonClick (): void {
        this.previousStep();
    }

    public onStepChanged (stepIndex: ActionStepperStepEnum): void {
        this.currentStep = stepIndex;
    }

    public onDataChanged (data: IActionStepperData): void {
        this.data = data;
    }

    public onBookingFormStateChanged (formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>): void {
        this.bookingFormGroup = formGroup;
    }

    public onGeneralInfoFormStateChanged (formGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>): void {
        this.generalInfoFormGroup = formGroup;
    }

    public nextStep (): void {
        this.actionStepperComponent.nextStep();
    }

    public previousStep (): void {
        this.actionStepperComponent.previousStep();
    }

    @NeedsConfirmation(
        `Do you really want to just save this Request Event as a <strong>draft</strong>?
        <b style="color: red;">Please note that your Request Event will not be processed by Approver.</b>`
    )
    public onCreateButtonClick (): void {
        if (this.bookingFormGroup.valid && this.generalInfoFormGroup.valid) {
            this.creationSubscription = this.createRequestEvent()
                .pipe(
                    tap(() => {
                        this.close(true);
                        this.notificationService.success({ text: 'Request event created!' });
                    })
                )
                .subscribe();
        } else {
            this.bookingFormGroup.markAllAsTouched();
            this.generalInfoFormGroup.markAllAsTouched();

            this.notificationService.error({ text: 'Some fields aren\'t valid!' });
        }
    }

    public onSubmitForApprovalButtonClick (): void {
        if (this.bookingFormGroup.valid && this.generalInfoFormGroup.valid) {
            this.creationSubscription = this.createRequestEvent()
                .pipe(
                    delayWhen((requestEventId) => {
                        return this.requestEventService.changeStatus(
                            requestEventId,
                            RequestEventTypes.PendingApproval
                        );
                    }),
                    tap(() => {
                        this.close(true);
                        this.notificationService.success({ text: 'Request event submitted for Approval!' });
                    })
                )
                .subscribe();
        } else {
            this.bookingFormGroup.markAllAsTouched();
            this.generalInfoFormGroup.markAllAsTouched();

            this.notificationService.error({ text: 'Some fields aren\'t valid!' });
        }
    }

    public createRequestEvent (): Observable<number> {
        return this.requestEventService.create(this.data.generalInfoFormData)
            .pipe(
                delayWhen((requestEventId) => {
                    return forkJoin(
                        this.requestEventService.assignFilters(requestEventId, this.data.bookingFormData),
                        this.requestEventService.assignRooms(requestEventId, this.data.selectedRoomIds),
                        this.requestEventService.assignServices(requestEventId, this.data.selectedServiceIds)
                    );
                })
            );
    }

}
