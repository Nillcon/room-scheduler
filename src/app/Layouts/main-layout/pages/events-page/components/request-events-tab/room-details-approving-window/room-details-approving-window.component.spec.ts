import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomDetailsApprovingWindowComponent } from './room-details-approving-window.component';

describe('RoomDetailsApprovingWindowComponent', () => {
  let component: RoomDetailsApprovingWindowComponent;
  let fixture: ComponentFixture<RoomDetailsApprovingWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomDetailsApprovingWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomDetailsApprovingWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
