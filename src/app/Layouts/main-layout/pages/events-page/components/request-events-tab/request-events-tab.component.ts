import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IRequestEvent } from '@App/Features/request-event';
import { filter, switchMap } from 'rxjs/operators';
import { RequestEventCreationWindowComponent } from '@Layouts/main-layout/pages/events-page/components/request-events-tab/request-event-creation-window/request-event-creation-window.component';
import { RequestEventsListTableComponent } from './request-events-list-table/request-events-list-table.component';

@Component({
    selector: 'app-request-events-tab',
    templateUrl: './request-events-tab.component.html',
    styleUrls: ['./request-events-tab.component.scss']
})
export class RequestEventsTabComponent implements OnInit {

    public requestEvents: IRequestEvent[];

    @ViewChild('RequestEventsTable', { static: false })
    private requestEventsTableComponent: RequestEventsListTableComponent;

    constructor (
        public injector: Injector,
        private dialog: MatDialog
    ) {}

    public ngOnInit (): void {}

    public onAddButtonClick (): void {
        this.dialog.open(RequestEventCreationWindowComponent, {
            width: '95%',
            height: '95%',
            disableClose: true
        })
        .afterClosed()
        .pipe(
            filter((result) => result === true),
            switchMap(() => {
                return this.requestEventsTableComponent.updateRequestEvents();
            })
        )
        .subscribe();
    }

}
