import { Component, OnInit, Input } from '@angular/core';
import { IsArray, IsNotEmptyObject } from 'class-validator';
import { IRoom } from '@App/Features/room';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { ISvc } from '@App/Features/svc';
import { IEquipment } from '@App/Features/equipment';
import { IActionStepperData } from '@Layouts/main-layout/pages/events-page/interfaces/action-stepper-data.interface';

@ClassValidation()
@Component({
    selector: 'app-summary-info',
    templateUrl: './summary-info.component.html',
    styleUrls: ['./summary-info.component.scss']
})
export class SummaryInfoComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public data: IActionStepperData;

    @Input()
    public rruleSummary: string;

    @Input()
    @IsArray()
    public roomArray: IRoom[];

    @Input()
    @IsArray()
    public svcArray: ISvc[];

    @Input()
    @IsArray()
    public equipmentArray: IEquipment[];

    public selectedEquipmentArray: IEquipment[];
    public selectedRoomArray: IRoom[];
    public selectedSvcArray: ISvc[];

    constructor () {}

    public ngOnInit (): void {
        this.selectedEquipmentArray = this.equipmentArray
            .filter((currEquipment) => {
                return this.data.bookingFormData.equipmentIds.includes(currEquipment.id);
            });

        this.selectedRoomArray = this.roomArray
            .filter((currRoom) => {
                return this.data.selectedRoomIds.includes(currRoom.id);
            });

        this.selectedSvcArray = this.svcArray
            .filter((currSvc) => {
                return this.data.selectedServiceIds.includes(currSvc.id);
            });
    }

}
