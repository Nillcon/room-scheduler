import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRoom } from '@App/Features/room';
import { IRoomBookingForm } from '@App/Features/room/interfaces/booking-form.interface';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';

@ClassValidation()
@Component({
    selector: 'filtered-rooms-list-selector',
    templateUrl: './filtered-rooms-list-selector.component.html',
    styleUrls: ['./filtered-rooms-list-selector.component.scss']
})
export class FilteredRoomsListSelectorComponent implements OnInit {

    @Input()
    public roomBookingData: Partial<IRoomBookingForm>;

    @Input()
    public roomArray: IRoom[];

    @Input()
    @IsArray()
    public selectedRoomIds: string[] = [];

    @Output() public OnRoomsSelectionChange: EventEmitter<IRoom[]> = new EventEmitter();

    public selectedRoomsCapacity: number = 0;

    constructor () {}

    public ngOnInit (): void {}

    public onRoomsSelectionChange (rooms: IRoom[]): void {
        let newCapacity: number = 0;

        rooms.forEach((room) => {
            newCapacity += +room.capacity;
        });

        this.selectedRoomsCapacity = newCapacity;

        this.OnRoomsSelectionChange.emit(rooms);
    }

}
