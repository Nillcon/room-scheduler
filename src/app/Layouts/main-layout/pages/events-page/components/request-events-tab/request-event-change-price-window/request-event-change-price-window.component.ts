import { Component, OnInit, Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import { IsNumber } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { NotificationService } from '@Core/root/notification/notification.service';
import { InvoicesPriceTableComponent } from './invoices-price-table/invoices-price-table.component';
import { tap } from 'rxjs/operators';
import { RequestEventService } from '@App/Features/request-event/request-event.service';

@ClassValidation()
@Component({
    selector: 'app-request-event-change-price-window',
    templateUrl: './request-event-change-price-window.component.html',
    styleUrls: ['./request-event-change-price-window.component.scss']
})
export class RequestEventChangePriceWindowComponent implements OnInit {

    @IsNumber()
    public requestEventId: number;

    public totalPrice: number;

    public savingSubscription: Subscription;

    @ViewChild(InvoicesPriceTableComponent, { static: false })
    private invoicesTableComponent: InvoicesPriceTableComponent;

    constructor (
        @Inject(MAT_DIALOG_DATA) requestEventId: number,
        private dialogRef: MatDialogRef<RequestEventChangePriceWindowComponent>,
        private changeDetector: ChangeDetectorRef,
        private requestEventService: RequestEventService,
        private notificationService: NotificationService
    ) {
        this.requestEventId = requestEventId;
    }

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public countTotalPrice (): void {
        let newPrice: number = 0;

        this.invoicesTableComponent.invoiceServiceArray
            .forEach((invoice) => {
                if (invoice.checkedByApprover) {
                    newPrice += +invoice.price * +invoice.count;
                }
            });

        this.totalPrice = newPrice;
            this.changeDetector.detectChanges();
    }

    public save (): void {
        const data = this.invoicesTableComponent.invoiceServiceArray
            .filter((currInvoiceService) => {
                return currInvoiceService.checkedByApprover === true;
            });

        this.savingSubscription = this.requestEventService.saveInvoiceServices(this.requestEventId, data)
            .pipe(
                tap(() => {
                    this.notificationService.success({});
                    this.close(true);
                })
            )
            .subscribe();
    }

}
