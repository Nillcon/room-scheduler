import { Component, OnInit, ViewChild, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActionStepperStepEnum } from '../shared/action-stepper-step.enum';
import { Subscription, forkJoin, Observable } from 'rxjs';
import { IRequestEventGeneralInfoForm, IRequestEvent } from '@App/Features/request-event';
import { IActionStepperData } from '../../../interfaces/action-stepper-data.interface';
import { switchMap, tap, mapTo, delayWhen } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRoomBookingForm } from '@App/Features/room/interfaces/booking-form.interface';
import { RequestEventActionStepperComponent } from '../request-event-action-stepper/request-event-action-stepper.component';
import { RequestEventTypes } from '@App/Features/request-event/shared/request-event-types.enum';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { RequestEventService } from '@App/Features/request-event/request-event.service';

@Component({
    selector: 'app-request-event-editing-window',
    templateUrl: './request-event-editing-window.component.html',
    styleUrls: ['./request-event-editing-window.component.scss']
})
export class RequestEventEditingWindowComponent implements OnInit {

    public requestEvent: IRequestEvent;
    public bookingFormData: Partial<IRoomBookingForm>;
    public selectedRoomIds: string[];
    public selectedSvcIds: number[];

    public isAllEditingDataLoaded: boolean = false;

    public currentStep: ActionStepperStepEnum = ActionStepperStepEnum.Booking;
    public readonly stepsEnum: typeof ActionStepperStepEnum = ActionStepperStepEnum;

    public data: IActionStepperData;

    public creationSubscription: Subscription;

    @ViewChild('ActionStepper', { static: false })
    private actionStepperComponent: RequestEventActionStepperComponent;

    private bookingFormGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>;
    private generalInfoFormGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>;

    constructor (
        public injector: Injector,
        @Inject(MAT_DIALOG_DATA) public requestEventId: number,
        private dialogRef: MatDialogRef<RequestEventEditingWindowComponent>,
        private requestEventService: RequestEventService,
        private notificationService: NotificationService
    ) {}

    public ngOnInit (): void {
        this.updateRequestEventData()
            .pipe(
                switchMap(() => this.updateSelectedFilters()),
                switchMap(() => this.updateSelectedRoomIds()),
                switchMap(() => this.updateSelectedSvcIds())
            )
            .subscribe(() => {
                this.isAllEditingDataLoaded = true;
            });
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public updateRequestEventData (): Observable<IRequestEvent> {
        return this.requestEventService.get(this.requestEventId)
            .pipe(
                tap((requestEvent) => {
                    this.requestEvent = requestEvent;
                })
            );
    }

    public updateSelectedFilters (): Observable<Partial<IRoomBookingForm>> {
        return this.requestEventService.getFilters(this.requestEventId)
            .pipe(
                tap((data: Partial<IRoomBookingForm>) => {
                    this.bookingFormData = data;
                })
            );
    }

    public updateSelectedRoomIds (): Observable<string[]> {
        return this.requestEventService.getRooms(this.requestEventId)
            .pipe(
                tap((roomIds) => {
                    this.selectedRoomIds = roomIds;
                })
            );
    }

    public updateSelectedSvcIds (): Observable<number[]> {
        return this.requestEventService.getServices(this.requestEventId)
            .pipe(
                tap((svcIds) => {
                    this.selectedSvcIds = svcIds;
                })
            );
    }

    public onMainButtonClick (): void {
        if (this.currentStep === ActionStepperStepEnum.GeneralInfo) {
            this.onEditButtonClick();
        } else {
            this.nextStep();
        }
    }

    public onSecondaryButtonClick (): void {
        this.previousStep();
    }

    public onStepChanged (stepIndex: ActionStepperStepEnum): void {
        this.currentStep = stepIndex;
    }

    public onDataChanged (data: IActionStepperData): void {
        this.data = data;
    }

    public onBookingFormStateChanged (formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>): void {
        this.bookingFormGroup = formGroup;
    }

    public onGeneralInfoFormStateChanged (formGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>): void {
        this.generalInfoFormGroup = formGroup;
    }

    public nextStep (): void {
        this.actionStepperComponent.nextStep();
    }

    public previousStep (): void {
        this.actionStepperComponent.previousStep();
    }

    @NeedsConfirmation(
        `Do you really want to just save this Request Event as a <strong>draft</strong>?
        <b style="color: red;">Please note that your Request Event will not be processed by Approver.</b>`
    )
    public onEditButtonClick (): void {
        if (this.bookingFormGroup.valid && this.generalInfoFormGroup.valid) {
            this.creationSubscription = this.editRequestEvent()
                .pipe(
                    tap(() => {
                        this.close(true);
                        this.notificationService.success({ text: 'Request event saved!' });
                    })
                )
                .subscribe();
        } else {
            this.bookingFormGroup.markAllAsTouched();
            this.generalInfoFormGroup.markAllAsTouched();

            this.notificationService.error({ text: 'Some fields aren\'t valid!' });
        }
    }

    public onSubmitForApprovalButtonClick (): void {
        if (this.bookingFormGroup.valid && this.generalInfoFormGroup.valid) {
            this.creationSubscription = this.editRequestEvent()
                .pipe(
                    delayWhen((requestEventId) => {
                        return this.requestEventService.changeStatus(
                            requestEventId,
                            RequestEventTypes.PendingApproval
                        );
                    }),
                    tap(() => {
                        this.close(true);
                        this.notificationService.success({ text: 'Request event submitted for Approval!' });
                    })
                )
                .subscribe();
        } else {
            this.bookingFormGroup.markAllAsTouched();
            this.generalInfoFormGroup.markAllAsTouched();

            this.notificationService.error({ text: 'Some fields aren\'t valid!' });
        }
    }

    public editRequestEvent (): Observable<number> {
        return this.requestEventService.edit(this.requestEventId, this.data.generalInfoFormData)
            .pipe(
                switchMap(() => {
                    return forkJoin(
                        this.requestEventService.assignFilters(this.requestEventId, this.data.bookingFormData),
                        this.requestEventService.assignRooms(this.requestEventId, this.data.selectedRoomIds),
                        this.requestEventService.assignServices(this.requestEventId, this.data.selectedServiceIds)
                    );
                }),
                mapTo(this.requestEventId)
            );
    }

}
