import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RequestEventService } from '@Features/request-event/request-event.service';
import { NotificationService } from '@Core/root/notification/notification.service';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRequestEventApprovingForm } from '@Features/request-event/interfaces/approving-form.interface';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-room-details-approving-window',
  templateUrl: './room-details-approving-window.component.html',
  styleUrls: ['./room-details-approving-window.component.scss']
})
export class RoomDetailsApprovingWindowComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<IRequestEventApprovingForm>;

    public savingSubscription: Subscription;

    constructor (
        @Inject(MAT_DIALOG_DATA) public data: {
            requestEventId: number,
            roomId: string
        },
        private dialogRef: MatDialogRef<RoomDetailsApprovingWindowComponent>,
        private requestEventService: RequestEventService,
        private notificationService: NotificationService
    ) { }

    public ngOnInit (): void  {
    }

    public save (): void {
        if (this.formGroup.valid) {
            this.savingSubscription = this.requestEventService.changeRoomStatus(
                this.data.roomId,
                this.data.requestEventId,
                this.formGroup.value.status
            )
                .pipe(
                    tap(() => {
                        this.close(true);
                        this.notificationService.success({});
                    })
                )
                .subscribe();
        } else {
            this.notificationService.error({ text: 'Form isn\'t valid!' });
        }
    }

    public onFormGroupInit (formGroup: FormGroupTypeSafe<IRequestEventApprovingForm>): void {
        this.formGroup = formGroup;
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

}
