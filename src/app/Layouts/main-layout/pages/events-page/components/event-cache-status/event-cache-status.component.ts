import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { EventService } from '@App/Features/event';
import { Observable, Subscription } from 'rxjs';
import { IEventCacheData } from '@App/Features/event/interfaces/event-cache-data.interface';
import { NotificationService } from '@Core/root/notification/notification.service';
import { filter, switchMap, tap } from 'rxjs/operators';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-event-cache-status',
    templateUrl: './event-cache-status.component.html',
    styleUrls: ['./event-cache-status.component.scss']
})
export class EventCacheStatusComponent implements OnInit, OnDestroy {

    @Output()
    public OnCacheUpdated: EventEmitter<boolean> = new EventEmitter();

    public cacheData$: Observable<IEventCacheData>;

    private successMessageSubscription: Subscription;
    private errorMessageSubscription: Subscription;

    constructor (
        private notificationService: NotificationService,
        private eventService: EventService
    ) {}

    public ngOnInit (): void {
        this.eventService.startCacheConnection()
            .subscribe();

        this.cacheData$ = this.eventService.cacheData$;

        this.onSuccessMessageObserver();
        this.onErrorMessageObserver();
    }

    public ngOnDestroy (): void {
        this.eventService.stopCacheConnection();
    }

    public onUpdateButtonClick (): void {
        this.eventService.updateCache();
    }

    private onSuccessMessageObserver (): void {
        this.successMessageSubscription = this.eventService.onCacheSuccessMessage$
            .pipe(
                switchMap((successMessage) => {
                    return this.notificationService.confirm({
                        title: successMessage.title,
                        text: successMessage.message
                    });
                }),
                filter((result) => result === true),
                tap(() => {
                    this.OnCacheUpdated.emit(true);
                })
            )
            .subscribe();
    }

    private onErrorMessageObserver (): void {
        this.errorMessageSubscription = this.eventService.onCacheErrorMessage$
            .pipe(
                tap((errorMessage) => {
                    this.notificationService.error({
                        title: errorMessage.title,
                        text: errorMessage.message
                    });
                })
            )
            .subscribe();
    }

}
