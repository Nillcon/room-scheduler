import { Component, OnInit, ViewChild } from '@angular/core';
import { EventsTableComponent } from './events-table/events-table.component';
import { RoleEnum } from '@App/Features/role/shared/role.enum';

@Component({
    selector: 'app-events-tab',
    templateUrl: './events-tab.component.html',
    styleUrls: ['./events-tab.component.scss']
})
export class EventsTabComponent implements OnInit {

    public readonly roleEnum: typeof RoleEnum = RoleEnum;

    @ViewChild('EventsTable', { static: false })
    private eventsTableComponent: EventsTableComponent;

    constructor () {}

    public ngOnInit (): void {}

    public onCacheUpdated (): void {
        this.eventsTableComponent.createDataSource();
    }

}
