import { Component, OnInit, Injector } from '@angular/core';
import { IEvent, EventService } from '@App/Features/event';
import CustomStore from 'devextreme/data/custom_store';
import DevExpress from 'devextreme/bundles/dx.all';
import { UrlService } from '@Core/root/url/url.service';
import { EventsPageQueryVariableEnum } from '../../../shared/events-page-query-variable.enum';
import { ColumnEnum } from '@App/Features/event/components/list-table/shared/list-table-columns.enum';
import { UserService } from '@App/Features/user';
import { RoleEnum } from '@App/Features/role/shared/role.enum';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-events-table',
    templateUrl: './events-table.component.html',
    styleUrls: ['./events-table.component.scss']
})
export class EventsTableComponent implements OnInit {

    public dataSource: CustomStore;

    public itemIdFromUrl: string;

    public forbiddenColumns: ColumnEnum[];

    constructor (
        public injector: Injector,
        private userService: UserService,
        private eventService: EventService,
        private urlService: UrlService
    ) {}

    public ngOnInit (): void {
        this.createDataSource();
        this.initItemIdFromUrl();
        this.initTableForbiddenColumns();
    }

    public createDataSource (): void {
        this.dataSource = new CustomStore({
            key: 'uid',
            load: (options: DevExpress.data.LoadOptions) => {
                return this.eventService.getAllWithOData(options)
                    .toPromise();
            }
        });
    }

    @NeedsConfirmation('Are you really want to delete the event?', 'Confirm deleting')
    public onEventDelete (event: IEvent): void {
        this.eventService.delete(event.uid, event.iCalUid)
            .pipe(
                tap(() => {
                    window.scroll(0, 0);
                    this.createDataSource();
                })
            )
            .subscribe();
    }

    private initItemIdFromUrl (): void {
        const itemIdFromUrl: string = this.urlService.getParameter(EventsPageQueryVariableEnum.EventId);

        if (itemIdFromUrl) {
            this.itemIdFromUrl = itemIdFromUrl;
        }
    }

    private initTableForbiddenColumns (): void {
        const forbiddenColumns: ColumnEnum[] = [ColumnEnum.Edit];

        if (!this.userService.includesRole(RoleEnum.SuperUser)) {
            forbiddenColumns.push(ColumnEnum.Delete);
        }

        this.forbiddenColumns = forbiddenColumns;
    }

}
