import { Component, OnInit } from '@angular/core';
import { UrlService } from '@Core/root/url/url.service';
import { EventsPageQueryVariableEnum } from './shared/events-page-query-variable.enum';
import { EventsPageTabEnum } from './shared/events-page-tab.enum';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';

@Component({
    selector: 'app-events-page',
    templateUrl: './events-page.component.html',
    styleUrls: ['./events-page.component.scss']
})
export class EventsPageComponent implements OnInit {

    public openedTab: number = EventsPageTabEnum.Events;

    constructor (
        private urlService: UrlService,
        private storageService: LocalStorageService
    ) {}

    public ngOnInit (): void {
        this.prepareTabs();
    }

    public saveOpenedTab (openedTab: EventsPageTabEnum): void {
        this.storageService.set(StorageKey.Event_Page_Opened_Tab, openedTab);
    }

    public onSelectedTabChange (): void {
        this.saveOpenedTab(this.openedTab);
    }

    private prepareTabs (): void {
        const eventId: string        = this.urlService.getParameter(EventsPageQueryVariableEnum.EventId);
        const requestEventId: string = this.urlService.getParameter(EventsPageQueryVariableEnum.RequestEventId);

        if (eventId) {
            this.openedTab = EventsPageTabEnum.Events;
        } else if (requestEventId) {
            this.openedTab = EventsPageTabEnum.RequestEvents;
        } else {
            const openedTab: EventsPageTabEnum = this.storageService.get(StorageKey.Event_Page_Opened_Tab);

            this.openedTab = openedTab;
        }
    }

}
