export enum EventsPageQueryVariableEnum {
    EventId        = 'eventId',
    RequestEventId = 'requestEventId'
}
