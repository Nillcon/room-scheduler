import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroupTypeSafe } from 'form-type-safe';
import { ISvc } from '@App/Features/svc';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';
import { SvcService } from '@App/Features/svc/svc.service';

@Component({
    selector: 'app-svc-creation-window',
    templateUrl: './svc-creation-window.component.html',
    styleUrls: ['./svc-creation-window.component.scss']
})
export class SvcCreationWindowComponent implements OnInit {

    public creationSubscription: Subscription;

    private svcFormGroup: FormGroupTypeSafe<Partial<ISvc>>;

    constructor (
        private dialogRef: MatDialogRef<SvcCreationWindowComponent>,
        private svcService: SvcService,
        private notifyService: NotificationService
    ) {}

    public ngOnInit (): void {}

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onSvcFormGroupInit (formGroup: FormGroupTypeSafe<Partial<ISvc>>): void {
        this.svcFormGroup = formGroup;
    }

    public createSvc (): void {
        if (this.svcFormGroup.valid) {
            this.creationSubscription = this.svcService.create(this.svcFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Service created!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.svcFormGroup.markAllAsTouched();
        }
    }

}
