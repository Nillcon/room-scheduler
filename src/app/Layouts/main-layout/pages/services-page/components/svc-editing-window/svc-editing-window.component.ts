import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IsInt } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { ISvc } from '@App/Features/svc';
import { NotificationService } from '@Core/root/notification/notification.service';
import { FormGroupTypeSafe } from 'form-type-safe';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { SvcService } from '@App/Features/svc/svc.service';

@ClassValidation()
@Component({
    selector: 'app-svc-editing-window',
    templateUrl: './svc-editing-window.component.html',
    styleUrls: ['./svc-editing-window.component.scss']
})
export class SvcEditingWindowComponent implements OnInit {

    @IsInt()
    public svcId: number;

    public svc: ISvc;

    public savingSubscription: Subscription;

    private svcFormGroup: FormGroupTypeSafe<Partial<ISvc>>;

    constructor (
        private dialogRef: MatDialogRef<SvcEditingWindowComponent>,
        @Inject(MAT_DIALOG_DATA) svcId: number,
        private notifyService: NotificationService,
        private svcService: SvcService
    ) {
        this.svcId = svcId;
    }

    public ngOnInit (): void {
        this.updateSvc();
    }

    public close (result = false): void {
        this.dialogRef.close(result);
    }

    public onSvcFormGroupInit (formGroup: FormGroupTypeSafe<Partial<ISvc>>): void {
        this.svcFormGroup = formGroup;
    }

    public updateSvc (): void {
        this.svcService.get(this.svcId)
            .subscribe(svc => {
                this.svc = svc;
            });
    }

    public editSvc (): void {
        if (this.svcFormGroup.valid) {
            this.savingSubscription = this.svcService.edit(this.svcId, this.svcFormGroup.value)
                .pipe(
                    tap(() => {
                        this.notifyService.success({ text: 'Service saved!' });
                        this.close(true);
                    })
                )
                .subscribe();
        } else {
            this.notifyService.error({ text: 'Form is not valid' });
            this.svcFormGroup.markAllAsTouched();
        }
    }

}
