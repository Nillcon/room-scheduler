import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServicesPageComponent } from './services-page.component';
import { ServicesPageGuard } from './guards/services-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: ServicesPageComponent,
        canActivate: [ServicesPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ServicesPageRoutingModule {}
