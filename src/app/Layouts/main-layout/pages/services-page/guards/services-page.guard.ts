import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '@App/Features/user';
import { MainLayoutPagesMap } from '@Layouts/main-layout/shared/main-layout-pages.map';
import { MainLayoutPagesEnum } from '@Layouts/main-layout/main-layout-pages';
import { RoleEnum } from '@App/Features/role';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { AppPagesEnum } from '@App/app-pages';

export class ServicesPageGuard implements CanActivate {

    constructor (
        private userService: UserService,
        private navigationService: NavigationService
    ) {}

    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const pageRoles: RoleEnum[] = MainLayoutPagesMap.get(MainLayoutPagesEnum.Services).roles;

        if (pageRoles) {
            const isPageAvailable: boolean = this.userService.includesOneOfRoles(
                pageRoles
            );

            if (!isPageAvailable) {
                this.navigationService.navigate(`${AppPagesEnum.Index}/${MainLayoutPagesEnum.Main}`);
            }

            return isPageAvailable;
        } else {
            return true;
        }
    }

}
