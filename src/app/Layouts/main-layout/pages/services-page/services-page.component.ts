import { Component, OnInit, Injector } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SvcCreationWindowComponent } from './components/svc-creation-window/svc-creation-window.component';
import { filter, tap } from 'rxjs/operators';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { SvcEditingWindowComponent } from './components/svc-editing-window/svc-editing-window.component';
import { ISvc } from '@App/Features/svc';
import { SvcService } from '@App/Features/svc/svc.service';

@Component({
    selector: 'app-services-page',
    templateUrl: './services-page.component.html',
    styleUrls: ['./services-page.component.scss']
})
export class ServicesPageComponent implements OnInit {

    public svcArray: ISvc[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private svcService: SvcService
    ) {}

    public ngOnInit (): void {
        this.updateSvcArray();
    }

    public updateSvcArray (): void {
        this.svcService.getAll()
            .subscribe(svcArray => {
                this.svcArray = svcArray;
            });
    }

    public onAddButtonClick (): void {
        this.dialog.open(SvcCreationWindowComponent, {
            width: '500px'
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                tap(() => this.updateSvcArray())
            )
            .subscribe();
    }

    public onSvcEdit (svc: ISvc): void {
        this.dialog.open(SvcEditingWindowComponent, {
            width: '500px',
            data: svc.id
        })
            .afterClosed()
            .pipe(
                filter(result => result === true),
                tap(() => this.updateSvcArray())
            )
            .subscribe();
    }

    @NeedsConfirmation('Do you really want to delete the service?', 'Confirm deleting')
    public onSvcDelete (svc: ISvc): void {
        this.svcService.delete(svc.id)
            .pipe(
                tap(() => this.updateSvcArray())
            )
            .subscribe();
    }

}
