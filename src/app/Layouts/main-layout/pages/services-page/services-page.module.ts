import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesPageComponent } from './services-page.component';
import { ServicesPageRoutingModule } from './services-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { SvcCreationWindowComponent } from './components/svc-creation-window/svc-creation-window.component';
import { SvcEditingWindowComponent } from './components/svc-editing-window/svc-editing-window.component';
import { ServicesPageGuard } from './guards/services-page.guard';

@NgModule({
    declarations: [
        ServicesPageComponent,
        SvcCreationWindowComponent,
        SvcEditingWindowComponent
    ],
    imports: [
        CommonModule,

        ServicesPageRoutingModule,

        MainLayoutSharedModule,
        SharedModule,
        FeaturesModule
    ],
    entryComponents: [
        SvcCreationWindowComponent,
        SvcEditingWindowComponent
    ],
    providers: [
        ServicesPageGuard
    ]
})
export class ServicesPageModule { }
