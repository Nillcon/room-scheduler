import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '@App/Features/user';
import { MainLayoutPagesMap } from '@Layouts/main-layout/shared/main-layout-pages.map';
import { MainLayoutPagesEnum } from '@Layouts/main-layout/main-layout-pages';
import { RoleEnum } from '@App/Features/role';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { AppPagesEnum } from '@App/app-pages';
import { VisitorLayoutPagesEnum } from '@Layouts/visitor-layout/visitor-layout-pages';

export class IndexPageGuard implements CanActivate {

    constructor (
        private userService: UserService,
        private navigationService: NavigationService
    ) {}

    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const pageRoles: RoleEnum[] = MainLayoutPagesMap.get(MainLayoutPagesEnum.Main).roles;

        if (pageRoles) {
            const isPageAvailable: boolean = this.userService.includesOneOfRoles(
                pageRoles
            );

            if (!isPageAvailable) {
                this.navigationService.navigate(`${AppPagesEnum.Visitor}/${VisitorLayoutPagesEnum.Index}`);
            }

            return isPageAvailable;
        } else {
            return true;
        }
    }

}
