import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IndexPageComponent } from './index-page.component';
import { IndexPageGuard } from './guards/index-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: IndexPageComponent,
        canActivate: [IndexPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class IndexPageRoutingModule {}
