import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexPageComponent } from './index-page.component';
import { IndexPageRoutingModule } from './index-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { SpaceCreationWindowComponent } from './components/space-creation-window/space-creation-window.component';
import { SpaceSettingsWindowComponent } from './components/space-settings-window/space-settings-window.component';
import { SpaceEditingWindowComponent } from './components/space-editing-window/space-editing-window.component';
import { IndexCalendarComponent } from './components/index-calendar/index-calendar.component';
import { SpaceToolbarComponent } from './components/index-calendar/space-toolbar/space-toolbar.component';
import { IndexPageGuard } from './guards/index-page.guard';

@NgModule({
    declarations: [
        IndexPageComponent,
        SpaceCreationWindowComponent,
        SpaceEditingWindowComponent,
        SpaceSettingsWindowComponent,
        IndexCalendarComponent,
        SpaceToolbarComponent
    ],
    imports: [
        CommonModule,
        MainLayoutSharedModule,

        IndexPageRoutingModule,
        FeaturesModule,
        SharedModule
    ],
    entryComponents: [
        SpaceCreationWindowComponent,
        SpaceEditingWindowComponent,
        SpaceSettingsWindowComponent
    ],
    providers: [
        IndexPageGuard
    ]
})
export class IndexPageModule { }
