import { Component, OnInit } from '@angular/core';
import { IEvent } from '@App/Features/event';
import { ISpace } from '@App/Features/space';

@Component({
    selector: 'app-index-page',
    templateUrl: './index-page.component.html',
    styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit {

    public selectedSpace: ISpace;

    public events: IEvent[];

    constructor (
    ) {}

    public ngOnInit (): void {
    }


}
