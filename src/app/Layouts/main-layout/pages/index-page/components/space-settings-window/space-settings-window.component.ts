import { Component, OnInit, Inject, Injector } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ISpace } from '@App/Features/space/interfaces/space.interface';
import { NotificationService } from '@Core/root/notification/notification.service';
import { SpaceEditingWindowComponent } from '../space-editing-window/space-editing-window.component';
import { filter, tap, switchMap } from 'rxjs/operators';
import { SpaceCreationWindowComponent } from '../space-creation-window/space-creation-window.component';
import { Observable } from 'rxjs';
import { IRoom } from '@App/Features/room';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IsArray } from 'class-validator';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { SpaceService } from '@App/Features/space';

@ClassValidation()
@Component({
    selector: 'app-space-settings-window',
    templateUrl: './space-settings-window.component.html',
    styleUrls: ['./space-settings-window.component.scss']
})
export class SpaceSettingsWindowComponent implements OnInit {

    @IsArray()
    public rooms: IRoom[];

    public spaces: ISpace[];

    constructor (
        public dialogRef: MatDialogRef<SpaceSettingsWindowComponent>,
        @Inject(MAT_DIALOG_DATA) rooms: IRoom[],
        public injector: Injector,
        private spaceService: SpaceService,
        private dialog: MatDialog,
        private notifyService: NotificationService
    ) {
        this.rooms = rooms;
    }

    public ngOnInit (): void {
        this.updateSpaces()
            .subscribe();
    }

    public updateSpaces (): Observable<ISpace[]> {
        return this.spaceService.getAll()
            .pipe(
                tap(res => this.spaces = res)
            );
    }

    public openCreateSpaceWindow (): void {
        const window = this.dialog.open(SpaceCreationWindowComponent, {
            width: '500px',
            data: this.rooms
        });

        window.afterClosed()
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateSpaces())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    public openEditSpaceWindow (space: ISpace): void {
        const window = this.dialog.open(SpaceEditingWindowComponent, {
            width: '500px',
            data: {
                space: space,
                rooms: this.rooms
            }
        });

        window.afterClosed()
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateSpaces())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    @NeedsConfirmation()
    public deleteSpace (space: ISpace): void {
        this.spaceService.delete(space)
            .pipe(
                switchMap(() => this.updateSpaces())
            )
            .subscribe(() => {
                this.notifyService.success({
                    text: 'Space successfully deleted'
                });
            });
    }

    public close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
