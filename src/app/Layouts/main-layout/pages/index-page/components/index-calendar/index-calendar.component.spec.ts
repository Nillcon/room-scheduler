import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexCalendarComponent } from './index-calendar.component';

describe('IndexCalendarComponent', () => {
  let component: IndexCalendarComponent;
  let fixture: ComponentFixture<IndexCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
