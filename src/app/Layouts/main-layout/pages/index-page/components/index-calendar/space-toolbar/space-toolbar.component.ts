import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ISpace } from '@App/Features/space';

@Component({
  selector: 'app-space-toolbar',
  templateUrl: './space-toolbar.component.html',
  styleUrls: ['./space-toolbar.component.scss']
})
export class SpaceToolbarComponent implements OnInit {

    @Input()
    public spaces: ISpace[];

    @Input()
    public selectedSpace: ISpace;

    @Output()
    public OnCreationButtonClick: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnSelectionChange: EventEmitter<ISpace> = new EventEmitter();

    @Output()
    public OnSettingButtonClick: EventEmitter<boolean> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onClickCreationButton (): void {
        this.OnCreationButtonClick.emit(true);
    }

    public onSelectionChange (space: ISpace): void {
        this.OnSelectionChange.emit(space);
    }

    public onClickSettingButton (): void {
        this.OnSettingButtonClick.emit(true);
    }

}
