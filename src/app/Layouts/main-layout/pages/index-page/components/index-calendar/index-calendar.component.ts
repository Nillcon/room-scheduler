import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { NotificationService } from '@Core/root/notification/notification.service';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';
import { DateService } from '@Core/root/date/date.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';

import { ISpace, SpaceService } from '@App/Features/space';
import { IEvent, EventService } from '@App/Features/event';
import { RoomService } from '@App/Features/room/room.service';
import { IRoom } from '@App/Features/room';

import { SpaceCreationWindowComponent } from '../space-creation-window/space-creation-window.component';
import { SpaceSettingsWindowComponent } from '../space-settings-window/space-settings-window.component';

import { filter, switchMap, tap, delayWhen } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ActionEventArgs } from '@syncfusion/ej2-schedule';
import { UserService } from '@App/Features/user';
import { RoleEnum } from '@App/Features/role/shared/role.enum';


@Component({
  selector: 'app-index-calendar',
  templateUrl: './index-calendar.component.html',
  styleUrls: ['./index-calendar.component.scss']
})
export class IndexCalendarComponent implements OnInit {

    public events: IEvent[];
    public rooms: IRoom[];
    public spaces: ISpace[];

    public currentDate: Date = new Date();

    public fromDate: Date;
    public toDate: Date;

    public selectedSpace: ISpace;
    public selectedRooms: IRoom[];

    public isReadOnly: boolean;

    constructor (
        private userService: UserService,
        private dialog: MatDialog,
        private notifyService: NotificationService,
        private storageService: LocalStorageService,
        private eventService: EventService,
        private spaceService: SpaceService,
        private roomService: RoomService,
        private dateService: DateService,
    ) { }

    public ngOnInit (): void {
        this.updateAllData()
            .subscribe();
    }

    public updateAllData (): Observable<any> {
        this.initReadonlyInfo();
        this.initCurrentDateRange();

        return this.updateEvents(this.fromDate, this.toDate)
            .pipe(
                switchMap(() => this.updateRooms()),
                switchMap(() => this.updateSpaces()),
                tap(() => {
                    this.selectedSpace = this.getSelectedSpaceFromStorage();

                    if (this.selectedSpace) {
                        this.selectedRooms = this.selectedSpace.rooms;
                    }
                })
            );
    }

    public updateEvents (from?: Date, to?: Date): Observable<IEvent[]> {
        return this.eventService.getByFilters(from, to)
            .pipe(
                tap(events =>  this.events = events)
            );
    }

    public updateRooms (): Observable<IRoom[]> {
        return this.roomService.getAll()
            .pipe(
                tap(rooms => {
                    this.rooms = rooms;
                }),
            );
    }

    public updateSpaces (): Observable<ISpace[]> {
        return this.spaceService.getSpacesWithFullRoomsObject(this.rooms)
            .pipe(
                tap(spaces => this.spaces = spaces)
            );
    }

    public openCreateSpaceWindow (): void {
        const window = this.dialog.open(SpaceCreationWindowComponent, {
            width: '500px',
            data: this.rooms
        });

        window.afterClosed()
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateSpaces())
            )
            .subscribe(() => this.notifyService.success({}));
    }

    public openSpaceSettingsWindow (): void {
        const window = this.dialog.open(SpaceSettingsWindowComponent, {
            minHeight: '450px',
            maxHeight: '650px',
            width: '500px',
            data: this.rooms
        });

        window.afterClosed()
            .pipe(
                switchMap(() => this.updateSpaces()),
                tap(() => this.selectedSpace = this.getSelectedSpaceFromStorage())
            )
            .subscribe();
    }

    public saveSelectedSpace (space: ISpace): void {
        if (space) {
            this.storageService.set(StorageKey.Selected_Space, space);
        } else {
            this.storageService.remove(StorageKey.Selected_Space);
        }

        this.selectedSpace = space;
    }

    public onActionEventUpdated (actionEvent: ActionEventArgs): void {
        actionEvent.addedRecords = this.deleteTimeZoneFromEvents(actionEvent.addedRecords as IEvent[]);
        actionEvent.changedRecords = this.deleteTimeZoneFromEvents(actionEvent.changedRecords as IEvent[]);

        this.eventService.updateActionEvent(actionEvent)
            .subscribe(() => {
                this.updateEvents(this.fromDate, this.toDate);
            });
    }

    public onSpaceChanged (space: ISpace): void {
        this.saveSelectedSpace(space);

        if (space) {
            this.selectedRooms = JSON.parse(JSON.stringify(space.rooms));
        }
    }

    public onSelectedRoomsChanged (): void {
        this.selectedSpace = null;
    }

    public onDateRangeChanged (event: IDateRange): void {
        if (event.from.toDateString() === this.fromDate.toDateString()) {
            if (event.to.toDateString() === this.toDate.toDateString()) {
                return;
            }
        }

        this.fromDate = event.from;
        this.toDate = event.to;

        of(
            this.notifyService.infoToast({
                title: 'Load',
                text: 'Events is loading...',
                duration: Infinity
            })
        )
            .pipe(
                delayWhen(() => this.updateEvents(event.from, event.to)),
                tap((notification) => {
                    notification.Close();
                })
            )
            .subscribe();
    }

    private initReadonlyInfo (): void {
        this.isReadOnly = !this.userService.includesOneOfRoles([RoleEnum.Approver, RoleEnum.SuperUser]);
    }

    private initCurrentDateRange (): void {
        this.fromDate = this.dateService.getDateWithoutTime(new Date());
        this.currentDate = this.fromDate;

        this.toDate = this.fromDate;
    }

    private deleteTimeZoneFromEvents (events: IEvent[]): IEvent[] {
        events = events.map((event: IEvent) => {
            event.start = this.dateService.getLocalJsonTimeString(new Date(event.start));

            event.end = this.dateService.getLocalJsonTimeString(new Date(event.end));

            return event;
        });

        return events;
    }

    private getSelectedSpaceFromStorage (): ISpace {
        let selectedSpace = this.storageService.get<ISpace>(StorageKey.Selected_Space);

        if (selectedSpace) {
            selectedSpace = this.spaces
                .find(space => space.id === selectedSpace.id);
        }

        return selectedSpace;
    }
}
