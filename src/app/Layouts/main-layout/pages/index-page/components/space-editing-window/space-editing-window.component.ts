import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ISpace, SpaceService } from '@App/Features/space';
import { IsNotEmptyObject, IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IRoom } from '@App/Features/room/interfaces/room.interface';
import { Subscription } from 'rxjs';

@ClassValidation()
@Component({
    selector: 'app-space-editing-window',
    templateUrl: './space-editing-window.component.html',
    styleUrls: ['./space-editing-window.component.scss']
})
export class SpaceEditingWindowComponent implements OnInit {

    @IsNotEmptyObject()
    public space: ISpace;

    @IsArray()
    public rooms: IRoom[];

    public spaceEditingForm: FormGroupTypeSafe<ISpace>;

    public saveSpaceSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<SpaceEditingWindowComponent>,
        @Inject(MAT_DIALOG_DATA) data: {space: ISpace, rooms: IRoom[]},
        private spaceService: SpaceService
    ) {
        this.space = data.space;
        this.rooms = data.rooms;
    }

    public ngOnInit (): void {
    }

    public onSpaceEditingFormInit (formGroup: FormGroupTypeSafe<ISpace>): void {
        this.spaceEditingForm = formGroup;
    }

    public saveSpace (): void {
        if (this.spaceEditingForm.valid) {
            this.spaceEditingForm.value.id =  this.space.id;

            this.saveSpaceSubscription = this.spaceService.edit(
               this.spaceEditingForm.value
            )
                .subscribe(() => {
                    this.close(true);
                });
        } else {
            this.spaceEditingForm.markAllAsTouched();
        }
    }

    public close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
