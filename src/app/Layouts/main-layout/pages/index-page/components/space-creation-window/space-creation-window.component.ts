import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IRoom } from '@App/Features/room';
import { FormGroupTypeSafe } from 'form-type-safe';
import { ISpace, SpaceService } from '@App/Features/space';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-space-creation-window',
    templateUrl: './space-creation-window.component.html',
    styleUrls: ['./space-creation-window.component.scss']
})
export class SpaceCreationWindowComponent implements OnInit {

    public spaceForm: FormGroupTypeSafe<ISpace>;

    public createSpaceSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<SpaceCreationWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public rooms: IRoom[],
        private spaceService: SpaceService
    ) {}

    public ngOnInit (): void {}

    public spaceFormInit (form: FormGroupTypeSafe<ISpace>): void {
        this.spaceForm = form;
    }

    public createSpace (): void {
        if (this.spaceForm.valid) {
            this.createSpaceSubscription = this.spaceService.create(this.spaceForm.value)
                .subscribe(() => {
                    this.close(true);
                });
        }
    }

    public close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
