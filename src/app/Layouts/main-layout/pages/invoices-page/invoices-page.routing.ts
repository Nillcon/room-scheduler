import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoicesPageComponent } from './invoices-page.component';
import { InvoicesPageGuard } from './guards/invoices-page.guard';

export const routes: Routes = [
    {
        path: '',
        component: InvoicesPageComponent,
        canActivate: [InvoicesPageGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class InvoicesPageRoutingModule {}
