import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicesPageComponent } from './invoices-page.component';
import { InvoicesPageRoutingModule } from './invoices-page.routing';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { InvoicesTableComponent } from './components/invoices-table/invoices-table.component';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { InvoiceCreationWindowComponent } from './components/invoice-creation-window/invoice-creation-window.component';
import { InvoiceEditingWindowComponent } from './components/invoice-editing-window/invoice-editing-window.component';
import { StatusEditingWindowComponent } from './components/status-editing-window/status-editing-window.component';
import { InvoicesPageGuard } from './guards/invoices-page.guard';

@NgModule({
    declarations: [
        InvoicesPageComponent,
        InvoicesTableComponent,
        InvoiceCreationWindowComponent,
        InvoiceEditingWindowComponent,
        StatusEditingWindowComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FeaturesModule,

        MainLayoutSharedModule,

        InvoicesPageRoutingModule
    ],
    entryComponents: [
        InvoiceCreationWindowComponent,
        InvoiceEditingWindowComponent,
        StatusEditingWindowComponent
    ],
    providers: [
        InvoicesPageGuard
    ]
})
export class InvoicesPageModule { }
