import { Component, OnInit } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { IInvoice } from '@App/Features/invoices/interfaces/invoice.interface';
import { Observable, Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material';

import { IRequestEvent } from '@App/Features/request-event';
import { tap } from 'rxjs/operators';
import { RequestEventService } from '@App/Features/request-event/request-event.service';
import { InvoicesService } from '@App/Features/invoices/invoices.service';

@Component({
    selector: 'app-invoice-creation-window',
    templateUrl: './invoice-creation-window.component.html',
    styleUrls: ['./invoice-creation-window.component.scss']
})
export class InvoiceCreationWindowComponent implements OnInit {

    public invoiceCreationForm: FormGroupTypeSafe<Partial<IInvoice>>;

    public requestEvents: IRequestEvent[];

    public createInvoiceSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<InvoiceCreationWindowComponent>,
        private invoicesService: InvoicesService,
        private requestEventService: RequestEventService
    ) { }

    public ngOnInit (): void {
        this.updateRequestEvents()
            .subscribe();
    }

    public createInvoice (): Observable<any> {
        return this.invoicesService.create(this.invoiceCreationForm.value);
    }

    public onCreateButtonClick (): void {
        this.createInvoiceSubscription = this.createInvoice()
            .subscribe(() => {
                this.close(true);
            });
    }

    public onInvoiceCreationFormInit (form: FormGroupTypeSafe<Partial<IInvoice>>): void {
        this.invoiceCreationForm = form;
    }

    public updateRequestEvents (): Observable<IRequestEvent[]> {
        return this.requestEventService.getAll()
            .pipe(
                tap(res => this.requestEvents = res)
            );
    }

    private close (status: boolean = false): void {
        this.dialogRef.close(status);
    }

}
