import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InvoiceStatusesEnum } from '@App/Features/invoices/shared/status.enum';
import { IInvoice } from '@App/Features/invoices/interfaces/invoice.interface';
import { Subscription } from 'rxjs';
import { InvoicesService } from '@App/Features/invoices/invoices.service';

@Component({
  selector: 'app-status-editing-window',
  templateUrl: './status-editing-window.component.html',
  styleUrls: ['./status-editing-window.component.scss']
})
export class StatusEditingWindowComponent implements OnInit {

    public status: InvoiceStatusesEnum;

    public invoiceChangeStatusSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<StatusEditingWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public invoice: Partial<IInvoice>,
        private invoicesService: InvoicesService
    ) { }

    public ngOnInit (): void {
        this.status = this.invoice.status;
    }

    public onClickSaveButton (): void {
        this.invoiceChangeStatusSubscription = this.invoicesService.changeStatus(this.invoice, this.status)
            .subscribe(() => {
                this.close(true);
            });
    }

    private close (status: boolean): void {
        this.dialogRef.close(status);
    }

}
