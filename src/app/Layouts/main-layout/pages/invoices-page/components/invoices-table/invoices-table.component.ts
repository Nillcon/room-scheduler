import { Component, OnInit, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IInvoice } from '@App/Features/invoices/interfaces/invoice.interface';
import { tap, switchMap, filter, delayWhen } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { MatDialog } from '@angular/material';
import { InvoiceCreationWindowComponent } from '../invoice-creation-window/invoice-creation-window.component';
import { InvoiceEditingWindowComponent } from '../invoice-editing-window/invoice-editing-window.component';
import { StatusEditingWindowComponent } from '../status-editing-window/status-editing-window.component';
import { InvoicesService } from '@App/Features/invoices/invoices.service';

@Component({
  selector: 'app-invoices-table',
  templateUrl: './invoices-table.component.html',
  styleUrls: ['./invoices-table.component.scss']
})
export class InvoicesTableComponent implements OnInit {

    public invoices: IInvoice[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private invoicesService: InvoicesService,
        private notifyService: NotificationService
    ) {}

    public ngOnInit (): void {
        this.updateInvoices()
            .subscribe();
    }

    public updateInvoices (): Observable<IInvoice[]> {
        return this.invoicesService.getAll()
            .pipe(
                tap(res => this.invoices = res)
            );
    }

    public openInvoiceCreationWindow (): Observable<any> {
        const dialog = this.dialog.open(InvoiceCreationWindowComponent);

        return dialog.afterClosed();
    }

    public openInvoiceEditingWindow (invoice: IInvoice): Observable<any> {
        const dialog = this.dialog.open(InvoiceEditingWindowComponent, {
            data: invoice
        });

        return dialog.afterClosed();
    }

    public openInvoiceStatusEditingWindow (invoice: IInvoice): Observable<any> {
        const dialog = this.dialog.open(StatusEditingWindowComponent, {
            data: invoice
        });

        return dialog.afterClosed();
    }

    public downloadPDFFile (invoice: IInvoice): Observable<Blob> {
        return this.invoicesService.downloadPdf(invoice);
    }

    public deleteInvoice (invoice: IInvoice): Observable<any> {
        return this.invoicesService.delete(invoice.id);
    }

    public onInvoicesCreateClick (): void {
        this.openInvoiceCreationWindow()
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateInvoices())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    public onInvoicesTablePDFClick (invoice: IInvoice): void {
        of(
            this.notifyService.infoToast({
                title: 'Download',
                text: 'PDF is downloading...',
                duration: Infinity
            })
        )
            .pipe(
                delayWhen(() => {
                    return this.downloadPDFFile(invoice);
                }),
                tap((notification) => {
                    notification.Close();
                })
            )
            .subscribe();
    }

    public onInvoicesTableStatusClick (invoice: IInvoice): void {
        this.openInvoiceStatusEditingWindow(invoice)
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateInvoices())
            )
            .subscribe();
    }

    public onInvoicesTableEdit (invoice: IInvoice): void {
        this.openInvoiceEditingWindow(invoice)
            .pipe(
                filter(res => !!res),
                switchMap(() => this.updateInvoices())
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

    @NeedsConfirmation()
    public onInvoicesTableDelete (invoice: IInvoice): void {
        this.deleteInvoice(invoice)
            .pipe(
                switchMap(() => this.updateInvoices()),
            )
            .subscribe(() => {
                this.notifyService.success({});
            });
    }

}
