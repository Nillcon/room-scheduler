import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IInvoice } from '@App/Features/invoices/interfaces/invoice.interface';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription, Observable } from 'rxjs';
import { IRequestEvent } from '@App/Features/request-event';
import { tap } from 'rxjs/operators';
import { RequestEventService } from '@App/Features/request-event/request-event.service';
import { InvoicesService } from '@App/Features/invoices/invoices.service';

@Component({
    selector: 'app-invoice-editing-window',
    templateUrl: './invoice-editing-window.component.html',
    styleUrls: ['./invoice-editing-window.component.scss']
})
export class InvoiceEditingWindowComponent implements OnInit {

    public invoiceEditingForm: FormGroupTypeSafe<Partial<IInvoice>>;

    public requestEvents: IRequestEvent[];

    public editingInvoiceSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<InvoiceEditingWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public invoice: Partial<IInvoice>,
        private invoicesService: InvoicesService,
        private requestEventService: RequestEventService
    ) {}

    public ngOnInit (): void {
        this.updateRequestEvents()
            .subscribe();
    }

    public editingInvoice (): Observable<any> {
        const value: Partial<IInvoice> = {
            ...this.invoiceEditingForm.value,
            id: this.invoice.id
        };

        return this.invoicesService.edit(value);
    }

    public updateRequestEvents (): Observable<IRequestEvent[]> {
        return this.requestEventService.getAll()
            .pipe(
                tap(res => this.requestEvents = res)
            );
    }

    public onEditingButtonClick (): void {
        this.editingInvoiceSubscription = this.editingInvoice()
            .subscribe(() => {
                this.close(true);
            });
    }

    public onInvoiceEditingFormInit (form: FormGroupTypeSafe<Partial<IInvoice>>): void {
        this.invoiceEditingForm = form;
    }


    private close (status: boolean = false): void {
        this.dialogRef.close(status);
    }

}
