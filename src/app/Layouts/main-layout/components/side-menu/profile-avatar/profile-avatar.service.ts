import { Injectable } from '@angular/core';
import { StringToColorService } from 'ngx-string-to-css-color';

@Injectable({
    providedIn: 'root'
})
export class ProfileAvatarService {

    constructor (
        private stringToColor: StringToColorService
    ) {}

    public getUserInitials (userName: string): string {
        const splitedName: Array<string> =  userName.split(' ');

        const firstLetter: string = splitedName[0][0];
        const secondLetter: string = (splitedName.length > 1) ? splitedName[splitedName.length - 1][0] : '';

        const result: string = (secondLetter) ? `${firstLetter}${secondLetter}` : firstLetter;

        return result;
    }

    public generateColorByInitials (userName: string): string {
        return this.stringToColor.stringToColor(userName, '0.1');
    }
}
