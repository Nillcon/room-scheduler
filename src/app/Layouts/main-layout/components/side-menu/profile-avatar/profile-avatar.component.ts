import { Component, OnInit, Input } from '@angular/core';
import { ProfileAvatarService } from './profile-avatar.service';

@Component({
    selector: 'app-profile-avatar',
    templateUrl: './profile-avatar.component.html',
    styleUrls: ['./profile-avatar.component.scss'],
    providers: [
        ProfileAvatarService
    ]
})
export class ProfileAvatarComponent implements OnInit {

    @Input() public userName: string = "";
    @Input() public userPhotoSrc: string = null;

    @Input() public width: string  = '100%';
    @Input() public height: string = '100%';

    public initials: string = '';
    public bgColor: string  = '';

    constructor (
        private prifileAvatarService: ProfileAvatarService
    ) { }

    public ngOnInit (): void {
        if (this.userName && !this.userPhotoSrc) {
            this.initials = this.prifileAvatarService.getUserInitials(this.userName);
            this.bgColor  = this.prifileAvatarService.generateColorByInitials(this.userName);
        }
    }

}
