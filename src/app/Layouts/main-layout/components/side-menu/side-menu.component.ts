import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { menuSizes } from './menu-sizes';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';
import { IUser, UserService } from '@App/Features/user';
import { IMenuItem } from '@Layouts/main-layout/interfaces/menu-item.interface';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { AppPagesEnum } from '@App/app-pages';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

    @Input()
    public menuItems: IMenuItem[];

    @Input()
    public userInfo: IUser;

    public menuSizes: typeof menuSizes = menuSizes;

    public menuWidth: string;
    public isOpened: boolean = false;
    public isFixed: boolean = false;

    constructor (
        private userService: UserService,
        private navigateService: NavigationService
    ) { }

    public ngOnInit (): void {
        this.initFixation();
    }

    public open (): void {
        this.isOpened = true;
        this.menuWidth = this.menuSizes['opened'];
    }

    public close (): void {
        this.isOpened = false;
        this.menuWidth = this.menuSizes['closed'];
    }

    public onMouseEnter (event: MouseEvent): void {
        if (!this.isFixed) {
            this.open();
        }
    }
    public onMouseLeave (event: MouseEvent): void {
        if (!this.isFixed) {
            this.close();
        }
    }

    public onLogOutButtonClick (): void {
        this.logOut();
    }

    private logOut (): void {
        this.userService.logout();

        this.navigateService.navigate(AppPagesEnum.SignIn);
    }

    private initFixation (): void {
        (this.isFixed) ? this.open() : this.close();
    }
}
