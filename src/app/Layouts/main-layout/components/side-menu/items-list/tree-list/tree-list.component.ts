import { Component, OnInit, Input } from '@angular/core';
import { IMenuItem } from '@Layouts/main-layout/interfaces/menu-item.interface';
import { NavigationService } from '@Core/root/navigation/navigation.service';

@Component({
  selector: 'app-tree-list',
  templateUrl: './tree-list.component.html',
  styleUrls: ['./tree-list.component.scss']
})
export class TreeListComponent implements OnInit {

    @Input()
    public listItems: IMenuItem[];

    @Input()
    public isOpened: boolean = true;

    @Input()
    public showTooltip: boolean = false;

    @Input()
    public textColor: string = '#ffffff';

    @Input()
    public queryParamsHandling = '';

    @Input()
    public wrapperClass: string = '';

    @Input()
    public itemClass: string = '';

    @Input()
    public iconClass: string = '';

    @Input()
    public textClass: string = '';

    @Input()
    public activeLinkClass: string = '';

    public currentLink: string;

    constructor (
        private navigationService: NavigationService
    ) { }

    public ngOnInit (): void {
        this.initCurrentLink();
    }

    public redirectTo (link: string): void {
        this.currentLink = link;

        this.navigationService.navigate(`Main/${link}`);
    }

    private initCurrentLink (): void {
        const currentUrl = this.navigationService.getCurrentUrl();

        for (const item of this.listItems) {
            if (currentUrl.includes(item.link)) {
                this.currentLink = item.link;

                break;
            }
        }
    }

}
