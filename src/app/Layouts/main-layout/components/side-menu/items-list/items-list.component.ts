import { Component, OnInit, Input, AfterContentInit, Output, EventEmitter } from '@angular/core';
import { menuSizes } from '../menu-sizes';
import { IMenuItem } from '@Layouts/main-layout/interfaces/menu-item.interface';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit, AfterContentInit {

    @Input()
    public isOpened: boolean = false;

    @Input()
    public menuItemsList: IMenuItem[];

    @Input()
    public showTooltip: boolean = false;

    @Input()
    public currentWidth: string;

    @Output()
    public OnClickAtTitle: EventEmitter<boolean> = new EventEmitter();

    public menuSizes: typeof menuSizes = menuSizes;

    constructor () { }

    public ngOnInit (): void {
    }

    public ngAfterContentInit (): void {
    }

    public ClickAtTitle (): void {
        this.OnClickAtTitle.emit(true);
    }

}
