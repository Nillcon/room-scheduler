import { Component, OnInit, Input } from '@angular/core';
import { IPage } from '@Core/root/navigation/interfaces/page.interface';
import { MainLayoutPagesMap } from '@Layouts/main-layout/shared/main-layout-pages.map';
import { MainLayoutPagesEnum } from '@Layouts/main-layout/main-layout-pages';

@Component({
    selector: 'page-title',
    templateUrl: './page-title.component.html',
    styleUrls: ['./page-title.component.scss']
})
export class PageTitleComponent implements OnInit {

    @Input() public title: string           = '';
    @Input() public titleClass: string      = 'text-title';
    @Input() public iconClass: string       = '';

    constructor () {}

    public ngOnInit (): void {
        if (!this.title && !this.iconClass) {
            const currentRoute = this.getCurrentRoute() as MainLayoutPagesEnum;
            const currentPage: IPage = MainLayoutPagesMap.get(currentRoute);

            if (currentPage) {
                this.title      = currentPage.name;
                this.iconClass  = currentPage.iconClass;
            }
        }
    }

    private getCurrentRoute (): string {
        const splittedPath: string[] = location.pathname.split('/').filter(item => {
            return item !== '';
        });
        return splittedPath[splittedPath.length - 1];
    }

}
