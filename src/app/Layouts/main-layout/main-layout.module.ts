import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainLayoutRoutingModule } from './main-layout.routing';
import { IndexPageModule } from './pages/index-page/index-page.module';
import { MainLayoutComponent } from './main-layout.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';

import { MatSidenavModule, MatMenuModule } from '@angular/material';

import { MainLayoutGuard } from './guards/main-layout.guard';
import { MainLayoutGuardService } from './services/guard.service';
import { SharedModule } from '@App/app-shared.module';
import { MainLayoutSharedModule } from './main-layout-shared.module';
import { ItemsListComponent } from './components/side-menu/items-list/items-list.component';
import { TreeListComponent } from './components/side-menu/items-list/tree-list/tree-list.component';
import { ProfileAvatarComponent } from './components/side-menu/profile-avatar/profile-avatar.component';
import { NgxStringToCssColorModule } from 'ngx-string-to-css-color';

@NgModule({
    declarations: [
        MainLayoutComponent,
        SideMenuComponent,
        ItemsListComponent,
        TreeListComponent,
        ProfileAvatarComponent
    ],
    imports: [
        CommonModule,

        MainLayoutRoutingModule,

        IndexPageModule,

        SharedModule,
        MainLayoutSharedModule,

        MatSidenavModule,
        MatMenuModule,

        NgxStringToCssColorModule
    ],
    exports: [
        MainLayoutComponent
    ],
    providers: [
        MainLayoutGuard,
        MainLayoutGuardService
    ]
})
export class MainLayoutModule {
    constructor () {}
}
