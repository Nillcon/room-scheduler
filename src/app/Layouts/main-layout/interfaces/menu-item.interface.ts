import { RoleEnum } from '@App/Features/role';

export interface IMenuItem {
    name: string;
    iconClass?: string;
    link?: string;
    roles?: RoleEnum[];
}
