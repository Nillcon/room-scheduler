import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutGuard } from './guards/main-layout.guard';
import { MainLayoutPagesEnum } from './main-layout-pages';

export const routes: Routes = [
    {
        path: MainLayoutPagesEnum.Main,
        loadChildren: '@Layouts/main-layout/pages/index-page/index-page.module#IndexPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Users,
        loadChildren: '@Layouts/main-layout/pages/users-page/users-page.module#UsersPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Equipment,
        loadChildren: '@Layouts/main-layout/pages/equipment-page/equipment-page.module#EquipmentPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Events,
        loadChildren: '@Layouts/main-layout/pages/events-page/events-page.module#EventsPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Rooms,
        loadChildren: '@Layouts/main-layout/pages/rooms-page/rooms-page.module#RoomsPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.EmailTemplates,
        loadChildren: '@Layouts/main-layout/pages/email-templates-page/email-templates-page.module#EmailTemplatesPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Services,
        loadChildren: '@Layouts/main-layout/pages/services-page/services-page.module#ServicesPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Reports,
        loadChildren: '@Layouts/main-layout/pages/reports-page/reports-page.module#ReportsPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Logs,
        loadChildren: '@Layouts/main-layout/pages/logs-page/logs-page.module#LogsPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: MainLayoutPagesEnum.Invoices,
        loadChildren: '@Layouts/main-layout/pages/invoices-page/invoices-page.module#InvoicesPageModule',
        canActivate: [
            MainLayoutGuard
        ]
    },
    {
        path: '',
        redirectTo: MainLayoutPagesEnum.Main,
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MainLayoutRoutingModule {}
