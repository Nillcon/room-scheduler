import { Injectable, OnDestroy } from "@angular/core";
import { UserService } from '@App/Features/user';
import { Subscription } from 'rxjs';
import { tap, filter, first } from 'rxjs/operators';
import { NavigationService } from '../../../Core/root/navigation/navigation.service';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@Injectable()
@AutoUnsubscribe()
export class MainLayoutGuardService implements OnDestroy {
    private userAuthStateSubscription: Subscription;

    constructor (
        private userService: UserService,
        private navigationService: NavigationService
    ) {
    }

    public ngOnDestroy (): void {}
}
