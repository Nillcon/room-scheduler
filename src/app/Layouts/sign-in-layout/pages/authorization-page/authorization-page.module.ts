import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizationPageComponent } from './authorization-page.component';
import { AuthorizationPageRoutingModule } from './authorization-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesModule } from '@Features/features.module';

@NgModule({
    declarations: [
        AuthorizationPageComponent
    ],
    imports: [
        CommonModule,
        SharedModule,

        FeaturesModule,
        AuthorizationPageRoutingModule
    ]
})
export class AuthorizationPageModule { }
