import { Component, OnInit } from '@angular/core';
import { wobble } from 'ng-animate';
import { trigger, transition, useAnimation } from '@angular/animations';
import { timer, Subscription, throwError } from 'rxjs';
import { ISignInForm, UserService } from '@App/Features/user';
import { FormGroupTypeSafe } from 'form-type-safe';
import { NavigationService } from '@Core/root/navigation/navigation.service';
import { AppPagesEnum } from '@App/app-pages';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'app-authorization-page',
    templateUrl: './authorization-page.component.html',
    styleUrls: ['./authorization-page.component.scss'],
    animations: [
        trigger('wobble', [transition('false => true', useAnimation(wobble))])
    ]
})
export class AuthorizationPageComponent implements OnInit {

    public readonly incorrectInputAnimTimeInSeconds: number = 1.2;
    public isAnimateIncorrectInput: boolean = false;

    public signInSubscription: Subscription;

    private authorizationFormGroup: FormGroupTypeSafe<ISignInForm>;

    constructor (
        private userService: UserService,
        private navigateService: NavigationService
    ) {}

    public ngOnInit (): void {}

    public signIn (): void {
        if (this.authorizationFormGroup.valid) {
            this.signInSubscription = this.userService.authorize(this.authorizationFormGroup.value)
                .pipe(
                    catchError((error) => {
                        this.onIncorrectInput();
                        return throwError(error);
                    })
                )
                .subscribe(() => {
                    this.navigateService.navigate(AppPagesEnum.Index);
                });
        } else {
            this.authorizationFormGroup.markAllAsTouched();
        }
    }

    public onAuthorizationFormInit (formGroup: FormGroupTypeSafe<ISignInForm>): void {
        this.authorizationFormGroup = formGroup;
    }

    public onIncorrectInput (): void {
        if (!this.isAnimateIncorrectInput) {
            this.isAnimateIncorrectInput = true;

            timer(this.incorrectInputAnimTimeInSeconds * 1000)
                .subscribe(() => {
                    this.isAnimateIncorrectInput = false;
                });
        }
    }

}
