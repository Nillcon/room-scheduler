import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserService } from '@App/Features/user';
import { NavigationService } from '@Core/root/navigation/navigation.service';

export class SignInLayoutGuard implements CanActivate {
    constructor (
        private userService: UserService,
        private navigationService: NavigationService
    ) {}

    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.userService.IsAuthorized$
        .pipe(
            map((authState) => {
                if (authState) {
                    this.navigationService.navigate('/');
                    return false;
                }

                return true;
            })
        );
    }
}
