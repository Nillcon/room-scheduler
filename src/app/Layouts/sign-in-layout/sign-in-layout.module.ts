import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInLayoutComponent } from './sign-in-layout.component';
import { SignInLayoutRoutingModule } from './sign-in-layout.routing';
import { AuthorizationPageModule } from './pages/authorization-page/authorization-page.module';
import { SignInLayoutGuard } from './guards/sign-in-layout.guard';

@NgModule({
    declarations: [
        SignInLayoutComponent
    ],
    imports: [
        CommonModule,

        SignInLayoutRoutingModule,

        AuthorizationPageModule
    ],
    exports: [
        SignInLayoutComponent
    ],
    providers: [
        SignInLayoutGuard
    ]
})
export class SignInLayoutModule { }
