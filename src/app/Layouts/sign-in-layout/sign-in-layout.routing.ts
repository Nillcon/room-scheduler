import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInLayoutGuard } from './guards/sign-in-layout.guard';

export const routes: Routes = [
    {
        path: '',
        loadChildren: '@Layouts/sign-in-layout/pages/authorization-page/authorization-page.module#AuthorizationPageModule',
        canActivate: [
            SignInLayoutGuard
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SignInLayoutRoutingModule {}
