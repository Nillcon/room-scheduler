import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundLayoutComponent } from './not-found-layout.component';
import { NotFoundLayoutRoutingModule } from './not-found-layout.routing';

@NgModule({
    declarations: [
        NotFoundLayoutComponent
    ],
    imports: [
        CommonModule,

        NotFoundLayoutRoutingModule
    ],
    exports: [
        NotFoundLayoutComponent
    ]
})
export class NotFoundLayoutModule { }
