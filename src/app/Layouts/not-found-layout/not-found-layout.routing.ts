import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '**',
        loadChildren: '@Layouts/not-found-layout/pages/not-found-page/not-found-page.module#NotFoundPageModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class NotFoundLayoutRoutingModule {}
