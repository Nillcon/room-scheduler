import { UserService } from '@App/Features/user';

export class AppConfig {
    constructor (
        private userService: UserService
    ) {}

    public load (): any {
        return this.userService.update()
            .toPromise()
            .catch(() => {
                return true;
            });
    }
}
