import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform  {
    constructor (
        private sanitizer: DomSanitizer
    ) {}

    public transform (url: string): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
