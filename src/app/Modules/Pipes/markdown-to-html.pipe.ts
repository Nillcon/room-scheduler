
import { DomSanitizer } from '@angular/platform-browser';
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'markdownToHtml'
})
export class MarkdownToHtmlPipe implements PipeTransform {

    constructor (
        private sanitizer: DomSanitizer
    ) {}

    public transform (textWithMarkdown: string): string {
        let textAsHtml: string = textWithMarkdown;
        textAsHtml = this.markuptext(textAsHtml, '**', 'strong');

        return textAsHtml;
    }

    private markuptext (text: string, identifier: string, htmltag: string): string {
        const array  = text.split(identifier);
        let previous = "";
        let newtext  = "";
        let previous_i;

        for (let i = 0; i < array.length; i++) {
            if ((i % 2) === 0) {
                previous_i = i - 1;
                array[previous_i] = `<${htmltag}>${previous}</${htmltag}>`;
            }

            previous = array[i];
        }

        for (let i = 0; i < array.length; i++) {
            newtext += array[i];
        }

        return newtext;
    }

}
