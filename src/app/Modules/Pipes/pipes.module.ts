import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgIsNumberPipeModule, NgSomePipeModule, NgWherePipeModule } from 'angular-pipes';
import { SafeHtmlPipe } from './safe-html.pipe';
import { SafeUrlPipe } from './safe-url.pipe';
import { MarkdownToHtmlPipe } from './markdown-to-html.pipe';

@NgModule({
    declarations: [
        SafeHtmlPipe,
        SafeUrlPipe,
        MarkdownToHtmlPipe
    ],
    imports: [
        CommonModule,

        NgIsNumberPipeModule,
        NgSomePipeModule,
        NgWherePipeModule
    ],
    exports: [
        SafeHtmlPipe,
        SafeUrlPipe,
        MarkdownToHtmlPipe,

        NgIsNumberPipeModule,
        NgSomePipeModule,
        NgWherePipeModule
    ]
})
export class PipesModule { }
