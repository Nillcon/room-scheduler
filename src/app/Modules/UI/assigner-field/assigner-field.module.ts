import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignerFieldComponent } from './assigner-field.component';
import { DxTagBoxModule } from 'devextreme-angular';

@NgModule({
    declarations: [
        AssignerFieldComponent
    ],
    imports: [
        CommonModule,

        DxTagBoxModule
    ],
    exports: [
        AssignerFieldComponent
    ]
})
export class AssignerFieldModule { }
