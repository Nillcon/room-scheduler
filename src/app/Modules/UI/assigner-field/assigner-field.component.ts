import { Component, OnInit, Input, Output, EventEmitter, ViewChild, forwardRef } from '@angular/core';
import { DxTagBoxComponent } from 'devextreme-angular';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
    selector: 'assigner-field',
    templateUrl: './assigner-field.component.html',
    styleUrls: ['./assigner-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AssignerFieldComponent),
            multi: true
        }
    ]
})
export class AssignerFieldComponent<T> implements OnInit, ControlValueAccessor {

    @Input()  public title: string;
    @Input()  public placeholder: string;
    @Input()  public autocompleteItems: T[];
    @Input()  public assignedItems: T[];
    @Input()  public showClearButton: boolean = true;
    @Input()  public searchEnabled: boolean = true;
    @Input()  public valueExpr: string;
    @Input()  public displayExpr: string;
    @Input()  public disabled: boolean;
    @Input()  public autocompleteUpdateFunction: (phrase?: string) => Observable<T[]>;

    @Output() public OnFocusIn: EventEmitter<any>           = new EventEmitter();
    @Output() public OnKeyUp: EventEmitter<any>             = new EventEmitter();

    @Output() public OnClearAssign: EventEmitter<boolean>   = new EventEmitter();
    @Output() public OnAssign: EventEmitter<T>              = new EventEmitter();
    @Output() public OnDeassign: EventEmitter<T>            = new EventEmitter();

    @ViewChild('TagBox', { static: false }) private tagBoxComponent: DxTagBoxComponent;

    constructor () { }

    public ngOnInit (): void {}

    public removeItemFromTagbox (itemToRemove: T): void {
        this.tagBoxComponent.value = (this.tagBoxComponent.value as T[]).filter(currItem => {
            if (this.valueExpr) {
                return (currItem[this.valueExpr] !== itemToRemove[this.valueExpr]);
            } else {
                return (currItem !== itemToRemove);
            }
        });
    }

    public updateAutocomplete (phrase: string = ''): void {
        if (typeof(this.autocompleteUpdateFunction) === 'function') {
            this.autocompleteUpdateFunction(phrase)
                .subscribe(items => {
                    this.autocompleteItems = items;
                });
        }
    }

    public onFocus (e: any): void {
        const value: string = e.event.target.value;

        if (!value) {
            this.updateAutocomplete();
        }

        this.OnFocusIn.next(e);
    }

    public onKeyDown (e: any): void {
        const searchValue: string = e.event.target.value;
        this.updateAutocomplete(searchValue);

        this.OnKeyUp.next(e);
    }

    public onValueChange (e: any): void {
        // Checks previous value and if new value is empty array,
        // sending request for clear all items from role
        const prevItems: T[]   = e.previousValue;
        const newItems: T[]    = e.value;

        if (prevItems.length > 0 && newItems.length === 0) {
            this.OnClearAssign.next(true);
        } else if (prevItems.length < newItems.length) {
            // Find and assign new items
            this.getDifferentItems(newItems, prevItems)
                .forEach(item => {
                    this.OnAssign.next(item);
                });
        } else if (prevItems.length > newItems.length) {
            // Find and deassign missing items
            this.getDifferentItems(prevItems, newItems)
                .forEach(item => {
                    this.OnDeassign.next(item);
                });
        }

        this.writeValue();
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (assignedItems?: T[]): void {
        if (assignedItems) {
            this.assignedItems = assignedItems;
        } else {
            this.onChange(this.assignedItems);
        }
    }

    public onChange: Function = () => {};

    private getDifferentItems (targetArray: T[], comparisonArray: T[]): T[] {
        return targetArray.filter(itemFromTargetArray => {
            const currItemIndex: number = comparisonArray.findIndex(itemFromComparisonArray => {
                return (itemFromTargetArray === itemFromComparisonArray);
            });

            return (currItemIndex === -1);
        });
    }

}
