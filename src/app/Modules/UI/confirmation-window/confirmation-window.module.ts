import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationWindowComponent } from './confirmation-window.component';
import { ConfirmationWindowService } from './confirmation-window.service';
import { MatDialogModule, MatButtonModule } from '@angular/material';
import { MatDialogHeaderModule } from '../mat-dialog-header/mat-dialog-header.module';
import { PipesModule } from '@Modules/Pipes/pipes.module';

@NgModule({
    declarations: [
        ConfirmationWindowComponent
    ],
    imports: [
        CommonModule,

        PipesModule,
        MatDialogHeaderModule,
        MatButtonModule,
        MatDialogModule
    ],
    entryComponents: [
        ConfirmationWindowComponent
    ],
    providers: [
        ConfirmationWindowService
    ]
})
export class ConfirmationWindowModule { }
