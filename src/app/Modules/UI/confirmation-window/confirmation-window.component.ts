import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';

interface ConfirmationData {
    title: string;
    message: string;
}

@ClassValidation()
@Component({
    selector: 'app-confirmation-window',
    templateUrl: './confirmation-window.component.html',
    styleUrls: [
        './confirmation-window.component.scss'
    ]
})
export class ConfirmationWindowComponent implements OnInit {

    @IsNotEmptyObject()
    public data: ConfirmationData;

    constructor (
        public dialogRef: MatDialogRef<ConfirmationWindowComponent>,
        @Inject(MAT_DIALOG_DATA) data: ConfirmationData
    ) {
        this.data = data;
    }

    public ngOnInit (): void {}

    public closeWithResult (result: boolean): void {
        this.dialogRef.close(result);
    }

}
