import { Component, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';

@Component({
    selector: 'date-range',
    templateUrl: './date-range.component.html',
    styleUrls: ['./date-range.component.scss'],
    providers: [
        {
            useExisting: DateRangeComponent,
            provide: NG_VALUE_ACCESSOR,
            multi: true
        }
    ]
})
export class DateRangeComponent implements OnInit, ControlValueAccessor {

    public formGroup: FormGroupTypeSafe<IDateRange>;

    constructor (
        private fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
        this.formGroupInit();
    }

    public registerOnChange (fn: any): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (dateRange?: IDateRange): void {
        if (dateRange) {
            this.formGroup.setValue(dateRange);
        }

        this.onChange(dateRange || this.formGroup.value);
    }

    public onChange: any = (value: IDateRange) => {};

    private formGroupInit (): void {
        this.formGroup = this.fb.group<IDateRange>({
            from: this.fb.control(new Date()),
            to: this.fb.control(new Date())
        });
    }
}
