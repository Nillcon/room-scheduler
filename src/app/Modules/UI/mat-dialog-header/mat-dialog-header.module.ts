import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogHeaderComponent } from './mat-dialog-header.component';
import { MatTooltipModule, MatButtonModule } from '@angular/material';

@NgModule({
    declarations: [
        MatDialogHeaderComponent
    ],
    imports: [
        CommonModule,

        MatButtonModule,
        MatTooltipModule
    ],
    exports: [
        MatDialogHeaderComponent
    ]
})
export class MatDialogHeaderModule { }
