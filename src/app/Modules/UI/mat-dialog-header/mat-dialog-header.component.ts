import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IsString, Length } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';

@ClassValidation()
@Component({
    selector: 'mat-dialog-header',
    templateUrl: './mat-dialog-header.component.html',
    styleUrls: ['./mat-dialog-header.component.scss']
})
export class MatDialogHeaderComponent implements OnInit {

    @Input()
    @IsString()
    @Length(0, 50)
    public title: string;

    @Input() public iconClass               = '';
    @Input() public titleClass              = 'text-primary';
    @Input() public closeButtonClass        = '';
    @Input() public closeButtonIconClass    = 'fas fa-times';
    @Input() public closeButtonText         = 'Close';
    @Input() public closeButtonEnabled      = true;

    @Output() public OnClose: EventEmitter<boolean> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

    public onCloseButtonClick (): void {
        this.OnClose.next(true);
    }

}
