import { Component, OnInit, ContentChild, Input, OnDestroy, TemplateRef, ElementRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'content-loader',
  templateUrl: './content-loader.component.html',
  styleUrls: ['./content-loader.component.scss']
})
export class ContentLoaderComponent implements OnInit, OnDestroy {

    @ContentChild(TemplateRef, {static: false})
    public detailRef: ElementRef;

    @Input() public set showIf (_isShow: any) {
        this.isShow = !!_isShow;
    }
    @Input() public set showIfObservable (_observable: Observable<any>) {
        if (this.showIsSubscription) {
            this.showIsSubscription.unsubscribe();
        }

        this.showIsSubscription = _observable
            .subscribe(() => this.isShow = true);
    }

    public isShow: boolean = false;
    private showIsSubscription: Subscription;

    constructor () { }

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}
}
