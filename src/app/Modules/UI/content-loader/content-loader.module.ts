import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentLoaderComponent } from './content-loader.component';
import { MatProgressSpinnerModule } from '@angular/material';

@NgModule({
    declarations: [
        ContentLoaderComponent
    ],
    imports: [
        CommonModule,
        MatProgressSpinnerModule
    ],
    exports: [
        ContentLoaderComponent
    ]
})
export class ContentLoaderModule { }
