import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@App/app-shared.module';
import { BlockMenuComponent } from './block-menu.component';

@NgModule({
    declarations: [
        BlockMenuComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        BlockMenuComponent
    ]
})
export class BlockMenuModule { }
