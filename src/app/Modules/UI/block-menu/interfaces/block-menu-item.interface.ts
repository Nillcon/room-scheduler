import { RoleEnum } from '@App/Features/role/shared/role.enum';

export interface IBlockMenuItem {
    id: number;
    name: string;
    iconClass?: string;
    link?: string;
    forbiddenFor?: RoleEnum[];
}
