import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IBlockMenuItem } from './interfaces';

@Component({
    selector: 'block-menu',
    templateUrl: './block-menu.component.html',
    styleUrls: ['./block-menu.component.scss']
})
export class BlockMenuComponent implements OnInit {

    @Input() public menuItems: IBlockMenuItem[];
    @Input() public selectedItem: IBlockMenuItem;

    @Output() public OnSelectItem: EventEmitter<IBlockMenuItem> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
        this.initSelectedItem();
    }

    public selectItem (item: IBlockMenuItem): void {
        this.selectedItem = item;
        this.OnSelectItem.emit(item);
    }

    private initSelectedItem (): void {
        if (!this.selectedItem) {
            this.selectItem(this.menuItems[0] || null);
        }
    }

}
