export interface IBreadcrumbsItem {
    name: string;
    link?: string;
    parameters?: object;
}
