import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IBreadcrumbsItem } from './interfaces/breadcrumbs-item.interface';
import { NavigationService } from '@Core/root/navigation/navigation.service';

@Component({
    selector: 'breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {

    @Input()
    public items: IBreadcrumbsItem[];

    @Output()
    public OnRedirect: EventEmitter<IBreadcrumbsItem> = new EventEmitter();

    constructor (
        private navigateService: NavigationService
    ) { }

    public ngOnInit (): void {
    }

    public redirectToLink (link: string, parameters?: {}): void {
        if (link) {
            this.navigateService.navigate(link, false, parameters);
        }
    }

}
