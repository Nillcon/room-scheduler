import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'fab-button',
    templateUrl: './fab-button.component.html',
    styleUrls: ['./fab-button.component.scss']
})
export class FabButtonComponent implements OnInit {

    @Input() public color: string = 'primary';
    @Input() public iconClass: string = 'fas fa-plus';
    @Input() public tooltipText = '';
    @Input() public tooltipPosition = 'above';

    constructor () {}

    public ngOnInit (): void {}
}
