import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FabButtonComponent } from './fab-button.component';
import { MatButtonModule, MatTooltipModule } from '@angular/material';

@NgModule({
    declarations: [
        FabButtonComponent
    ],
    imports: [
        CommonModule,

        MatButtonModule,
        MatTooltipModule
    ],
    exports: [
        FabButtonComponent
    ]
})
export class FabButtonModule {}
