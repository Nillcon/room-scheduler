import { FormControl } from '@angular/forms';

export function EmailValidator (control: FormControl): { emailNotValid: boolean } {
    const regular: RegExp = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z].)+[a-zA-Z]{2,9})$/i;
    return regular.test(control.value) ? null : { emailNotValid: true };
}
