export enum AppPagesEnum {
    Index = 'Main',
    Visitor = 'Visitor',
    SignIn = 'SignIn',
    NotFound = '404'
}
