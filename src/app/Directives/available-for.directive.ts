import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { ElementRenderableFeatures } from "@Directives/element-renderable-features";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { IUser, UserService } from '@App/Features/user';
import { RoleEnum } from '@App/Features/role';

@AutoUnsubscribe()
@Directive({
    selector: "[AvailableFor]"
})
export class AvailableForDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {

    private userData: IUser;
    private userInfoSubscription: Subscription;

    @Input('AvailableFor') private rolesList: RoleEnum[];

    constructor (
        element: ElementRef,
        templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        private userService: UserService
    ) {
        super(element, templateRef, viewContainer);
    }

    public ngOnInit (): void {
        this.userInfoSubscription = this.userService.Data$
            .subscribe(data => {
                this.userData = data;

                this.processElement();
            });
    }

    public ngOnDestroy (): void {}

    private processElement (): void {
        let isElementAvailable: boolean = false;

        if (this.rolesList) {
            try {
                if (this.userService.includesOneOfRoles(this.rolesList)) {
                    isElementAvailable = true;
                }
            } catch (err) {}
        } else {
            isElementAvailable = true;
        }

        (isElementAvailable) ? this.showElement() : this.hideElement();
    }

}
