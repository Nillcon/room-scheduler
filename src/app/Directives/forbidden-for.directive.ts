import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { ElementRenderableFeatures } from "@Directives/element-renderable-features";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { IUser, UserService } from '@App/Features/user';
import { RoleEnum } from '@App/Features/role';

@AutoUnsubscribe()
@Directive({
    selector: "[ForbiddenFor]"
})
export class ForbiddenForDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {

    private userData: IUser;
    private userInfoSubscription: Subscription;

    @Input('ForbiddenFor') private rolesList: RoleEnum[];

    constructor (
        element: ElementRef,
        templateRef: TemplateRef<any>,
        viewContainer: ViewContainerRef,
        private userService: UserService
    ) {
        super(element, templateRef, viewContainer);
    }

    public ngOnInit (): void {
        this.userInfoSubscription = this.userService.Data$
            .subscribe(data => {
                this.userData = data;

                this.processElement();
            });
    }

    public ngOnDestroy (): void {}

    private processElement (): void {
        let isElementForbidden: boolean = false;

        if (this.rolesList) {
            try {
                if (this.userService.includesOneOfRoles(this.rolesList)) {
                    isElementForbidden = true;
                }
            } catch (err) {}
        }

        (isElementForbidden) ? this.hideElement() : this.showElement();
    }

}
