import { ElementRef, TemplateRef, ViewContainerRef } from "@angular/core";

export class ElementRenderableFeatures {
    constructor (
        private _elem: ElementRef,
        private _templateRef: TemplateRef<any>,
        private _viewContainer: ViewContainerRef,
    ) {}

    protected hideElement (): void {
        this._viewContainer.clear();
    }

    protected showElement (): void {
        this.hideElement();
        this._viewContainer.createEmbeddedView(this._templateRef);
    }
}
