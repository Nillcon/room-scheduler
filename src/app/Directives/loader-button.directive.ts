import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { ElementRenderableFeatures } from "@Directives/element-renderable-features";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";

@AutoUnsubscribe()
@Directive({
    selector: "[ButtonLoader]"
})
export class ButtonLoaderDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {

    @Input('ButtonLoader') public set subscription (subscription: Subscription) {
        if (subscription) {
            this._subscription = subscription;
            this.processElement();
        }
    }

    private _subscription: Subscription;
    private _isLoading: boolean = false;
    private _defaultHtml: string;
    private _loadingElement: HTMLDivElement;

    constructor (
        private element: ElementRef,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef
    ) {
        super(element, templateRef, viewContainer);
    }

    public ngOnInit (): void {
        this.showElement();
    }
    public ngOnDestroy (): void {}

    private processElement (): void {
        if (!this._isLoading) {
            this._isLoading = true;

            this.createLoaderForButton();

            const subscriptionResolver: Promise<any> = new Promise(resolve => {
                this._subscription.add(resolve);
            });

            subscriptionResolver.then(() => {
                this.removeLoaderFromButton();
                this._isLoading = false;
            });
        }
    }

    private createLoaderForButton (): void {
        const elementWithDirective: HTMLElement = this.element.nativeElement.nextElementSibling;
        const targetElem: HTMLElement = this.getButtonElement(elementWithDirective);

        this.createLoader();

        targetElem.firstElementChild['style'].display = 'none';
        targetElem.insertBefore(this._loadingElement, targetElem.firstElementChild);
    }

    private removeLoaderFromButton (): void {
        const elementWithDirective: HTMLElement = this.element.nativeElement.nextElementSibling;
        const targetElem: HTMLElement = this.getButtonElement(elementWithDirective);

        this.removeLoader();
        targetElem.firstElementChild['style'].display = '';
    }

    private getButtonElement (elementWithDirective: HTMLElement): HTMLElement {
        let targetElem: HTMLElement;

        if (elementWithDirective.tagName === 'BUTTON') {
            targetElem = elementWithDirective;
        } else {
            targetElem = elementWithDirective.querySelector('button');
        }

        if (!targetElem) {
            throw new Error('ButtonLoader directive works only with <button> elements!');
        }

        return targetElem;
    }

    private createLoader (): void {
        this._loadingElement = document.createElement('div');
        this._loadingElement.innerHTML = '<div class="button-loader"><div></div><div></div><div></div><div></div></div>';
    }

    private removeLoader (): void {
        if (this._loadingElement) {
            this._loadingElement.remove();
        }
    }

}
