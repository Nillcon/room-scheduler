import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ISvc } from '@Features/svc';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';

@BaseForm()
@Component({
    selector: 'svc-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss']
})
export class CreationFormComponent implements OnInit, IBaseForm<Partial<ISvc>> {

    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<ISvc>>> = new EventEmitter();

    public readonly priceMinVal: number = 0;

    public createSubscription: Subscription;

    public formGroup: FormGroupTypeSafe<Partial<ISvc>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.formBuilder.group<Partial<ISvc>>({
            name: this.formBuilder.control(
                '',
                [Validators.required]
            ),
            description: this.formBuilder.control(''),
            price: this.formBuilder.control(
                0,
                [
                    Validators.required,
                    Validators.min(this.priceMinVal)
                ]
            ),
            isShowForClient: this.formBuilder.control(false)
        });
    }

}
