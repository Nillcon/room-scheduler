import { Component, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { ISvc } from '@Features/svc/interfaces/svc.interface';
import { IsArray, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { MatCheckbox } from '@angular/material';

@ClassValidation()
@Component({
    selector: 'svc-list',
    templateUrl: './entities-list.component.html',
    styleUrls: ['./entities-list.component.scss']
})
export class EntitiesListComponent implements OnInit, AfterViewInit {

    @Input()
    @IsArray()
    public svcArray: ISvc[] = [];

    @Input()
    @IsBoolean()
    public isSelectable: boolean = false;

    @Input()
    @IsArray()
    public selectedSvcIds: number[] = [];

    @Output() public OnSelection: EventEmitter<ISvc[]> = new EventEmitter();

    public selectedSvcList: Set<ISvc> = new Set();

    @ViewChildren('SvcCheckbox')
    private svcCheckboxesList: QueryList<MatCheckbox>;

    constructor () {}

    public ngOnInit (): void {}

    public ngAfterViewInit (): void {
        this.initSvcCheckboxesSelection();
    }

    public onSvcSelectionChange (svc: ISvc): void {
        if (this.selectedSvcList.has(svc)) {
            this.selectedSvcList.delete(svc);
        } else {
            this.selectedSvcList.add(svc);
        }

        this.emitSelectionData();
    }

    private emitSelectionData (): void {
        this.OnSelection.next([...this.selectedSvcList]);
    }

    private initSvcCheckboxesSelection (): void {
        if (this.selectedSvcIds.length) {
            this.svcCheckboxesList.forEach((checkboxComponent) => {
                const isSvcSelected: boolean = this.selectedSvcIds.includes(+checkboxComponent.id);
                checkboxComponent.checked = isSvcSelected;

                if (isSvcSelected) {
                    this.selectedSvcList.add(checkboxComponent.value as any);
                }
            });

            this.emitSelectionData();
        }
    }

}
