import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ISvc } from '@Features/svc';

@Component({
    selector: 'services-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()  public svcArray: Partial<ISvc>[];
    @Output() public OnEditButtonClick: EventEmitter<ISvc> = new EventEmitter();
    @Output() public OnDeleteButtonClick: EventEmitter<ISvc> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
