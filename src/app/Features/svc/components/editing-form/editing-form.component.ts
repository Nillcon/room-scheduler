import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ISvc } from '@Features/svc';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';

@BaseForm()
@Component({
    selector: 'svc-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss']
})
export class EditingFormComponent implements OnInit, IBaseForm<Partial<ISvc>> {

    @Input()  public inputData: ISvc;
    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<ISvc>>> = new EventEmitter();

    public readonly priceMinVal: number = 0;

    public formGroup: FormGroupTypeSafe<Partial<ISvc>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (svc: ISvc): void {
        this.formGroup = this.formBuilder.group<Partial<ISvc>>({
            name: this.formBuilder.control(
                svc.name || '',
                [Validators.required]
            ),
            description: this.formBuilder.control(
                svc.description || ''
            ),
            price: this.formBuilder.control(
                svc.price || 0,
                [
                    Validators.required,
                    Validators.min(this.priceMinVal)
                ]
            ),
            isShowForClient: this.formBuilder.control(svc.isShowForClient || false)
        });
    }

}
