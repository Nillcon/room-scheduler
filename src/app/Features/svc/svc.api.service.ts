import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { ISvc } from '@Features/svc';

@Injectable()
export class SvcApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAll (): Observable<ISvc[]> {
        return this.httpService.get<ISvc[]>(`/Svc`);
    }

    public get (id: number): Observable<ISvc> {
        return this.httpService.get<ISvc>(`/Svc/${id}`);
    }

    public getAllForClients (): Observable<ISvc[]> {
        return this.httpService.get(`/Svc/ForClients`);
    }

    public create (data: Partial<ISvc>): Observable<any> {
        return this.httpService.post(`/Svc`, data);
    }

    public edit (id: number, data: Partial<ISvc>): Observable<any> {
        return this.httpService.put(`/Svc/${id}`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/Svc/${id}`);
    }

}
