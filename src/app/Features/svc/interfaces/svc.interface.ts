export interface ISvc {
    id: number;
    name: string;
    description: string;
    price: number;
    isShowForClient: boolean;
}
