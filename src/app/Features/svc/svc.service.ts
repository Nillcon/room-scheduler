import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ISvc } from '@Features/svc';
import { SvcApiService } from './svc.api.service';

@Injectable()
export class SvcService {

    constructor (
        private svcApiService: SvcApiService
    ) {}

    public getAll (): Observable<ISvc[]> {
        return this.svcApiService.getAll();
    }

    public get (id: number): Observable<ISvc> {
        return this.svcApiService.get(id);
    }

    public getAllForClients (): Observable<ISvc[]> {
        return this.svcApiService.getAllForClients();
    }

    public create (data: Partial<ISvc>): Observable<any> {
        return this.svcApiService.create(data);
    }

    public edit (id: number, data: Partial<ISvc>): Observable<any> {
        return this.svcApiService.edit(id, data);
    }

    public delete (id: number): Observable<any> {
        return this.svcApiService.delete(id);
    }

}
