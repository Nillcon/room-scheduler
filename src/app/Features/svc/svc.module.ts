import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvcApiService } from './svc.api.service';
import { SvcService } from './svc.service';
import { ListTableComponent } from './components/list-table/list-table.component';
import { SharedModule } from '@App/app-shared.module';
import { CreationFormComponent } from './components/creation-form/creation-form.component';
import { EditingFormComponent } from './components/editing-form/editing-form.component';
import { EntitiesListComponent } from './components/entities-list/entities-list.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListTableComponent,
        EntitiesListComponent,
        CreationFormComponent,
        EditingFormComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    exports: [
        ListTableComponent,
        EntitiesListComponent,
        CreationFormComponent,
        EditingFormComponent
    ],
    providers: [
        SvcApiService,
        SvcService
    ]
})
export class SvcModule { }
