export * from './components/creation-form/creation-form.component';
export * from './components/editing-form/editing-form.component';
export * from './components/list-table/list-table.component';

export * from './interfaces/svc.interface';

export * from './svc.api.service';
export * from './svc.module';