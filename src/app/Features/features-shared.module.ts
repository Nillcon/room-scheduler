import { NgModule } from '@angular/core';
import { GetInvoiceStatusDataByStatusPipe } from '@Features/invoices/pipes/get-invoice-status-data-by-status.pipe';
import { GetEquipmentDataByTypePipe } from './equipment/pipes/get-equipment-data-by-type.pipe';
import { GetActionLogDataByTypePipe } from './action-log/pipes/get-action-log-data-by-type.pipe';
import { GetRoomColorDataByColorIdPipe } from './room/pipes/get-room-color-data-by-color-id.pipe';

@NgModule({
    declarations: [
        GetEquipmentDataByTypePipe,
        GetActionLogDataByTypePipe,
        GetInvoiceStatusDataByStatusPipe,
        GetRoomColorDataByColorIdPipe
    ],
    exports: [
        GetEquipmentDataByTypePipe,
        GetActionLogDataByTypePipe,
        GetInvoiceStatusDataByStatusPipe,
        GetRoomColorDataByColorIdPipe
    ]
})
export class FeaturesSharedModule {}
