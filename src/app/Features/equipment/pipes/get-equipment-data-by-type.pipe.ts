import { PipeTransform, Pipe } from '@angular/core';
import { EquipmentTypeEnum } from '../shared/equipment-type.enum';
import { IEquipmentType } from '../interfaces/equipment-type.interface';
import { EquipmentTypeMap } from '../shared/equipment-type.map';

@Pipe({
    name: 'getEquipmentDataByType'
})
export class GetEquipmentDataByTypePipe implements PipeTransform {

    constructor () {}

    public transform (type: EquipmentTypeEnum): IEquipmentType {
        return EquipmentTypeMap.get(type);
    }

}
