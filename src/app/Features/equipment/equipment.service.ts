import { Injectable } from '@angular/core';
import { IEquipment } from './interfaces/equipment.interface';
import { EquipmentTypeEnum } from './shared/equipment-type.enum';
import { EquipmentApiService } from './equipment.api.service';
import { Observable } from 'rxjs';

@Injectable()
export class EquipmentService {

    constructor (
        private equipmentApiService: EquipmentApiService
    ) {}

    public getAll (): Observable<IEquipment[]> {
        return this.equipmentApiService.getAll();
    }

    public get (id: number): Observable<IEquipment> {
        return this.equipmentApiService.get(id);
    }

    public create (data: Partial<IEquipment>): Observable<any> {
        return this.equipmentApiService.create(data);
    }

    public edit (id: number, data: Partial<IEquipment>): Observable<any> {
        return this.equipmentApiService.edit(id, data);
    }

    public delete (id: number): Observable<any> {
        return this.equipmentApiService.delete(id);
    }

    public getDistinctTypes (equipment: IEquipment[]): EquipmentTypeEnum[] {
        return [...new Set(equipment.map(elem => elem.type))];
    }

}
