export enum EquipmentTypeEnum {
    Table,
    Chair,
    Media,
    Other
}
