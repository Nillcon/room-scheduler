import { EquipmentTypeEnum } from './equipment-type.enum';
import { IEquipmentType } from '@Features/equipment';

export const EquipmentTypeMap: Map<EquipmentTypeEnum, IEquipmentType> = new Map<EquipmentTypeEnum, IEquipmentType>()
    .set(
        EquipmentTypeEnum.Table,
        {
            id: EquipmentTypeEnum.Table,
            name: 'Table',
            iconClass: 'fas fa-archive'
        }
    )
    .set(
        EquipmentTypeEnum.Chair,
        {
            id: EquipmentTypeEnum.Chair,
            name: 'Chair',
            iconClass: 'fas fa-chair'
        }
    )
    .set(
        EquipmentTypeEnum.Media,
        {
            id: EquipmentTypeEnum.Media,
            name: 'Media',
            iconClass: 'fas fa-desktop'
        }
    )
    .set(
        EquipmentTypeEnum.Other,
        {
            id: EquipmentTypeEnum.Other,
            name: 'Other',
            iconClass: 'fas fa-ellipsis-h'
        }
    );
