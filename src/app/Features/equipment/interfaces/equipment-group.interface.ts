import { IEquipment } from './equipment.interface';

export interface IEquipmentGroup {
    count: number;
    equipment: IEquipment;
}
