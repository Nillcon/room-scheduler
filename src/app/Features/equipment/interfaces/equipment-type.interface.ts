import { EquipmentTypeEnum } from '@Features/equipment';

export interface IEquipmentType {
    id: EquipmentTypeEnum;
    name: string;
    iconClass: string;
}
