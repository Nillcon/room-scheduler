import { EquipmentTypeEnum } from '@Features/equipment';

export interface IEquipment {
    id: number;
    code: string;
    name: string;
    type: EquipmentTypeEnum;
    description: string;
}
