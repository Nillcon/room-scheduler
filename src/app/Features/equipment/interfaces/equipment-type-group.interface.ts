import { IEquipmentGroup } from "./equipment-group.interface";
import { EquipmentTypeEnum } from "../shared/equipment-type.enum";

export interface IEquipmentTypeGroup {
    count: number;
    equipmentGroups: IEquipmentGroup[];
    roomId: string;
    type: EquipmentTypeEnum;
}
