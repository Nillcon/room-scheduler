import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { IEquipment } from '@Features/equipment';

@Injectable()
export class EquipmentApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAll (): Observable<IEquipment[]> {
        return this.httpService.get<IEquipment[]>(`/Equipment`);
    }

    public get (id: number): Observable<IEquipment> {
        return this.httpService.get<IEquipment>(`/Equipment/${id}`);
    }

    public create (data: Partial<IEquipment>): Observable<any> {
        return this.httpService.post(`/Equipment`, data);
    }

    public edit (id: number, data: Partial<IEquipment>): Observable<any> {
        return this.httpService.put(`/Equipment/${id}`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/Equipment/${id}`);
    }

}
