import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { IEquipment } from '@Features/equipment/interfaces/equipment.interface';
import { IEquipmentType } from '@Features/equipment/interfaces/equipment-type.interface';
import { EquipmentTypeMap } from '@Features/equipment/shared/equipment-type.map';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';

@BaseForm()
@Component({
    selector: 'equipment-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss']
})
export class EditingFormComponent implements OnInit, IBaseForm<Partial<IEquipment>> {

    @Input()  public inputData: Partial<IEquipment>;
    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IEquipment>>> = new EventEmitter();

    public equipmentTypeArray: IEquipmentType[] = [...EquipmentTypeMap.values()];

    public formGroup: FormGroupTypeSafe<Partial<IEquipment>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (equipment: IEquipment): void {
        this.formGroup = this.formBuilder.group<Partial<IEquipment>>({
            name: this.formBuilder.control(
                equipment.name,
                [Validators.required]
            ),
            description: this.formBuilder.control(
                equipment.description
            ),
            code: this.formBuilder.control(
                equipment.code
            ),
            type: this.formBuilder.control(
                equipment.type,
                [Validators.required]
            )
        });
    }

}
