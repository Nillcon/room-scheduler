import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { IEquipment } from '@Features/equipment/interfaces/equipment.interface';
import { IEquipmentType } from '@Features/equipment/interfaces/equipment-type.interface';
import { EquipmentTypeMap } from '@Features/equipment/shared/equipment-type.map';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';

@BaseForm()
@Component({
    selector: 'equipment-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss']
})
export class CreationFormComponent implements OnInit, IBaseForm<Partial<IEquipment>> {

    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IEquipment>>> = new EventEmitter();

    public equipmentTypeArray: IEquipmentType[] = [...EquipmentTypeMap.values()];

    public formGroup: FormGroupTypeSafe<Partial<IEquipment>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.formBuilder.group<Partial<IEquipment>>({
            name: this.formBuilder.control(
                '',
                [Validators.required]
            ),
            description: this.formBuilder.control(''),
            code: this.formBuilder.control(
                ''
            ),
            type: this.formBuilder.control(
                undefined,
                [Validators.required]
            )
        });
    }

}
