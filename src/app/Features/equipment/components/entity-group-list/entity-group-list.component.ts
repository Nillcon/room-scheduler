import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IEquipmentGroup } from '@Features/equipment/interfaces/equipment-group.interface';

@ClassValidation()
@Component({
    selector: 'equipment-group-list',
    templateUrl: './entity-group-list.component.html',
    styleUrls: ['./entity-group-list.component.scss']
})
export class EntityGroupListComponent implements OnInit {

    @Input()
    @IsArray()
    public equipmentGroups: IEquipmentGroup[];

    @Input() public isSelectable: boolean = false;

    @Output() public OnSelection: EventEmitter<IEquipmentGroup[]> = new EventEmitter();

    public selectedEquipmentGroups: Set<IEquipmentGroup> = new Set<IEquipmentGroup>();

    constructor () {}

    public ngOnInit (): void {}

    public onEquipmentGroupSelectionChange (equipmentGroup: IEquipmentGroup): void {
        if (this.selectedEquipmentGroups.has(equipmentGroup)) {
            this.selectedEquipmentGroups.delete(equipmentGroup);
        } else {
            this.selectedEquipmentGroups.add(equipmentGroup);
        }
    }

}
