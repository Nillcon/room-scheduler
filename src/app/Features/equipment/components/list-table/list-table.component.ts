import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IEquipment } from '@Features/equipment/interfaces/equipment.interface';

@Component({
    selector: 'equipment-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()
    public equipment: IEquipment[];

    @Output()
    public OnEditButtonClick: EventEmitter<IEquipment> = new EventEmitter();

    @Output()
    public OnDeleteButtonClick: EventEmitter<IEquipment> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
