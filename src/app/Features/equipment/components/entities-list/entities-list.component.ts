import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEquipment } from '@Features/equipment/interfaces/equipment.interface';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';

@ClassValidation()
@Component({
    selector: 'equipment-list',
    templateUrl: './entities-list.component.html',
    styleUrls: ['./entities-list.component.scss']
})
export class EntitiesListComponent implements OnInit {

    @Input()
    @IsArray()
    public equipmentArray: IEquipment[];

    @Input() public isSelectable: boolean = false;

    @Output() public OnSelection: EventEmitter<IEquipment[]> = new EventEmitter();

    public selectedEquipment: Set<IEquipment> = new Set<IEquipment>();

    constructor () {}

    public ngOnInit (): void {}

    public onEquipmentSelectionChange (equipment: IEquipment): void {
        if (this.selectedEquipment.has(equipment)) {
            this.selectedEquipment.delete(equipment);
        } else {
            this.selectedEquipment.add(equipment);
        }
    }

}
