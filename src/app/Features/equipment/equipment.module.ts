import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentApiService } from './equipment.api.service';
import { ListTableComponent } from './components/list-table/list-table.component';
import { SharedModule } from '@App/app-shared.module';
import { EditingFormComponent } from './components/editing-form/editing-form.component';
import { CreationFormComponent } from './components/creation-form/creation-form.component';
import { EntitiesListComponent } from './components/entities-list/entities-list.component';
import { EntityGroupListComponent } from './components/entity-group-list/entity-group-list.component';
import { EquipmentService } from './equipment.service';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListTableComponent,
        EntitiesListComponent,
        CreationFormComponent,
        EditingFormComponent,
        EntityGroupListComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    providers: [
        EquipmentApiService,
        EquipmentService
    ],
    exports: [
        ListTableComponent,
        EntitiesListComponent,
        CreationFormComponent,
        EditingFormComponent,
        EntityGroupListComponent
    ]
})
export class EquipmentModule { }
