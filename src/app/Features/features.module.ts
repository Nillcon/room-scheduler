import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventModule } from './event/event.module';
import { RoleModule } from './role/role.module';
import { UserModule } from './user/user.module';
import { SpaceModule } from './space/space.module';
import { RoomModule } from './room/room.module';
import { EquipmentModule } from './equipment/equipment.module';
import { SvcModule } from './svc/svc.module';
import { EmailTemplateModule } from './email-template/email-template.module';
import { RoomGroupModule } from './room-group/room-group.module';
import { ActionLogModule } from './action-log/action-log.module';
import { RequestEventModule } from './request-event';
import { InvoicesModule } from './invoices/invoices.module';
import { ReportModule } from './report/report.module';

@NgModule({
    declarations: [

    ],
    imports: [
        CommonModule,

        EventModule,
        RequestEventModule,
        RoleModule,
        UserModule,
        SpaceModule,
        RoomModule,
        RoomGroupModule,
        EquipmentModule,
        SvcModule,
        EmailTemplateModule,
        ActionLogModule,
        InvoicesModule,
        ReportModule
    ],
    exports: [
        EventModule,
        RequestEventModule,
        RoleModule,
        UserModule,
        SpaceModule,
        RoomModule,
        RoomGroupModule,
        EquipmentModule,
        SvcModule,
        EmailTemplateModule,
        ActionLogModule,
        InvoicesModule,
        ReportModule
    ]
})
export class FeaturesModule {}
