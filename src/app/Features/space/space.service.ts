import { Injectable } from '@angular/core';
import { ISpace } from './interfaces/space.interface';
import { IRoom } from '@Features/room';
import { SpaceApiService } from './space.api.service';
import { Observable } from 'rxjs';
import { flatMap, map, toArray } from 'rxjs/operators';
import { RoomService } from '@Features/room/room.service';

@Injectable()
export class SpaceService {

    constructor (
        private spaceApiService: SpaceApiService,
        private roomService: RoomService
    ) { }

    public getSpacesWithFullRoomsObject (rooms: IRoom[]): Observable<ISpace[]> {
        return this.spaceApiService.getAll()
            .pipe(
                flatMap(res => res),
                map(space => {
                    space.rooms = space.rooms
                        .map(room => this.roomService.getRoomById(rooms, room.id))
                        .filter(room => !!room);

                    return space;
                }),
                toArray()
            );
    }

    public get (id: number): Observable<ISpace> {
        return this.spaceApiService.get(id);
    }

    public getAll (): Observable<ISpace[]> {
        return this.spaceApiService.getAll();
    }

    public edit (space: ISpace): Observable<any> {
        return this.spaceApiService.edit(space);
    }

    public create (space: ISpace): Observable<any> {
        return this.spaceApiService.create(space);
    }

    public delete (space: ISpace): Observable<any> {
        return this.spaceApiService.delete(space);
    }

}
