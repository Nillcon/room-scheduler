import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ISpace } from '@Features/space/interfaces/space.interface';
import { MatSelectChange } from '@angular/material';

@Component({
  selector: 'space-item-selector',
  templateUrl: './item-selector.component.html',
  styleUrls: ['./item-selector.component.scss']
})
export class ItemSelectorComponent implements OnInit {

    @Input()
    public spaces: ISpace[];

    @Input()
    public set selectedSpace (selectedSpace: ISpace) {
        this._selectedSpace = selectedSpace;
        this.initSelectedSpaceId();
    }

    @Output()
    public OnSelectionChanged: EventEmitter<ISpace> = new EventEmitter();

    public _selectedSpace: ISpace;
    public selectedSpaceId: number;

    constructor () { }

    public ngOnInit (): void {
    }

    public selectionChange (matSelectChange: MatSelectChange): void {
        const selectedSpaceId = matSelectChange.value;

        let selectedSpace: ISpace;

        if (selectedSpaceId) {
            selectedSpace = this.getSpaceById(selectedSpaceId);
        } else {
            this.selectedSpaceId = null;
        }

        this.OnSelectionChanged.emit(selectedSpace);
        this.selectedSpace = selectedSpace;
    }

    private initSelectedSpaceId (): void {
        this.selectedSpaceId = this._selectedSpace ? this._selectedSpace.id : null;
    }

    private getSpaceById (id: number): ISpace {
        return this.spaces.find(res => res.id === id);
    }

}
