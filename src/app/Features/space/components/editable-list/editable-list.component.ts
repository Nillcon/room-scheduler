import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ISpace } from '@Features/space';

@Component({
  selector: 'space-editable-list',
  templateUrl: './editable-list.component.html',
  styleUrls: ['./editable-list.component.scss']
})
export class EditableListComponent implements OnInit {

    @Input()
    public spaces: ISpace[];

    @Output()
    public OnEdit: EventEmitter<ISpace> = new EventEmitter();

    @Output()
    public OnDelete: EventEmitter<ISpace> = new EventEmitter();

    constructor (
    ) { }

    public ngOnInit (): void {
    }

    public onSpaceEdit (space: ISpace): void {
        this.OnEdit.emit(space);
    }

    public onSpaceDelete (space: ISpace): void {
        this.OnDelete.emit(space);
    }

}
