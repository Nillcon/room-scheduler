import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { FormBuilderTypeSafe, FormGroupTypeSafe } from 'form-type-safe';
import { IRoom } from '@Features/room';
import { ISpace } from '@Features/space';
import { Validators } from '@angular/forms';

@BaseForm()
@Component({
    selector: 'space-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnDestroy, IBaseForm<ISpace> {

    @Input()
    public inputData: ISpace;

    @Input()
    public rooms: IRoom[];

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<ISpace>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<ISpace>;

    constructor (
        private fb: FormBuilderTypeSafe,
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (space: ISpace): void {
        this.formGroup = this.fb.group<ISpace>({
            name: this.fb.control(
                (space) ? space.name : '', [
                Validators.required
            ]),
            rooms: this.fb.control(
                (space) ? space.rooms : [], [
                Validators.required
            ])
        });
    }

}
