import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaceApiService } from './space.api.service';
import { SpaceService } from './space.service';
import { SharedModule } from '@App/app-shared.module';
import { EditableListComponent } from './components/editable-list/editable-list.component';
import { FormComponent } from './components/form/form.component';
import { ItemSelectorComponent } from './components/item-selector/item-selector.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        EditableListComponent,
        FormComponent,
        ItemSelectorComponent,
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    providers: [
        SpaceService,
        SpaceApiService
    ],
    exports: [
        FormComponent,
        EditableListComponent,
        ItemSelectorComponent
    ]
})
export class SpaceModule { }
