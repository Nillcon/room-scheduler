import { IRoom } from '@Features/room';

export interface ISpace {
    id?: number;
    name: string;
    rooms: IRoom[];
}
