import { Injectable } from '@angular/core';
import { ISpace } from '@Features/space';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { Observable } from 'rxjs';

@Injectable()
export class SpaceApiService {

    constructor (
        private httpService: HttpRequestService
    ) { }

    public get (id: number): Observable<ISpace> {
        return this.httpService.get<ISpace>(`/Spaces/${id}`);
    }

    public getAll (): Observable<ISpace[]> {
        return this.httpService.get<ISpace[]>('/Spaces');
    }

    public edit (space: ISpace): Observable<any> {
        return this.httpService.put(`/Spaces/${space.id}`, space);
    }

    public create (space: ISpace): Observable<any> {
        return this.httpService.post<any>('/Spaces', space);
    }

    public delete (space: ISpace): Observable<any> {
        return this.httpService.delete(`/Spaces/${space.id}`);
    }

}
