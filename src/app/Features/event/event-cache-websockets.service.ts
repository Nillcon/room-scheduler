import { Injectable, OnDestroy } from "@angular/core";
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { IEventCacheData } from './interfaces/event-cache-data.interface';
import { IEventCacheMessage } from './interfaces/event-cache-message.interface';
import { EventCacheWsEvent as CacheEvent } from './shared/event-cache-ws-event.enum';
import { HttpRequestService } from '@Core/root/http/http-request.service';

@Injectable()
export class EventCacheWebsocketsService implements OnDestroy {

    public get connectionStatus$ (): Observable<boolean> {
        return this._connectionStatus$.asObservable();
    }

    public get cacheData$ (): Observable<IEventCacheData> {
        return this._cacheData$.asObservable();
    }

    public get onSuccessMessage$ (): Observable<IEventCacheMessage> {
        return this._onSuccess$.asObservable();
    }

    public get onErrorMessage$ (): Observable<IEventCacheMessage> {
        return this._onError$.asObservable();
    }

    private connection: HubConnection;

    private _connectionStatus$: BehaviorSubject<boolean>    = new BehaviorSubject(false);
    private _cacheData$: BehaviorSubject<IEventCacheData>   = new BehaviorSubject(null);
    private _onSuccess$: Subject<IEventCacheMessage>        = new Subject();
    private _onError$: Subject<IEventCacheMessage>          = new Subject();

    private readonly hourInMilliseconds = 3600000;

    constructor (
        private httpService: HttpRequestService,
        private storageService: LocalStorageService
    ) {}

    public ngOnDestroy (): void {}

    public startConnection (): Observable<boolean> {
        this.stopConnection();

        this.hubListener();

        return new Observable((subscriber) => {
            this.connection.start()
                .then(() => {
                    const connectionStatus: boolean = true;

                    this._connectionStatus$.next(connectionStatus);
                    subscriber.next(connectionStatus);
                })
                .catch(() => {
                    const connectionStatus: boolean = false;

                    this._connectionStatus$.next(connectionStatus);
                    subscriber.next(connectionStatus);
                });

            this.connection.serverTimeoutInMilliseconds = this.hourInMilliseconds;
        });
    }

    public stopConnection (): void {
        if (this.connection) {
            this.connection.stop()
                .then(() => {
                    this._connectionStatus$.next(false);
                })
                .catch(() => {});
        }
    }

    public updateCache (): void {
        this.connection.invoke(CacheEvent.Update)
            .catch(() => {});
    }

    private hubListener (): void {
        const host  = this.httpService.host;
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.connection = new HubConnectionBuilder()
            .withUrl(`${host}/EventsCacheHub`, { accessTokenFactory: () => token })
            .build();

        this.connection.on(CacheEvent.OnData, (data) => {
            this.onNewCacheData(data);
        });

        this.connection.on(CacheEvent.OnSuccessMessage, (data) => {
            this.onSuccessMessage(data);
        });

        this.connection.on(CacheEvent.OnErrorMessage, (data) => {
            this.onErrorMessage(data);
        });
    }

    private onNewCacheData (data: IEventCacheData): void {
        this._cacheData$.next(data);
    }

    private onSuccessMessage (message: IEventCacheMessage): void {
        this._onSuccess$.next(message);
    }

    private onErrorMessage (message: IEventCacheMessage): void {
        this._onError$.next(message);
    }

}
