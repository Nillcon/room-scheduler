import { Injectable } from '@angular/core';
import { IEvent } from './interfaces/event.interface';
import { IRoom } from '@App/Features/room';
import { Observable } from 'rxjs';
import { DateService } from '@Core/root/date/date.service';
import { EventApiService } from './event.api.service';
import { EventCacheWebsocketsService } from './event-cache-websockets.service';
import { IEventCacheData } from './interfaces/event-cache-data.interface';
import { IEventCacheMessage } from './interfaces/event-cache-message.interface';
import DevExpress from 'devextreme/bundles/dx.all';
import { ActionEventArgs } from '@syncfusion/ej2-schedule';

@Injectable()
export class EventService {

    public get cacheConnectionStatus$ (): Observable<boolean> {
        return this.eventCacheWsService.connectionStatus$;
    }

    public get cacheData$ (): Observable<IEventCacheData> {
        return this.eventCacheWsService.cacheData$;
    }

    public get onCacheSuccessMessage$ (): Observable<IEventCacheMessage> {
        return this.eventCacheWsService.onSuccessMessage$;
    }

    public get onCacheErrorMessage$ (): Observable<IEventCacheMessage> {
        return this.eventCacheWsService.onErrorMessage$;
    }

    constructor (
        private dateService: DateService,
        private eventApiService: EventApiService,
        private eventCacheWsService: EventCacheWebsocketsService
    ) {}

    public getByFilters (start?: Date, end?: Date, rooms?: string[]): Observable<IEvent[]> {
        const _start = this.dateService.getLocalJsonTimeString(start);
        const _end = this.dateService.getLocalJsonTimeString(end);

        return this.eventApiService.getByFilters(_start, _end, rooms);
    }

    public getEventsByRooms (roomsId: string[]): Observable<IEvent[]> {
        return this.eventApiService.getEventsByRooms(roomsId);
    }

    public getAll (): Observable<IEvent[]> {
        return this.eventApiService.getAll();
    }

    public getAllWithOData (options: DevExpress.data.LoadOptions): Observable<IEvent[]> {
        return this.eventApiService.getAllWithOData(options);
    }

    public updateActionEvent (actionEventArgs: ActionEventArgs): Observable<any> {
        return this.eventApiService.updateActionEvent(actionEventArgs);
    }

    public getRoomEvents (events: IEvent[], rooms: IRoom[]): IEvent[] {
        const _events: IEvent[] = JSON.parse(JSON.stringify(events));

        return _events
            .map(event => {
                event.roomsId = event.roomsId
                    .filter(roomId => {
                        return !!rooms
                            .filter(room => {
                                return room.id === roomId;
                            })
                            .length;
                    });

                return event;
            })
            .filter(event => !!event.roomsId.length);
    }

    public delete (eventUId: string, eventICalUid: string): Observable<any> {
        return this.eventApiService.delete(eventUId, eventICalUid);
    }

    public startCacheConnection (): Observable<boolean> {
        return this.eventCacheWsService.startConnection();
    }

    public stopCacheConnection (): void {
        this.eventCacheWsService.stopConnection();
    }

    public updateCache (): void {
        this.eventCacheWsService.updateCache();
    }

}
