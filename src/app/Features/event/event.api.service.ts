import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEvent } from '@App/Features/event';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import DevExpress from 'devextreme/bundles/dx.all';
import { ActionEventArgs } from '@syncfusion/ej2-schedule';

@Injectable()
export class EventApiService {

    constructor (
        private httpService: HttpRequestService,
    ) {}

    public getEventsByRooms (roomsId: string[]): Observable<IEvent[]> {
        return this.httpService.get<IEvent[]>(`/Events`);
    }

    public getAll (): Observable<IEvent[]> {
        return this.httpService.get<IEvent[]>(`/Events`);
    }

    public getAllWithOData (options: DevExpress.data.LoadOptions): Observable<IEvent[]> {
        return this.httpService.get<IEvent[]>(`/Events/OData?loadOptions=${JSON.stringify(options)}`);
    }

    public updateActionEvent (actionEventArgs: ActionEventArgs): Observable<any> {
        return this.httpService.post(`/Events/UpdateData`, actionEventArgs);
    }

    public getByFilters (start?: string, end?: string, rooms?: string[]): Observable<IEvent[]> {
        return this.httpService.get<IEvent[]>(
            `/Events/GetByFilters` +
            `?${(start) ? 'start=' + start : ''}` +
            `&${(end) ? 'end=' + end : ''}` +
            `&${(rooms) ? 'rooms=' + rooms : ''}`
        );
    }

    public delete (eventUId: string, eventICalUid: string): Observable<any> {
        return this.httpService.delete(`/Events?id=${eventUId}&iCalUid=${eventICalUid}`);
    }

}
