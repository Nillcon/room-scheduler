export enum ColumnEnum {
    Name,
    DateStart,
    DateEnd,
    Location,
    Author,
    AllDay,
    Edit,
    Delete
}
