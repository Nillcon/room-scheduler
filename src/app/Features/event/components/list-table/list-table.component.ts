import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IEvent } from '@Features/event';
import CustomStore from 'devextreme/data/custom_store';
import { ColumnEnum } from './shared/list-table-columns.enum';

@Component({
    selector: 'events-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()
    public events: IEvent[];

    @Input()
    public dataSource: CustomStore;

    @Input()
    public itemIdToSelect: string;

    @Input()
    public set forbiddenColumns (columns: ColumnEnum[]) {
        const newDictionary: { [key: number]: boolean } = {};

        columns.forEach((currColumn) => {
            newDictionary[currColumn] = true;
        });

        this.forbiddenColumnsDictionary = newDictionary;
    }

    @Output()
    public OnEditButtonClick: EventEmitter<IEvent> = new EventEmitter();

    @Output()
    public OnDeleteButtonClick: EventEmitter<IEvent> = new EventEmitter();

    public columnsEnum: typeof ColumnEnum = ColumnEnum;

    public forbiddenColumnsDictionary: { [key: number]: boolean } = {};

    constructor () {}

    public ngOnInit (): void {}

}
