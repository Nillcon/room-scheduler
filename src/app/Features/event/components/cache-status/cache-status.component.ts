import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IEventCacheData } from '@Features/event/interfaces/event-cache-data.interface';
import { EventCacheStatusEnum } from '@Features/event/shared/event-cache-status.enum';

@ClassValidation()
@Component({
    selector: 'event-cache-status',
    templateUrl: './cache-status.component.html',
    styleUrls: ['./cache-status.component.scss']
})
export class CacheStatusComponent implements OnInit {

    @Input()
    public set cacheData (data: IEventCacheData) {
        this._cacheData = data;

        if (data) {
            switch (data.status) {
                case EventCacheStatusEnum.Updated:
                    this.updateButtonColor = 'primary';
                    break;
                case EventCacheStatusEnum.Updating:
                    this.updateButtonColor = '';
                    break;
                case EventCacheStatusEnum.Error:
                    this.updateButtonColor = 'warn';
                    break;
            }
        }
    }

    public get cacheData (): IEventCacheData {
        return this._cacheData;
    }

    @Output()
    public OnUpdateButtonClick: EventEmitter<boolean> = new EventEmitter();

    public updateButtonColor: string = 'primary';

    public cacheStatusEnum: typeof EventCacheStatusEnum = EventCacheStatusEnum;

    private _cacheData: IEventCacheData;

    constructor () {}

    public ngOnInit (): void {}

    public onUpdateButtonClick (): void {
        if (this.cacheData.status !== EventCacheStatusEnum.Updating) {
            this.OnUpdateButtonClick.emit(true);
        }
    }

}
