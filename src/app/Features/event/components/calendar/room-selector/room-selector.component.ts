import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { IRoom } from '@Features/room';
import { MatSelectionListChange, MatSelectionList } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
    selector: 'app-room-selector',
    templateUrl: './room-selector.component.html',
    styleUrls: ['./room-selector.component.scss']
})
export class RoomSelectorComponent implements OnInit {

    @Input()
    public rooms: IRoom[];

    @Input()
    public selectedRooms: IRoom[];

    @Output()
    public OnSelectionChanged: EventEmitter<IRoom[]> = new EventEmitter();

    @ViewChild(MatSelectionList, {static: false})
    public selectionList: MatSelectionList;

    constructor () { }

    public ngOnInit (): void {
    }

    public trackByRooms (index: number, item: IRoom): string {
        return item.id;
    }

    public compareWith (room1: any, room2: any): boolean {
        return room1.id === room2.id;
    }

    public selectAll (): void {
        this.updateSelectedRooms(this.rooms);

    }

    public deselectAll (): void {
        this.updateSelectedRooms([]);
    }

    public updateSelectedRooms (selectedRooms: IRoom[]): void {
        this.selectedRooms = selectedRooms;
        this.OnSelectionChanged.next(this.selectedRooms);
    }

    public onListChanged (event: MatSelectionListChange): void {
       this.updateSelectedRooms(this.selectedRooms);
    }

    public onClickSelectAll (): void {
        this.selectAll();
    }

    public onClickDeselectAll (): void {
        this.deselectAll();
    }

}
