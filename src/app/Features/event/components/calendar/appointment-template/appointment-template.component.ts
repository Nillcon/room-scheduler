import { Component, OnInit, Input } from '@angular/core';
import { IEvent } from '@Features/event';
import { CalendarViewType } from '@Features/event/shared/calendar-view-type.enum';

@Component({
  selector: 'app-appointment-template',
  templateUrl: './appointment-template.component.html',
  styleUrls: ['./appointment-template.component.scss']
})
export class AppointmentTemplateComponent implements OnInit {

    @Input()
    public event: IEvent;

    @Input()
    public currentView: CalendarViewType;

    public CalendarViewType = CalendarViewType;

    constructor () { }

    public ngOnInit (): void {
    }

}
