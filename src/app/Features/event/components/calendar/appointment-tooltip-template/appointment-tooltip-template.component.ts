import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEvent } from '@App/Features/event';

@Component({
  selector: 'app-appointment-tooltip-template',
  templateUrl: './appointment-tooltip-template.component.html',
  styleUrls: ['./appointment-tooltip-template.component.scss']
})
export class AppointmentTooltipTemplateComponent implements OnInit {

    @Input() public event: IEvent;

    @Output() public OnEditBtnClick: EventEmitter<boolean> = new EventEmitter();
    @Output() public OnDeleteBtnClick: EventEmitter<boolean> = new EventEmitter();
    @Output() public OnCloseBtnClick: EventEmitter<boolean> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public emitClickBtn (eventEmitter: EventEmitter<boolean>): void {
        eventEmitter.next(true);
    }

}
