import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-tooltip-toolbar',
    templateUrl: './tooltip-toolbar.component.html',
    styleUrls: ['./tooltip-toolbar.component.scss']
})
export class TooltipToolbarComponent implements OnInit {

    @Output() public OnEditBtnClick: EventEmitter<boolean> = new EventEmitter();
    @Output() public OnDeleteBtnClick: EventEmitter<boolean> = new EventEmitter();
    @Output() public OnCloseBtnClick: EventEmitter<boolean> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public emitClickBtn (eventEmitter: EventEmitter<boolean>): void {
        eventEmitter.next(true);
    }

}
