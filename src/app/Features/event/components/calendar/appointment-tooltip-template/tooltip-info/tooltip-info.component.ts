import { Component, OnInit, Input } from '@angular/core';
import { IEvent } from '@App/Features/event';

@Component({
    selector: 'app-tooltip-info',
    templateUrl: './tooltip-info.component.html',
    styleUrls: ['./tooltip-info.component.scss']
})
export class TooltipInfoComponent implements OnInit {

    @Input() public event: IEvent;

    constructor () { }

    public ngOnInit (): void {
    }

}
