import { Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

import { IRoom } from '@Features/room/interfaces/room.interface';
import { RoomColorMap } from '@Features/room/shared/room-color.map';
import { IEvent } from '@Features/event';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';

import { CalendarViewType } from '@Features/event/shared/calendar-view-type.enum';

import {
    EventSettingsModel,
    ScheduleComponent,
    GroupModel,
    ActionEventArgs,
    PopupCloseEventArgs,
    EventRenderedArgs,
    PopupOpenEventArgs
} from '@syncfusion/ej2-angular-schedule';
import { Predicate, Query } from '@syncfusion/ej2-data';

@AutoUnsubscribe()
@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnDestroy {

    @Input()
    public currentDate: Date;

    @Input()
    public isReadOnly: boolean = false;

    @Input()
    public set events (events: IEvent[]) {
        events = events.map(event => {
            event.location = event.roomNames.join('; ');

            return event;
        });

        this._events = events;

        this.eventSettings = {...this.eventSettings, dataSource: events};
    }

    @Input()
    public rooms: IRoom[];

    @Input()
    public set selectedRooms (selectedRooms: IRoom[]) {
        if (selectedRooms) {
            this._selectedRooms = selectedRooms;

            if (this.scheduleObj) {
                this.changeCalendarQuery();
            }
        }
    }

    @Input()
    public currentView: CalendarViewType = CalendarViewType.Day;

    @Output()
    public OnActionEventChanged: EventEmitter<ActionEventArgs> = new EventEmitter();

    @Output()
    public OnDateRangeChanged: EventEmitter<IDateRange> = new EventEmitter();

    @Output()
    public OnSelectedRoomsChanged: EventEmitter<IRoom[]> = new EventEmitter();

    @ViewChild("scheduleObj", {static: false})
    public scheduleObj: ScheduleComponent;

    public readonly CalendarViewType = CalendarViewType;

    public group: GroupModel = {
        enableCompactView: true,
        byGroupID: true,
        resources: ["Rooms"]
    };

    public _events: IEvent[] = [];
    public _selectedRooms: IRoom[];

    public eventSettings: EventSettingsModel = {
        fields: {
            id: 'uid',
            subject: { name: 'name' },
            startTime: { name: 'start' },
            endTime: { name: 'end' },
            recurrenceRule: { name: 'recurrenceRule' },
            isAllDay: { name: 'isAllDayEvent' },
            recurrenceException: { name: 'recurrenceException' },
            recurrenceID: { name: 'recurrenceID' },
            location: { name: 'location' },
            description: { name: 'description' },
        },
    };

    public isCalendarInit: boolean = false;
    private isDateRangeInit: boolean = false;

    private readonly lightColor: string = '#ffffff';
    private readonly darkColor: string = '#4a4a4a';

    constructor (
    ) { }

    public ngOnInit (): void {
        if (!this._selectedRooms) {
            this._selectedRooms = this.rooms;
        }
    }

    public ngOnDestroy (): void {}

    // Events
    public onCalendarCreated (): void {
        this.changeCalendarQuery();
    }

    public onRoomSelectionChanged (rooms: IRoom[]): void {
        this.OnSelectedRoomsChanged.emit(rooms);
        this._selectedRooms = rooms;

        this.changeCalendarQuery();
    }

    public onCalendarViewChange (event: CalendarViewType): void {
        this.currentView = event;

        if (this.scheduleObj) {
            setTimeout(() => {
                this.setSelectedDate();
            });
        }
    }

    public onCalendarPopupOpen (event: PopupCloseEventArgs): void {
        if (event.type === 'Editor') {
            if (event.data['uid']) {
                this.disableOwnerSelectorInPopup();
            } else {
                this.enableOwnerSelectorInPopup();
            }
        } else if (event.type === 'QuickInfo') {
            this.changeQuickInfoPopupStyles(event);
        } else {
            this.enableOwnerSelectorInPopup();
        }
    }

    public onCalendarDateChanged (event: any): void {
        if (this.scheduleObj) {
            setTimeout(() => {
                this.setSelectedDate();
            });
        }
    }

    public onEventRendered (args: EventRenderedArgs): void {
        const event: Partial<IEvent> = <Partial<IEvent>>args.data;
        const color = RoomColorMap.get(event.color);

        if (color) {
            args.element.style.color = (RoomColorMap.get(event.color).tint === 'light') ? this.darkColor : this.lightColor;
        }
    }

    public changeCalendarQuery (): void {
        let predicate: Predicate;

        this._selectedRooms.forEach((room: IRoom) => {
                if (predicate) {
                    predicate = predicate.or('roomId', 'equal', room.id);
                } else {
                    predicate = new Predicate('roomId', 'equal', room.id);
                }
        });

        this.scheduleObj.eventSettings.query = new Query().where(predicate);
    }

    public changeQuickInfoPopupStyles (args: PopupOpenEventArgs): void {
        const event: Partial<IEvent> = <Partial<IEvent>>args.data;

        if (event.color) {
            const color = (RoomColorMap.get(event.color).tint === 'light') ? this.darkColor : this.lightColor;

            const elementHeder: HTMLElement = document.querySelector('.e-quick-popup-wrapper .e-popup-header .e-subject');
            const elementEdit: HTMLElement = document.querySelector('.e-quick-popup-wrapper .e-popup-header .e-edit');
            const elementDelete: HTMLElement = document.querySelector('.e-quick-popup-wrapper .e-popup-header .e-delete');
            const elementClose: HTMLElement = document.querySelector('.e-quick-popup-wrapper .e-popup-header .e-close');

            elementHeder.style.color = color;
            elementEdit.style.color = color;
            elementDelete.style.color = color;
            elementClose.style.color = color;
        }
    }

    public disableOwnerSelectorInPopup (): void {
        const roomIdSelector: HTMLElement = document.querySelector('#EditForm .e-dialog-parent .e-resources-row .e-roomId-container');
        const multiselect: HTMLElement = roomIdSelector.querySelector('.e-multiselect');

        roomIdSelector.style.pointerEvents = 'none';

        multiselect.style.backgroundColor = ' #f0f0f0';
        multiselect.style.paddingTop = '2px';
    }

    public enableOwnerSelectorInPopup (): void {
        const roomIdSelector: HTMLElement = document.querySelector('#EditForm .e-dialog-parent .e-resources-row .e-roomId-container');
        const multiselect: HTMLElement = roomIdSelector.querySelector('.e-multiselect');

        roomIdSelector.style.pointerEvents = 'auto';

        multiselect.style.backgroundColor = '#ffffff';
        multiselect.style.paddingTop = 'none';
    }

    public onDataBinding (event: any): void {
        if (!this.isDateRangeInit) {
            this.setSelectedDate();

            this.isCalendarInit = true;
            this.isDateRangeInit = true;
        }
    }

    public actionEventArgsEmmiting (event: ActionEventArgs): void {
        const availiableResponseType = ['eventCreated', 'eventRemoved', 'eventChanged'];

        if (availiableResponseType.includes(event.requestType)) {
            this.OnActionEventChanged.emit(event);
        }
    }

    private setSelectedDate (): void {
        const selectedDays: Date[] = <Date[]>this.scheduleObj.getCurrentViewDates();

        if (selectedDays.length) {
            this.OnDateRangeChanged.emit({
                from: selectedDays[0],
                to: selectedDays[selectedDays.length - 1]
            });
        }
    }
}
