import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@App/app-shared.module';

import { DxDropDownBoxModule, DxScrollViewModule } from 'devextreme-angular';

import { EventCacheWebsocketsService } from './event-cache-websockets.service';
import { EventApiService } from './event.api.service';
import { EventService } from './event.service';
import { SpaceModule } from '@Features/space';

import { AppointmentTooltipTemplateComponent } from './components/calendar/appointment-tooltip-template/appointment-tooltip-template.component';
import { TooltipToolbarComponent } from './components/calendar/appointment-tooltip-template/tooltip-toolbar/tooltip-toolbar.component';
import { TooltipInfoComponent } from './components/calendar/appointment-tooltip-template/tooltip-info/tooltip-info.component';
import { AppointmentTemplateComponent } from './components/calendar/appointment-template/appointment-template.component';
import { RoomSelectorComponent } from './components/calendar/room-selector/room-selector.component';
import { CacheStatusComponent } from './components/cache-status/cache-status.component';
import { ListTableComponent } from './components/list-table/list-table.component';
import { CalendarComponent } from './components/calendar/calendar.component';

import {
    ScheduleModule,
    DayService,
    WeekService,
    WorkWeekService,
    MonthService,
    AgendaService,
    MonthAgendaService
} from '@syncfusion/ej2-angular-schedule';
import { FeaturesSharedModule } from '@Features/features-shared.module';


@NgModule({
    declarations: [
        CalendarComponent,
        AppointmentTooltipTemplateComponent,
        TooltipToolbarComponent,
        TooltipInfoComponent,
        AppointmentTemplateComponent,
        ListTableComponent,
        RoomSelectorComponent,
        CacheStatusComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule,

        SpaceModule,

        DxDropDownBoxModule,
        DxScrollViewModule,
        ScheduleModule
    ],
    exports: [
        CalendarComponent,
        ListTableComponent,
        CacheStatusComponent
    ],
    providers: [
        EventService,
        EventApiService,

        DayService,
        WeekService,
        WorkWeekService,
        MonthService,
        AgendaService,
        MonthAgendaService,
        EventCacheWebsocketsService
    ]
})
export class EventModule { }
