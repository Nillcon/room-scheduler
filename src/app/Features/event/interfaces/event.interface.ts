export interface IEvent {
    uid: string;
    iCalUid: string;
    name: string;
    start: string;
    end: string;
    color: number;
    description: string;
    roomId: string;
    roomsId: string[];
    roomNames: string[];
    recurrenceRule: string;
    location: string;
    isAllDayEvent: boolean;
    appointmentType: number;
    theatreId: number;
}
