export interface IEventCacheMessage {
    title: string;
    message: string;
}
