import { EventCacheStatusEnum } from '../shared/event-cache-status.enum';

export interface IEventCacheData {
    lastUpdateDate: string;
    status: EventCacheStatusEnum;
    progress: number;
}
