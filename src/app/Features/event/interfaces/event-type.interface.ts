import { EventTypeEnum } from '@Features/event';

export interface IEventType {
    id: EventTypeEnum;
    name: string;
}
