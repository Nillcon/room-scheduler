export enum EventCacheStatusEnum {
    Updated,
    Updating,
    Error
}
