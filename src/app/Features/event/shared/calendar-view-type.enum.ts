export enum CalendarViewType {
    Day = 'Day',
    Week = 'Week',
    Month = 'Month',
    WorkWeek = 'WorkWeek',
    Agenda = 'Agenda',
    MonthAgenda = 'MonthAgenda'
}
