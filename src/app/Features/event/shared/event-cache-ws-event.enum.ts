export enum EventCacheWsEvent {
    Update              = 'Update',
    OnData              = 'OnData',
    OnSuccessMessage    = 'OnSuccess',
    OnErrorMessage      = 'OnError'
}
