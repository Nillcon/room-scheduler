import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailTemplateApiService } from './email-template.api.service';
import { EmailTemplateService } from './email-template.service';
import { ListTableComponent } from './components/list-table/list-table.component';
import { SharedModule } from '@App/app-shared.module';
import { CreationFormComponent } from './components/creation-form/creation-form.component';
import { MarkdownEditorComponent } from './components/markdown-editor/markdown-editor.component';
import { DxHtmlEditorModule } from 'devextreme-angular';
import { FormComponent } from './components/form/form.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListTableComponent,
        CreationFormComponent,
        MarkdownEditorComponent,
        FormComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule,

        DxHtmlEditorModule
    ],
    providers: [
        EmailTemplateApiService,
        EmailTemplateService
    ],
    exports: [
        ListTableComponent,
        MarkdownEditorComponent,
        FormComponent
    ],
})
export class EmailTemplateModule {}
