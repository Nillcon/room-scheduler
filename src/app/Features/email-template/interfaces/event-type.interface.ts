import { EventTypeEnum } from '../shared/event-type.enum';

export interface IEventType {
    id: EventTypeEnum;
    name: string;
}
