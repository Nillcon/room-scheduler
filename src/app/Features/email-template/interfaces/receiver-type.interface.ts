import { ReceiverTypeEnum } from '../shared/receiver-type.enum';

export interface IReceiverType {
    id: ReceiverTypeEnum;
    name: string;
}
