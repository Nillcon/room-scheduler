import { RoleEnum } from '@Features/role';
import { EventTypeEnum } from '@Features/event';

export interface IEmailTemplate {
    id: number;
    name: string;
    subject: string;
    text: string;
    receiverType: RoleEnum;
    eventType: EventTypeEnum;
    isActive: boolean;
}
