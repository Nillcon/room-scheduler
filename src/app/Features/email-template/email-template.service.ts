import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEmailTemplate } from '@Features/email-template';
import { EmailTemplateApiService } from './email-template.api.service';

@Injectable()
export class EmailTemplateService {

    constructor (
        private emailTemplateApiService: EmailTemplateApiService
    ) {}

    public getAll (): Observable<IEmailTemplate[]> {
        return this.emailTemplateApiService.getAll();
    }

    public get (id: number): Observable<IEmailTemplate> {
        return this.emailTemplateApiService.get(id);
    }

    public create (data: Partial<IEmailTemplate>): Observable<any> {
        return this.emailTemplateApiService.create(data);
    }

    public edit (id: number, data: Partial<IEmailTemplate>): Observable<any> {
        return this.emailTemplateApiService.edit(id, data);
    }

    public delete (id: number): Observable<any> {
        return this.emailTemplateApiService.delete(id);
    }

}
