import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { IEmailTemplate } from '@Features/email-template';

@Injectable()
export class EmailTemplateApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAll (): Observable<IEmailTemplate[]> {
        return this.httpService.get<IEmailTemplate[]>(`/EmailTemplates`);
    }

    public get (id: number): Observable<IEmailTemplate> {
        return this.httpService.get<IEmailTemplate>(`/EmailTemplates/${id}`);
    }

    public create (data: Partial<IEmailTemplate>): Observable<any> {
        return this.httpService.post(`/EmailTemplates`, data);
    }

    public edit (id: number, data: Partial<IEmailTemplate>): Observable<any> {
        return this.httpService.put(`/EmailTemplates/${id}`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/EmailTemplates/${id}`);
    }

}
