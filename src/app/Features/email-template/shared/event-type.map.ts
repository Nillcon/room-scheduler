import { EventTypeEnum } from './event-type.enum';
import { IEventType } from '../interfaces/event-type.interface';

export const EventTypeMap: Map<EventTypeEnum, IEventType> = new Map<EventTypeEnum, IEventType>()
    .set(
        EventTypeEnum.Create,
        {
            id: EventTypeEnum.Create,
            name: 'Create request'
        }
    )
    .set(
        EventTypeEnum.Approve,
        {
            id: EventTypeEnum.Approve,
            name: 'Global approve'
        }
    )
    .set(
        EventTypeEnum.Modify,
        {
            id: EventTypeEnum.Modify,
            name: 'Modify request'
        }
    )
    .set(
        EventTypeEnum.Cancel,
        {
            id: EventTypeEnum.Cancel,
            name: 'Cancel request'
        }
    )
    .set(
        EventTypeEnum.RoomApprove,
        {
            id: EventTypeEnum.RoomApprove,
            name: 'Room approve'
        }
    )
    .set(
        EventTypeEnum.AllAgree,
        {
            id: EventTypeEnum.AllAgree,
            name: 'All agree'
        }
    );
