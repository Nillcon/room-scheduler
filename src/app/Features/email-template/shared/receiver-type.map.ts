import { ReceiverTypeEnum } from './receiver-type.enum';
import { IReceiverType } from '../interfaces/receiver-type.interface';


export const ReceiverTypeMap: Map<ReceiverTypeEnum, IReceiverType> = new Map<ReceiverTypeEnum, IReceiverType>()
    .set(
        ReceiverTypeEnum.SuperUser,
        {
            id: ReceiverTypeEnum.SuperUser,
            name: 'Super user'
        }
    )
    .set(
        ReceiverTypeEnum.Author,
        {
            id: ReceiverTypeEnum.Author,
            name: 'Author'
        }
    )
    .set(
        ReceiverTypeEnum.GlobalApprover,
        {
            id: ReceiverTypeEnum.GlobalApprover,
            name: 'Global approver'
        }
    )
    .set(
        ReceiverTypeEnum.RoomApprover,
        {
            id: ReceiverTypeEnum.RoomApprover,
            name: 'Room approver'
        }
    );
