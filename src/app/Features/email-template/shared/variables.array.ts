export const VariablesArray: string[] = [
    'DateFrom',
    'DateTo',
    'Capacity',
    'Name' ,
    'EventDescription' ,
    'LayoutDescription',
    'RoomLayoutType',
    'Status',
    'RecurrenceRule',
    'AuthorName',
    'IsAllDayEvent',
    'Location',
    'OrderedServices',
    'OrderedEquipment'
];
