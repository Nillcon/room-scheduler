export enum EventTypeEnum {
    Create = 0,
    Approve,
    Modify,
    Cancel,
    RoomApprove,
    AllAgree
}
