export enum ReceiverTypeEnum {
    SuperUser = 0,
    Author,
    GlobalApprover,
    RoomApprover
}
