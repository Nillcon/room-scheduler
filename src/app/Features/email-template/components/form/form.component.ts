import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IEmailTemplate } from '@Features/email-template/interfaces/email-template.interface';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { ReceiverTypeEnum } from '@Features/email-template/shared/receiver-type.enum';

import { ReceiverTypeMap } from '@Features/email-template/shared/receiver-type.map';
import { IReceiverType } from '@Features/email-template/interfaces/receiver-type.interface';
import { EventTypeMap } from '@Features/email-template/shared/event-type.map';
import { IEventType } from '@Features/email-template/interfaces/event-type.interface';
import { EventTypeEnum } from '@Features/email-template/shared/event-type.enum';
import { Validators } from '@angular/forms';

@BaseForm()
@Component({
  selector: 'email-template-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, IBaseForm<Partial<IEmailTemplate>> {

    @Input()
    public inputData: IEmailTemplate;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IEmailTemplate>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IEmailTemplate>>;

    public receiverTypes: IReceiverType[] = [...ReceiverTypeMap.values()];
    public eventTypes: IEventType[] = [...EventTypeMap.values()];

    constructor (
        private fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {}

    public formGroupInit (inputData?: IEmailTemplate): void {
        if (inputData) {
            this._formGroupValueInit(inputData);
        } else {
            this._formGroupEmptyInit();
        }
    }

    private _formGroupValueInit (inputData: IEmailTemplate): void {
        this.formGroup = this.fb.group<Partial<IEmailTemplate>>({
            name: this.fb.control(inputData.name, [
                Validators.required
            ]),
            subject: this.fb.control(inputData.subject, [
                Validators.required
            ]),
            text: this.fb.control(inputData.text, []),
            eventType: this.fb.control(inputData.eventType, []),
            receiverType: this.fb.control(inputData.receiverType, [])
        });
    }

    private _formGroupEmptyInit (): void {
        this.formGroup = this.fb.group<Partial<IEmailTemplate>>({
            name: this.fb.control('', [
                Validators.required
            ]),
            subject: this.fb.control('', [
                Validators.required
            ]),
            text: this.fb.control('', []),
            eventType: this.fb.control(EventTypeEnum.Create, []),
            receiverType: this.fb.control(ReceiverTypeEnum.SuperUser, [])
        });
    }

}
