import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IEmailTemplate } from '@Features/email-template';
import { Validators } from '@angular/forms';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';

@BaseForm()
@Component({
  selector: 'email-template-creation-form',
  templateUrl: './creation-form.component.html',
  styleUrls: ['./creation-form.component.scss']
})
export class CreationFormComponent implements OnInit, IBaseForm<Partial<IEmailTemplate>> {

    @Input()
    public inputData: Partial<IEmailTemplate>;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IEmailTemplate>>>;

    public formGroup: FormGroupTypeSafe<Partial<IEmailTemplate>>;

    constructor (
        private fb: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.fb.group<Partial<IEmailTemplate>>({
            name: this.fb.control('', [
                Validators.required
            ]),
            subject: this.fb.control('', [
                Validators.required
            ]),
            text: this.fb.control('', [
                Validators.required
            ]),
        });
    }

}
