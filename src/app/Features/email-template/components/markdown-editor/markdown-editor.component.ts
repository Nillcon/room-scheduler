import { Component, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { VariablesArray } from './../../shared/variables.array';

@Component({
    selector: 'email-template-markdown-editor',
    templateUrl: './markdown-editor.component.html',
    styleUrls: ['./markdown-editor.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: MarkdownEditorComponent,
            multi: true
        }
    ]
})
export class MarkdownEditorComponent implements OnInit, ControlValueAccessor {

    public value: string;

    public VariablesArray = VariablesArray;

    constructor () { }

    public ngOnInit (): void {
    }

    public registerOnChange (fn: any): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (value: string): void {
        this.value = value;
        this.onChange(value);
    }

    public htmlEditorOnChange (event: any): void {
        this.writeValue(event.value);
    }

    private onChange = (value: string) => {};
}
