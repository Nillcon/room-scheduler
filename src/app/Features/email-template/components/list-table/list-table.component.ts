import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IEmailTemplate } from '@Features/email-template/interfaces/email-template.interface';
import { IReceiverType } from '@Features/email-template/interfaces/receiver-type.interface';
import { ReceiverTypeMap } from '@Features/email-template/shared/receiver-type.map';

import { EventTypeMap } from '@Features/email-template/shared/event-type.map';
import { IEventType } from '@Features/email-template/interfaces/event-type.interface';

@Component({
    selector: 'email-templates-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()
    public emailTemplates: Partial<IEmailTemplate>[];

    @Output()
    public OnEditButtonClick: EventEmitter<IEmailTemplate> = new EventEmitter();
    @Output()
    public OnDeleteButtonClick: EventEmitter<IEmailTemplate> = new EventEmitter();

    public receiverTypes: IReceiverType[] = [...ReceiverTypeMap.values()];
    public eventTypes: IEventType[] = [...EventTypeMap.values()];

    constructor (
    ) {}

    public ngOnInit (): void {}
}
