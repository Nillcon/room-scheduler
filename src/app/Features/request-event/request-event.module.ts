import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestEventApiService } from './request-event.api.service';
import { RequestEventService } from './request-event.service';
import { ListTableComponent } from './components/list-table/list-table.component';
import { GeneralInformationFormComponent } from './components/general-information-form/general-information-form.component';
import { SharedModule } from '@App/app-shared.module';
import { InvoiceServicesComponent } from './components/invoice-services/invoice-services.component';
import { InvoicesPriceTableComponent } from './components/invoices-price-table/invoices-price-table.component';
import { ApprovingFormComponent } from './components/approving-form/approving-form.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';
import { MasterDetailsComponent } from './components/list-table/master-details/master-details.component';

@NgModule({
    declarations: [
        ListTableComponent,
        GeneralInformationFormComponent,
        InvoiceServicesComponent,
        InvoicesPriceTableComponent,
        ApprovingFormComponent,
        MasterDetailsComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    exports: [
        ListTableComponent,
        GeneralInformationFormComponent,
        InvoiceServicesComponent,
        InvoicesPriceTableComponent,
        ApprovingFormComponent
    ],
    providers: [
        RequestEventApiService,
        RequestEventService
    ]
})
export class RequestEventModule { }
