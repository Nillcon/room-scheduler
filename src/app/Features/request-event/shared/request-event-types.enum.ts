export enum RequestEventTypes {
    PendingApproval,
    Approved,
    Modified,
    Cancelled,
    Draft,
    Agreed
}
