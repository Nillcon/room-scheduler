import { RequestEventTypes } from './request-event-types.enum';
import { IRequestEventType } from '../interfaces/request-event-type.interface';

export const RequestEventTypesMap: Map<RequestEventTypes, IRequestEventType> = new Map<RequestEventTypes, IRequestEventType>()
    .set(
        RequestEventTypes.PendingApproval,
        {
            id: RequestEventTypes.PendingApproval,
            name: 'Pending approval',
            iconClass: 'far fa-clock text-secondary'
        }
    )
    .set(
        RequestEventTypes.Approved,
        {
            id: RequestEventTypes.Approved,
            name: 'Approved',
            iconClass: 'far fa-check-circle text-success'
        }
    )
    .set(
        RequestEventTypes.Modified,
        {
            id: RequestEventTypes.Modified,
            name: 'Modified',
            iconClass: 'fas fa-user-edit text-warning'
        }
    )
    .set(
        RequestEventTypes.Cancelled,
        {
            id: RequestEventTypes.Cancelled,
            name: 'Cancelled',
            iconClass: 'fas fa-ban text-danger'
        }
    )
    .set(
        RequestEventTypes.Draft,
        {
            id: RequestEventTypes.Draft,
            name: 'Draft',
            iconClass: 'fas fa-book-open text-secondary'
        }
    )
    .set(
        RequestEventTypes.Agreed,
        {
            id: RequestEventTypes.Agreed,
            name: 'Agreed',
            iconClass: 'fas fa-handshake text-success'
        }
    );
