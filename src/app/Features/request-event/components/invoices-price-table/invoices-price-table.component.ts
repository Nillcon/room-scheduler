import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { IsNumber, IsArray } from 'class-validator';
import { ISvc } from '@Features/svc/interfaces/svc.interface';
import { IInvoiceService } from '@Features/request-event/interfaces/invoice-service.interface';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { DxDataGridComponent } from 'devextreme-angular';

@ClassValidation()
@Component({
    selector: 'request-events-invoices-price-table',
    templateUrl: './invoices-price-table.component.html',
    styleUrls: ['./invoices-price-table.component.scss']
})
export class InvoicesPriceTableComponent implements OnInit, AfterViewInit {

    @Input()
    @IsNumber()
    public requestEventId: number;

    @Input()
    @IsArray()
    public invoiceServiceArray: IInvoiceService[];

    @Input()
    public set svcArray (svcArray: ISvc[]) {
        const newDict: { [key: number]: ISvc } = {};

        svcArray.forEach((currSvc) => {
            newDict[currSvc.id] = currSvc;
        });

        this._svcArray     = svcArray;
        this.svcDictionary = newDict;
    }

    public svcDictionary: { [key: number]: ISvc } = {};

    @Output()
    public OnSelection: EventEmitter<IInvoiceService[]> = new EventEmitter();

    @Output()
    public OnInvoiceServiceUpdated: EventEmitter<IInvoiceService> = new EventEmitter();

    @IsArray()
    private _svcArray: ISvc[];

    @ViewChild(DxDataGridComponent, { static: false })
    private tableComponent: DxDataGridComponent;

    public ngOnInit (): void {
        // this.OnSelection.emit([]);
    }

    public ngAfterViewInit (): void {
        this.prepareInvoiceServices();
        this.selectSelectedByApproverInvoiceServices();
    }

    public onSelection (e: any): void {
        const selectedInvoiceServicesIds: number[] = e.selectedRowKeys;
        const selectedInvoiceServices: IInvoiceService[] = [];

        this.invoiceServiceArray.forEach((invoiceService) => {
            const selectedInvoiceServiceIndex: number = selectedInvoiceServicesIds
                .findIndex((currSelectedInvoiceServiceId) => {
                    return (invoiceService.serviceId === currSelectedInvoiceServiceId);
                });

            invoiceService.checkedByApprover = (selectedInvoiceServiceIndex !== -1);
            selectedInvoiceServices.push(invoiceService);
        });

        this.OnSelection.emit(selectedInvoiceServices);
    }

    private prepareInvoiceServices (): void {
        this._svcArray.forEach((currSvc, index) => {
            const invoiceServiceIndex: number = this.invoiceServiceArray
                .findIndex((currInvoiceService) => {
                    return currInvoiceService.serviceId === currSvc.id;
                });

            if (invoiceServiceIndex === -1) {
                const invoiceService = this.getEmptyInvoiceService(currSvc.id);
                invoiceService.id = index * -1;
                this.invoiceServiceArray.push(invoiceService);
            }
        });
    }

    private selectSelectedByApproverInvoiceServices (): void {
        const selectedInvoiceServicesIds: number[] = [];

        this.invoiceServiceArray.forEach((invoiceService) => {
            if (invoiceService.checkedByApprover) {
                selectedInvoiceServicesIds.push(invoiceService.serviceId);
            }
        });

        this.tableComponent.instance.selectRows(selectedInvoiceServicesIds, true);
    }

    private getEmptyInvoiceService (serviceId: number): IInvoiceService {
        return {
            id: 0,
            serviceId: serviceId,
            requestEventId: this.requestEventId,
            checkedByApprover: false,
            checkedByClient: false,
            price: 0,
            count: 1,
            estimatedCharges: ''
        };
    }

}
