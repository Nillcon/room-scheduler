import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { IRequestEventApprovingForm } from '@Features/request-event/interfaces/approving-form.interface';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { IRequestEventType } from '@Features/request-event/interfaces/request-event-type.interface';
import { RequestEventTypesMap } from '@Features/request-event/shared/request-event-types.map';
import { RequestEventTypes } from '@Features/request-event/shared/request-event-types.enum';

@BaseForm()
@Component({
    selector: 'request-event-approving-form',
    templateUrl: './approving-form.component.html',
    styleUrls: ['./approving-form.component.scss']
})
export class ApprovingFormComponent implements OnInit, IBaseForm<IRequestEventApprovingForm> {

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<IRequestEventApprovingForm>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<IRequestEventApprovingForm>;

    public typeArray: IRequestEventType[];

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {
        this.initTypeArray();
    }

    public formGroupInit (): void {
        this.formGroup = this.formBuilder.group<IRequestEventApprovingForm>({
            status: this.formBuilder.control(
                '',
                [Validators.required]
            )
        });
    }

    private initTypeArray (): void {
        this.typeArray = [
            RequestEventTypesMap.get(RequestEventTypes.Approved),
            RequestEventTypesMap.get(RequestEventTypes.Cancelled)
        ];
    }

}
