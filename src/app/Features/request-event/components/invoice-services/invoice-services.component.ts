import { Component, OnInit, Input } from '@angular/core';
import { IInvoiceService } from '@Features/request-event/interfaces/invoice-service.interface';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { ISvc } from '@Features/svc';

@ClassValidation()
@Component({
    selector: 'request-event-invoice-services',
    templateUrl: './invoice-services.component.html',
    styleUrls: ['./invoice-services.component.scss']
})
export class InvoiceServicesComponent implements OnInit {

    @Input()
    @IsArray()
    public invoiceServiceArray: Partial<IInvoiceService>[];

    @Input()
    public set svcArray (svcArray: ISvc[]) {
        const newDict: { [key: number]: ISvc } = {};

        svcArray.forEach((currSvc) => {
            newDict[currSvc.id] = currSvc;
        });

        this._svcArray     = svcArray;
        this.svcDictionary = newDict;
    }

    public svcDictionary: { [key: number]: ISvc } = {};

    @IsArray()
    private _svcArray: ISvc[];

    constructor () {}

    public ngOnInit (): void {
        this.prepareInvoiceServices();
    }

    private prepareInvoiceServices (): void {
        this._svcArray.forEach((currSvc) => {
            const invoiceServiceIndex: number = this.invoiceServiceArray.findIndex((currInvoiceService) => {
                return currInvoiceService.serviceId === currSvc.id;
            });

            if (invoiceServiceIndex === -1) {
                this.invoiceServiceArray.push({
                    checkedByApprover: false,
                    checkedByClient: false,
                    price: 0,
                    serviceId: currSvc.id
                });
            }
        });
    }

}
