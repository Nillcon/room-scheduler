import { IDxHeaderFilterItem } from '@Interfaces/devextreme-header-filter-item.interface';
import { RequestEventTypesMap } from '@Features/request-event/shared/request-event-types.map';

export const StatusHeaderFilterItems: IDxHeaderFilterItem[] = [...RequestEventTypesMap.values()]
    .map((type) => {
        return <IDxHeaderFilterItem>{
            text: type.name,
            value: type.id
        };
    });
