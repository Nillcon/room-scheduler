import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRequestEventDetailsRoom } from '@Features/request-event/interfaces/request-event-details-room.interface';
import { RequestEventTypesMap } from '@Features/request-event/shared/request-event-types.map';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RequestEventTypes } from '@Features/request-event/shared/request-event-types.enum';

@Component({
    selector: 'master-details',
    templateUrl: './master-details.component.html',
    styleUrls: ['./master-details.component.scss']
})
export class MasterDetailsComponent implements OnInit {

    @Input()
    public requestEventId: number;

    @Input()
    public loadRoomMasterDetails: (requestEventId: number) => Observable<IRequestEventDetailsRoom[]>;

    @Output()
    public OnApprove: EventEmitter<IRequestEventDetailsRoom> = new EventEmitter();

    public detailsInfo: IRequestEventDetailsRoom[];

    public RequestEventTypesArray = [...RequestEventTypesMap.values()];

    public statusEnum: typeof RequestEventTypes = RequestEventTypes;

    constructor () { }

    public ngOnInit (): void {
        this.updateDetailsInfo()
            .subscribe();
    }

    public updateDetailsInfo (): Observable<IRequestEventDetailsRoom[]> {
        return this.loadRoomMasterDetails(this.requestEventId)
            .pipe(
                tap(res => this.detailsInfo = res)
            );
    }

    public onApproveClick (room: IRequestEventDetailsRoom): void {
        this.OnApprove.emit(room);
    }

}
