export enum ColumnEnum {
    Name,
    Capacity,
    Rooms,
    StartDate,
    EndDate,
    Equipment,
    AllDay,
    Status,
    Approve,
    Price,
    Clone,
    Edit,
    Delete
}
