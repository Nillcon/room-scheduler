import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { IRequestEvent } from '@Features/request-event/interfaces/request-event.interface';
import { RequestEventTypesMap } from '@Features/request-event/shared/request-event-types.map';
import { EquipmentTypeMap } from '@Features/equipment/shared/equipment-type.map';
import { ColumnEnum } from './shared/list-table-columns.enum';
import { RequestEventTypes } from '@Features/request-event/shared/request-event-types.enum';
import { StatusHeaderFilterItems } from './data/status-header-filters.data';
import { IDxHeaderFilterItem } from '@Interfaces/devextreme-header-filter-item.interface';
import { generateSummary, RecurrenceEditor } from '@syncfusion/ej2-schedule';
import { L10n } from '@syncfusion/ej2-base';
import { IEquipment } from '@Features/equipment';
import { KeyValue } from '@angular/common';
import { IRequestEventEquipmentDetails } from '@Layouts/main-layout/pages/events-page/interfaces/request-event-equpments-details.interface';
import { Observable } from 'rxjs';
import { IRequestEventDetailsRoom } from '@Features/request-event/interfaces/request-event-details-room.interface';


@Component({
    selector: 'request-events-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()
    public set requestEvents (items: IRequestEvent[]) {
        items.map(item => {
            item.isApproved = item.status === RequestEventTypes.Approved;

            if (item.rrule) {
                item.rrule = this.getRruleSummary(item.rrule);
            }

            return item;
        });

        this._requestEvents = items;
    }

    public get requestEvents (): IRequestEvent[] {
        return this._requestEvents;
    }

    @Input()
    public set forbiddenColumns (columns: ColumnEnum[]) {
        const newDictionary: { [key: number]: boolean } = {};

        columns.forEach((currColumn) => {
            newDictionary[currColumn] = true;
        });

        this.forbiddenColumnsDictionary = newDictionary;
    }

    @Input()
    public itemIdToSelect: number;

    @Input()
    public loadRoomDetailsFunction: (requestEventId: number) => Observable<IRequestEventDetailsRoom[]>;

    @Output()
    public OnApproveButtonClick: EventEmitter<IRequestEvent> = new EventEmitter();

    @Output()
    public OnEquipmentButtonClick: EventEmitter<IRequestEventEquipmentDetails> = new EventEmitter();

    @Output()
    public OnChangePasswordButtonClick: EventEmitter<IRequestEvent> = new EventEmitter();

    @Output()
    public OnChangePriceButtonClick: EventEmitter<IRequestEvent> = new EventEmitter();

    @Output()
    public OnCloneButtonClick: EventEmitter<IRequestEvent> = new EventEmitter();

    @Output()
    public OnEditButtonClick: EventEmitter<IRequestEvent> = new EventEmitter();

    @Output()
    public OnRoomApproveClick: EventEmitter<{requestEvent: IRequestEvent, room: IRequestEventDetailsRoom}> = new EventEmitter();

    @Output()
    public OnDeleteButtonClick: EventEmitter<IRequestEvent> = new EventEmitter();

    public RequestEventTypesArray   = [...RequestEventTypesMap.values()];
    public EquipmentTypeArray       = [...EquipmentTypeMap.values()];

    public statusHeaderFilterItems: IDxHeaderFilterItem[] = StatusHeaderFilterItems;

    public statusEnum: typeof RequestEventTypes = RequestEventTypes;

    public columnsEnum: typeof ColumnEnum = ColumnEnum;

    public forbiddenColumnsDictionary: { [key: number]: boolean } = {};

    public tableHeight: string;

    public detailsInfo: IRequestEventDetailsRoom[];

    private _requestEvents: IRequestEvent[];

    private recurenceEditor: RecurrenceEditor;

    constructor () {
        this.recurenceEditor = new RecurrenceEditor();
    }

    public ngOnInit (): void {
        this.updateTableHeight();
    }

    @HostListener('window:resize')
    public onWindowResize (): void {
        this.updateTableHeight();
    }

    public onEquipmentClick (requestEvent: IRequestEvent, equipmentGroup: KeyValue<string, IEquipment[]>): void {
        const _equipmentGroup: KeyValue<number, IEquipment[]> = {
            key: +equipmentGroup.key,
            value: equipmentGroup.value
        };

        this.OnEquipmentButtonClick.emit({
            equipmentGroup: _equipmentGroup,
            requestEvent: requestEvent
        });
    }

    public onChangePrice (requestEvent: IRequestEvent): void {
        this.OnChangePriceButtonClick.emit(requestEvent);
    }

    public onClone (requestEvent: IRequestEvent): void {
        this.OnCloneButtonClick.emit(requestEvent);
    }

    public onEdit (requestEvent: IRequestEvent): void {
        this.OnEditButtonClick.emit(requestEvent);
    }

    public onDelete (requestEvent: IRequestEvent): void {
        this.OnDeleteButtonClick.emit(requestEvent);
    }

    public onRoomApprove (requestEvent: IRequestEvent, room: IRequestEventDetailsRoom): void {
        this.OnRoomApproveClick.emit({
            requestEvent: requestEvent,
            room: room
        });
    }

    public onMasterDetailsClosing (): void {
    }

    public updateTableHeight (): void {
        this.tableHeight = `${window.innerHeight / 1.25}px`;
    }

    private getRruleSummary (rrule: string): string {
        const defaultLocale: object = this.recurenceEditor['defaultLocale'];

        return generateSummary(
            rrule,
            new L10n('recurrenceeditor', defaultLocale, this.recurenceEditor.locale),
            this.recurenceEditor.locale
        );
    }

}
