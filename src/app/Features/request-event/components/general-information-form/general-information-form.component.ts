import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRequestEventGeneralInfoForm } from '@Features/request-event/interfaces/general-info-form.interface';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';

@BaseForm()
@Component({
    selector: 'request-event-general-information-form',
    templateUrl: './general-information-form.component.html',
    styleUrls: ['./general-information-form.component.scss']
})
export class GeneralInformationFormComponent implements OnInit, IBaseForm<Partial<IRequestEventGeneralInfoForm>> {

    @Input()  public inputData: Partial<IRequestEventGeneralInfoForm>;
    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IRequestEventGeneralInfoForm>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (generalInfoData: Partial<IRequestEventGeneralInfoForm>): void {
        this.formGroup = this.formBuilder.group<Partial<IRequestEventGeneralInfoForm>>({
            name: this.formBuilder.control(
                generalInfoData ? generalInfoData.name : '',
                [Validators.required]
            ),
            note: this.formBuilder.control(
                generalInfoData ? generalInfoData.note : ''
            )
        });
    }

}
