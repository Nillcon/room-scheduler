import { RequestEventTypes } from '../shared/request-event-types.enum';

export interface IRequestEventApprovingForm {
    status: RequestEventTypes;
}
