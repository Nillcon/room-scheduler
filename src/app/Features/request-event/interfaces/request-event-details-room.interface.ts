import { RequestEventTypes } from '../shared/request-event-types.enum';

export interface IRequestEventDetailsRoom {
    roomName: string;
    roomId: string;
    status: RequestEventTypes;
    canBeApproved: boolean;
}
