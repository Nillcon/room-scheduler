import { RequestEventTypes } from '../shared/request-event-types.enum';
import { RequiredRoomTypeEnum } from '@Features/room/shared/required-room-type.enum';
import { IEquipment } from '@Features/equipment';
import { ISvc } from '@Features/svc';

export interface IRequestEvent {
    id: number;
    author: string;
    canBeApproved: boolean;
    dateFrom: string;
    dateTo: string;
    isAllDayEvent: boolean;
    capacity: number;
    name: string;
    eventDescription: string;
    layoutDescription: string;
    requiredRoomLayoutType: RequiredRoomTypeEnum;
    status: RequestEventTypes;
    rooms: string[];
    rrule: string;
    eventId: string;
    equipment: IEquipment[];
    services: ISvc[];
    isApproved?: boolean;
}
