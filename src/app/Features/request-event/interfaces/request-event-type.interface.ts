import { RequestEventTypes } from '../shared/request-event-types.enum';

export interface IRequestEventType {
    id: RequestEventTypes;
    name: string;
    iconClass: string;
}
