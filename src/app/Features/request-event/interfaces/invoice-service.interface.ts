export interface IInvoiceService {
    id: number;
    price: number;
    requestEventId: number;
    serviceId: number;
    count: number;
    estimatedCharges: string;
    checkedByApprover: boolean;
    checkedByClient: boolean;
}
