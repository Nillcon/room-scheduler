import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IRequestEvent } from '@Features/request-event';
import { IRoomBookingForm } from '@Features/room/interfaces/booking-form.interface';
import { map } from 'rxjs/operators';
import { IInvoiceService } from '@Features/request-event/interfaces/invoice-service.interface';
import { RequestEventTypes } from './shared/request-event-types.enum';
import { DateService } from '@Core/root/date/date.service';
import { RequestEventApiService } from './request-event.api.service';
import { IRequestEventDetailsRoom } from './interfaces/request-event-details-room.interface';

@Injectable()
export class RequestEventService {

    constructor (
        private requestEventApiService: RequestEventApiService,
        private dateService: DateService
    ) {}

    public getAll (): Observable<IRequestEvent[]> {
        return this.requestEventApiService.getAll();
    }

    public get (id: number): Observable<IRequestEvent> {
        return this.requestEventApiService.get(id);
    }

    public create (data: Partial<IRequestEvent>): Observable<number> {
        return this.requestEventApiService.create(data);
    }

    public edit (id: number, data: Partial<IRequestEvent>): Observable<any> {
        return this.requestEventApiService.edit(id, data);
    }

    public delete (id: number): Observable<any> {
        return this.requestEventApiService.delete(id);
    }

    public changeStatus (id: number, status: RequestEventTypes): Observable<any> {
        return this.requestEventApiService.changeStatus(id, status);
    }

    public getFilters (id: number): Observable<Partial<IRoomBookingForm>> {
        return this.requestEventApiService.getFilters(id)
            .pipe(
                map((data) => {
                    data.dateFrom = new Date(data.dateFrom);
                    data.dateTo   = new Date(data.dateTo);
                    return data;
                })
            );
    }

    public assignFilters (id: number, data: Partial<IRoomBookingForm>): Observable<any> {
        const newData: object = data;
        newData['dateFrom']   = this.dateService.getLocalJsonTimeString(data.dateFrom);
        newData['dateTo']     = this.dateService.getLocalJsonTimeString(data.dateTo);

        return this.requestEventApiService.assignFilters(id, newData);
    }

    public getRooms (id: number): Observable<string[]> {
        return this.requestEventApiService.getRooms(id);
    }

    public assignRooms (id: number, data: string[]): Observable<any> {
        return this.requestEventApiService.assignRooms(id, data);
    }

    public getServices (id: number): Observable<number[]> {
        return this.requestEventApiService.getServices(id);
    }

    public assignServices (id: number, data: number[]): Observable<any> {
        return this.requestEventApiService.assignServices(id, data);
    }

    public getInvoiceServices (requestEventId: number): Observable<IInvoiceService[]> {
        return this.requestEventApiService.getInvoiceServices(requestEventId);
    }

    public saveInvoiceServices (requestEventId: number, data: Partial<IInvoiceService>[]): Observable<any> {
        return this.requestEventApiService.saveInvoiceServices(requestEventId, data);
    }

    public getDetails (requestEventId: number): Observable<IRequestEventDetailsRoom[]> {
        return this.requestEventApiService.getDetails(requestEventId);
    }

    public changeRoomStatus (roomId: string, requestEventId: number, status: RequestEventTypes): Observable<any> {
        return this.requestEventApiService.changeRoomStatus(roomId, requestEventId, status);
    }

}
