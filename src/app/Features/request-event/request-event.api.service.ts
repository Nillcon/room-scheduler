import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { IRequestEvent } from '@Features/request-event';
import { IRoomBookingForm } from '@Features/room/interfaces/booking-form.interface';
import { IInvoiceService } from '@Features/request-event/interfaces/invoice-service.interface';
import { RequestEventTypes } from './shared/request-event-types.enum';
import { IRequestEventDetailsRoom } from './interfaces/request-event-details-room.interface';

@Injectable()
export class RequestEventApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAll (): Observable<IRequestEvent[]> {
        return this.httpService.get<IRequestEvent[]>(`/RequestEvents`);
    }

    public get (id: number): Observable<IRequestEvent> {
        return this.httpService.get<IRequestEvent>(`/RequestEvents/${id}`);
    }

    public create (data: Partial<IRequestEvent>): Observable<number> {
        return this.httpService.post(`/RequestEvents`, data);
    }

    public edit (id: number, data: Partial<IRequestEvent>): Observable<any> {
        return this.httpService.put(`/RequestEvents/${id}`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/RequestEvents/${id}`);
    }

    public changeStatus (id: number, status: RequestEventTypes): Observable<any> {
        return this.httpService.put(`/RequestEvents/${id}/ChangeStatus/${status}`, {});
    }

    public getFilters (id: number): Observable<Partial<IRoomBookingForm>> {
        return this.httpService.get<Partial<IRoomBookingForm>>(`/RequestEvents/${id}/Filters`);
    }

    public assignFilters (id: number, data: Partial<IRoomBookingForm>): Observable<any> {
        return this.httpService.put(`/RequestEvents/${id}/Filters`, data);
    }

    public getRooms (id: number): Observable<string[]> {
        return this.httpService.get<string[]>(`/RequestEvents/${id}/Rooms`);
    }

    public assignRooms (id: number, data: string[]): Observable<any> {
        return this.httpService.put(`/RequestEvents/${id}/Rooms`, { value: data });
    }

    public getServices (id: number): Observable<number[]> {
        return this.httpService.get<number[]>(`/RequestEvents/${id}/Services`);
    }

    public assignServices (id: number, data: number[]): Observable<any> {
        return this.httpService.put(`/RequestEvents/${id}/Services`, { value: data });
    }

    public getInvoiceServices (requestEventId: number): Observable<IInvoiceService[]> {
        return this.httpService.get(`/RequestEvents/${requestEventId}/InvoiceServices`);
    }

    public saveInvoiceServices (requestEventId: number, data: Partial<IInvoiceService>[]): Observable<any> {
        return this.httpService.put(`/RequestEvents/${requestEventId}/InvoiceServices`, { serviceItems: data });
    }

    public getDetails (requestEventId: number): Observable<IRequestEventDetailsRoom[]> {
        return this.httpService.get(`/RequestEvents/${requestEventId}/Details`);
    }

    public changeRoomStatus (roomId: string, requestEventId: number, status: RequestEventTypes): Observable<any> {
        return this.httpService.put(`/RequestEvents/${requestEventId}/${roomId}/ChangeStatus/${status}`);
    }

}
