import { RequiredRoomTypeEnum } from './required-room-type.enum';
import { IRequiredRoomType } from '../interfaces/required-room-type.interface';

export const RequiredRoomTypeMap: Map<RequiredRoomTypeEnum, IRequiredRoomType> = new Map<RequiredRoomTypeEnum, IRequiredRoomType>()
    .set(
        RequiredRoomTypeEnum.Any,
        {
            id: RequiredRoomTypeEnum.Any,
            name: 'Any'
        }
    )
    .set(
        RequiredRoomTypeEnum.Special,
        {
            id: RequiredRoomTypeEnum.Special,
            name: 'Special'
        }
    )
    .set(
        RequiredRoomTypeEnum.Specific,
        {
            id: RequiredRoomTypeEnum.Specific,
            name: 'Specific'
        }
    );
