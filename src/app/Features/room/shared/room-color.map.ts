import { RoomColorEnum } from './room-color.enum';
import { IRoomColor } from '../interfaces/room-color.interface';

export const RoomColorMap: Map<RoomColorEnum, IRoomColor> = new Map<RoomColorEnum, IRoomColor>()
    .set(
        RoomColorEnum.Red,
        {
            id: RoomColorEnum.Red,
            name: 'Red',
            hex: '#E7A1A2',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Orange,
        {
            id: RoomColorEnum.Orange,
            name: 'Orange',
            hex: '#F9BA89',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Peach,
        {
            id: RoomColorEnum.Peach,
            name: 'Peach',
            hex: '#F7DD8F',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Yellow,
        {
            id: RoomColorEnum.Yellow,
            name: 'Yellow',
            hex: '#FCFA90',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Green,
        {
            id: RoomColorEnum.Green,
            name: 'Green',
            hex: '#78D168',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Teal,
        {
            id: RoomColorEnum.Teal,
            name: 'Teal',
            hex: '#9FDCC9',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Olive,
        {
            id: RoomColorEnum.Olive,
            name: 'Olive',
            hex: '#C6D2B0',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Blue,
        {
            id: RoomColorEnum.Blue,
            name: 'Blue',
            hex: '#9DB7E8',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Purple,
        {
            id: RoomColorEnum.Purple,
            name: 'Purple',
            hex: '#B5A1E2',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.Maroon,
        {
            id: RoomColorEnum.Maroon,
            name: 'Maroon',
            hex: '#daaec2',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.Steel,
        {
            id: RoomColorEnum.Steel,
            name: 'Steel',
            hex: '#dad9dc',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.DarkSteel,
        {
            id: RoomColorEnum.DarkSteel,
            name: 'Dark Steel',
            hex: '#6b7994',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.Grey,
        {
            id: RoomColorEnum.Grey,
            name: 'Grey',
            hex: '#bfbfbf',
            tint: 'light'
        }
    )
    .set(
        RoomColorEnum.DarkGrey,
        {
            id: RoomColorEnum.DarkGrey,
            name: 'Dark Grey',
            hex: '#6f6f6f',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.Black,
        {
            id: RoomColorEnum.Black,
            name: 'Black',
            hex: '#4f4f4f',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkRed,
        {
            id: RoomColorEnum.DarkRed,
            name: 'Dark Red',
            hex: '#c11a25',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkOrange,
        {
            id: RoomColorEnum.DarkOrange,
            name: 'Dark Orange',
            hex: '#e2620d',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkPeach,
        {
            id: RoomColorEnum.DarkPeach,
            name: 'Dark Peach',
            hex: '#c79930',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkYellow,
        {
            id: RoomColorEnum.DarkYellow,
            name: 'Dark Yellow',
            hex: '#b9b300',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkGreen,
        {
            id: RoomColorEnum.DarkGreen,
            name: 'Dark Green',
            hex: '#368f2b',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkTeal,
        {
            id: RoomColorEnum.DarkTeal,
            name: 'Dark Teal',
            hex: '#329b7a',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkOlive,
        {
            id: RoomColorEnum.DarkOlive,
            name: 'Dark Olive',
            hex: '#C6D2B0',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkBlue,
        {
            id: RoomColorEnum.DarkBlue,
            name: 'Dark Blue',
            hex: '#2858a5',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkPurple,
        {
            id: RoomColorEnum.DarkPurple,
            name: 'Dark Purple',
            hex: '#5c3fa3',
            tint: 'dark'
        }
    )
    .set(
        RoomColorEnum.DarkMaroon,
        {
            id: RoomColorEnum.DarkMaroon,
            name: 'Dark Maroon',
            hex: '#93446b',
            tint: 'dark'
        }
    );
