import { IRoomType } from '../interfaces/room-type.interface';

export enum RoomTypesEnum {
    Theatre = 1,
    ClassRoom,
    Cabaret,
    Boardroom,
    UShaped,
    CircleOfChairs
}


export const RoomTypes: { [K: number]: IRoomType } = {
    [RoomTypesEnum.Theatre]: {
        name: 'Theatre',
        type: RoomTypesEnum.Theatre,
        imageUrl: 'assets/room-types/theatre.png'
    },
    [RoomTypesEnum.ClassRoom]: {
        name: 'Class room',
        type: RoomTypesEnum.ClassRoom,
        imageUrl: 'assets/room-types/u-shaped.png'
    },
    [RoomTypesEnum.Cabaret]: {
        name: 'Cabaret',
        type: RoomTypesEnum.Cabaret,
        imageUrl: 'assets/room-types/cabaret.png'
    },
    [RoomTypesEnum.Boardroom]: {
        name: 'Boardroom',
        type: RoomTypesEnum.Boardroom,
        imageUrl: 'assets/room-types/boardroom.png'
    },
    [RoomTypesEnum.UShaped]: {
        name: 'U-Shaped',
        type: RoomTypesEnum.UShaped,
        imageUrl: 'assets/room-types/u-shaped.png'
    },
    [RoomTypesEnum.CircleOfChairs]: {
        name: 'Circle of chairs',
        type: RoomTypesEnum.CircleOfChairs,
        imageUrl: 'assets/room-types/circle-of-chair.png'
    },
};
