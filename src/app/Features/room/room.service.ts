import { Injectable } from '@angular/core';
import { IRoom } from './interfaces/room.interface';
import { IEvent } from '@Features/event';
import { RoomApiService } from './room.api.service';
import { Observable } from 'rxjs';
import { flatMap, map, toArray } from 'rxjs/operators';
import { RoomColorMap } from './shared/room-color.map';
import { RoomColorEnum } from './shared/room-color.enum';
import { IRoomBookingForm } from './interfaces/booking-form.interface';
import { IRoomLayouts } from './interfaces/room-layouts.interface';
import { IRoomPhoto } from './interfaces/room-photo.interface';
import { IRoomSettingsForm } from './interfaces/room-settings-form.interface';
import { IRoomApprover } from './interfaces/room-approver.interface';
import { IRoomEquipment } from './interfaces/room-equipment.interface';

@Injectable()
export class RoomService {

    constructor (
        private roomApiService: RoomApiService
    ) {}

    public getRoomById (rooms: IRoom[], roomId: string): IRoom {
        return rooms.find(res => res.id === roomId);
    }

    public getEventRoom (event: IEvent, rooms: IRoom[]): IRoom[] {
        let _rooms: IRoom[];

        try {
            _rooms = rooms
                .filter(room => {
                    return !!event.roomsId
                        .filter(roomId => roomId === room.id)
                        .length;
                });
        } catch (error) {
            _rooms = rooms;
        }

        return _rooms;
    }

    public getAll (): Observable<IRoom[]> {
        return this.roomApiService.getAll()
            .pipe(
                flatMap(res => res),
                map(res => {
                    res.color = RoomColorMap.get(res.color.id);
                    res.colorHex = res.color.hex;

                    return res;
                }),
                toArray()
            );
    }

    public getFilteredRooms (bookingData: Partial<IRoomBookingForm>): Observable<IRoom[]> {
        return this.roomApiService.getFilteredRooms(bookingData)
            .pipe(
                flatMap(res => res),
                map(res => {
                    res.color = RoomColorMap.get(res.color.id);

                    return res;
                }),
                toArray(),
            );
    }

    public changeColor (roomId: string, roomColorId: RoomColorEnum): Observable<any> {
        return this.roomApiService.changeColor(roomId, roomColorId);
    }

    public saveLayouts (id: string, roomTypes: number[]): Observable<any> {
        return this.roomApiService.saveLayouts(id, roomTypes);
    }

    public getLayouts (id: string): Observable<IRoomLayouts> {
        return this.roomApiService.getLayouts(id);
    }

    public getPhotos (id: string): Observable<IRoomPhoto[]> {
        return this.roomApiService.getPhotos(id);
    }

    public uploadPhoto (roomId: string, imageBase64: string): Observable<any> {
        return this.roomApiService.uploadPhoto(roomId, imageBase64);
    }

    public getSettings (roomId: string): Observable<IRoomSettingsForm> {
        return this.roomApiService.getSettings(roomId);
    }

    public editSettings (roomId: string, data: Partial<IRoomSettingsForm>): Observable<IRoomSettingsForm> {
        return this.roomApiService.editSettings(roomId, data);
    }

    public deletePhoto (roomId: string, photoId: number): Observable<any> {
        return this.roomApiService.deletePhoto(roomId, photoId);
    }

    public getApprovers (roomId: string): Observable<IRoomApprover> {
        return this.roomApiService.getApprovers(roomId);
    }

    public setApprovers (roomId: string, approversInfo: IRoomApprover): Observable<any> {
        return this.roomApiService.setApprovers(roomId, approversInfo);
    }

    public getEquipment (roomId: string): Observable<IRoomEquipment[]> {
        return this.roomApiService.getEquipment(roomId)
            .pipe(
                map((res: {}) => res['equipment'])
            );
    }

    public setEquipment (roomId: string, equipment: IRoomEquipment[]): Observable<any> {
        return this.roomApiService.setEquipment(roomId, equipment);
    }

    public checkValid (id: string): Observable<boolean> {
        return this.roomApiService.checkValid(id);
    }

    public getGroupNamesWithApprovers (id: string): Observable<string[]> {
        return this.roomApiService.getGroupNamesWithApprovers(id);
    }

}
