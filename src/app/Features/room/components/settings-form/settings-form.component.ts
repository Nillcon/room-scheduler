import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IRoomSettingsForm } from '@Features/room/interfaces/room-settings-form.interface';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';

@BaseForm()
@Component({
    selector: 'room-settings-form',
    templateUrl: './settings-form.component.html',
    styleUrls: ['./settings-form.component.scss']
})
export class SettingsFormComponent implements OnInit, IBaseForm<Partial<IRoomSettingsForm>> {

    @Input()
    public inputData: Partial<IRoomSettingsForm>;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRoomSettingsForm>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IRoomSettingsForm>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (settingsForm?: Partial<IRoomSettingsForm>): void {
        this.formGroup = this.formBuilder.group<Partial<IRoomSettingsForm>>({
            isPrivate: this.formBuilder.control(
                settingsForm ? settingsForm.isPrivate : false
            )
        });
    }

}
