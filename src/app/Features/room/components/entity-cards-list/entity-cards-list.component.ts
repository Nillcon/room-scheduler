import { Component, OnInit, Output, EventEmitter, Input, ViewChildren, QueryList, AfterViewInit, OnDestroy } from '@angular/core';
import { IRoom, IOnChangeRoomColorEvent } from '@Features/room';
import { EntityCardComponent } from '../entity-card/entity-card.component';
import { IsArray, IsBoolean, IsString } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IOnEquipmentTypeGroupChipClick } from '@Features/room/interfaces/on-equipment-type-group-chip-click-event.interface';

@ClassValidation()
@Component({
    selector: 'room-entity-cards-list',
    templateUrl: './entity-cards-list.component.html',
    styleUrls: ['./entity-cards-list.component.scss']
})
export class EntityCardsListComponent implements OnInit, AfterViewInit {

    @Input()
    @IsArray()
    public roomArray: IRoom[];

    @Input()
    @IsArray()
    public selectedRoomIds: string[] = [];

    @Input()
    @IsBoolean()
    public isClickable: boolean = true;

    @Input()
    @IsBoolean()
    public isEditBtnVisible: boolean = false;

    @Input()
    @IsBoolean()
    public isSelectable: boolean = false;

    @Input()
    @IsBoolean()
    public isGallerySliding: boolean = true;

    @Input()
    @IsBoolean()
    public isColorSelectorEnabled: boolean = true;

    @Input()
    @IsString()
    public equipmentChipTooltip: string = '';

    @Input()
    @IsString()
    public noRoomsText: string = 'There are no rooms';

    @Output() public OnChangeColorButtonClick: EventEmitter<IOnChangeRoomColorEvent> = new EventEmitter();
    @Output() public OnViewButtonClick: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnEditButtonClick: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnSelectionChange: EventEmitter<IRoom[]> = new EventEmitter();
    @Output() public OnEquipmentChipClick: EventEmitter<IOnEquipmentTypeGroupChipClick> = new EventEmitter();

    public selectedRoomList: Set<IRoom> = new Set<IRoom>();

    @ViewChildren('RoomCard')
    private roomCardComponentList: QueryList<EntityCardComponent>;

    constructor () {}

    public ngOnInit (): void {}

    public ngAfterViewInit (): void {
        this.initRoomCardsSelection();
    }

    public trackByFunction (index: number, item: IRoom): string {
        return item.id;
    }

    public onRoomSelected (room: IRoom): void {
        this.selectedRoomList.add(room);
        this.emitSelectionData();
    }

    public onRoomDeselected (room: IRoom): void {
        this.selectedRoomList.delete(room);
        this.emitSelectionData();
    }

    private emitSelectionData (): void {
        this.OnSelectionChange.emit([...this.selectedRoomList]);
    }

    private initRoomCardsSelection (): void {
        if (this.selectedRoomIds.length) {
            this.roomCardComponentList.forEach((roomCardComponent) => {
                const isCurrRoomSelected: boolean = this.selectedRoomIds.includes(roomCardComponent.room.id);
                roomCardComponent.isSelected = isCurrRoomSelected;

                if (isCurrRoomSelected) {
                    this.selectedRoomList.add(roomCardComponent.room);
                }
            });

            this.emitSelectionData();
        }
    }

}
