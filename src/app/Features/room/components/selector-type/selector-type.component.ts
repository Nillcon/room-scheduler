import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { RoomTypes, RoomTypesEnum } from '@Features/room/shared/room-types.enum';
import { IRoomType } from '@Features/room/interfaces/room-type.interface';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@ClassValidation()
@Component({
    selector: 'room-selector-type',
    templateUrl: './selector-type.component.html',
    styleUrls: ['./selector-type.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SelectorTypeComponent),
            multi: true
        }
    ]
})
export class SelectorTypeComponent implements OnInit, ControlValueAccessor {

    @Input() public imageMaxWidth: string  = '100%';
    @Input() public imageMaxHeight: string = 'auto';

    public roomTypes: number[];
    public roomTypesArray = Object.values(RoomTypes);

    constructor () {}

    public ngOnInit (): void {
    }

    public registerOnChange (fn: any): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (value: number[]): void {
        this.roomTypes = value;

        this.onChange(value);
    }

    public onLayoutClick (roomType: IRoomType): void {
        if (this.roomTypes.includes(roomType.type)) {
            this.deleteLayoutFromRoom(roomType.type);
        } else {
            this.addLayoutToRoom(roomType.type);
        }
    }

    public addLayoutToRoom (layoutType: RoomTypesEnum): void {
        this.roomTypes.push(layoutType);

        this.writeValue(this.roomTypes);
    }

    public deleteLayoutFromRoom (roomType: RoomTypesEnum): void {
        this.roomTypes = this.roomTypes
            .filter(elem => elem !== roomType);

        this.writeValue(this.roomTypes);
    }

    private onChange = (value: number[]) => {};

}
