import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { IUser } from '@Features/user';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'room-approve-field',
    templateUrl: './approve-field.component.html',
    styleUrls: ['./approve-field.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ApproveFieldComponent),
            multi: true
        }
    ]
})
export class ApproveFieldComponent implements OnInit, ControlValueAccessor {

    @Input()
    public approvers: Partial<IUser>[];

    @Input()
    public autocompleteUpdateFunction: Function;

    @Input()
    public disabled: boolean = false;

    public selectedApprovers: Partial<IUser>[];

    constructor () { }

    public ngOnInit (): void {
    }

    public registerOnChange (fn: any): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (selectedApprovers: Partial<IUser>[]): void {
        this.selectedApprovers = selectedApprovers;

        this.onChange(this.selectedApprovers);
    }

    public addApprover (approver: Partial<IUser>): void {
        this.selectedApprovers.push(approver);

        this.writeValue(this.selectedApprovers);
    }

    public deleteApprover (approver: Partial<IUser>): void {
        this.selectedApprovers = this.selectedApprovers
            .filter(res => approver.login !== res.login);

        this.writeValue(this.selectedApprovers);
    }

    public clearApprovers (): void {
        this.selectedApprovers = [];

        this.writeValue(this.selectedApprovers);
    }

    private onChange = (value: Partial<IUser>[]): void => {};
}
