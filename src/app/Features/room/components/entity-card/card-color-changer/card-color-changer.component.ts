import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RoomColorEnum } from '@Features/room/shared/room-color.enum';
import { IRoomColor } from '@Features/room/interfaces/room-color.interface';

@Component({
  selector: 'room-card-color-changer',
  templateUrl: './card-color-changer.component.html',
  styleUrls: ['./card-color-changer.component.scss']
})
export class CardColorChangerComponent implements OnInit {

    @Input()  public colorId: RoomColorEnum = RoomColorEnum.Blue;
    @Output() public OnChangeColor: EventEmitter<IRoomColor> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
