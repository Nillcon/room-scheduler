import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRoom, IOnChangeRoomColorEvent, IRoomColor } from '@Features/room';
import { IsNotEmptyObject, IsBoolean, IsString } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { MatCheckboxChange } from '@angular/material';
import { IOnEquipmentTypeGroupChipClick } from '../../interfaces/on-equipment-type-group-chip-click-event.interface';

@ClassValidation()
@Component({
    selector: 'room-entity-card',
    templateUrl: './entity-card.component.html',
    styleUrls: ['./entity-card.component.scss']
})
export class EntityCardComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public room: IRoom;

    @Input()
    @IsBoolean()
    public isClickable: boolean = true;

    @Input()
    @IsBoolean()
    public isEditBtnVisible: boolean = false;

    @Input()
    @IsBoolean()
    public isSelectable: boolean = false;

    @Input()
    @IsBoolean()
    public isSelected: boolean = false;

    @Input()
    @IsBoolean()
    public isGallerySliding: boolean = true;

    @Input()
    @IsBoolean()
    public isColorSelectorEnabled: boolean = true;

    @Input()
    @IsString()
    public equipmentChipTooltip: string = '';

    @Output() public OnSelected: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnDeselected: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnChangeColorButtonClick: EventEmitter<IOnChangeRoomColorEvent> = new EventEmitter();
    @Output() public OnViewButtonClick: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnEditButtonClick: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnEquipmentChipClick: EventEmitter<IOnEquipmentTypeGroupChipClick> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

    public onChangeColor (roomColor: IRoomColor): void {
        this.OnChangeColorButtonClick.emit({
            room: this.room,
            color: roomColor
        });
    }

    public onCardClick (): void {
        if (this.isSelectable) {
            this.isSelected = !this.isSelected;
        }

        this.emitSelectionData();
    }

    public onCheckboxChecked (event: MatCheckboxChange): void {
        this.emitSelectionData();
    }

    public emitSelectionData (): void {
        if (this.isSelected) {
            this.OnSelected.emit(this.room);
        } else {
            this.OnDeselected.emit(this.room);
        }
    }

    public onPhotoGalleryInit (event: any): void {
        // DxGallery repaints after init with timeout, because sometimes it slides incorrect
        setTimeout(() => {
            try {
                const galleryComponent: any = event.component;
                galleryComponent.repaint();
            } catch (error) {}
        }, 100);
    }

}
