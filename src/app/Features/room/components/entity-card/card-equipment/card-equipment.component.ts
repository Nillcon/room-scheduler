import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRoom } from '@Features/room';
import { IsNotEmptyObject, IsString } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IOnEquipmentTypeGroupChipClick } from '@Features/room/interfaces/on-equipment-type-group-chip-click-event.interface';
import { IEquipmentTypeGroup } from '@Features/equipment/interfaces/equipment-type-group.interface';

@ClassValidation()
@Component({
    selector: 'room-card-equipment',
    templateUrl: './card-equipment.component.html',
    styleUrls: ['./card-equipment.component.scss']
})
export class CardEquipmentComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public room: IRoom;

    @Input()
    @IsString()
    public equipmentChipTooltip: string;

    @Output()
    public OnEquipmentChipClick: EventEmitter<IOnEquipmentTypeGroupChipClick> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

    public onChipClick (equipmentTypeGroup: IEquipmentTypeGroup): void {
        this.OnEquipmentChipClick.emit({
            room: this.room,
            equipmentTypeGroup: equipmentTypeGroup
        });
    }

}
