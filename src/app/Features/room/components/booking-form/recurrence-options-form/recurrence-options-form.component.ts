import { Component, OnInit, forwardRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RecurrenceEditorChangeEventArgs, RecurrenceEditor } from '@syncfusion/ej2-schedule';

@Component({
    selector: 'recurrence-options-form',
    templateUrl: './recurrence-options-form.component.html',
    styleUrls: ['./recurrence-options-form.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RecurrenceOptionsFormComponent),
            multi: true
        }
    ]
})
export class RecurrenceOptionsFormComponent implements OnInit, ControlValueAccessor {

    @Output()
    public OnRruleSummaryChange: EventEmitter<string> = new EventEmitter();

    public rrule: string = '';

    private rruleSummary: string;

    @ViewChild('RecurrenceEditor', { static: false })
    private recurrenceEditor: RecurrenceEditor;

    constructor () {}

    public ngOnInit (): void {}

    public onReccurenceDataChange (e: RecurrenceEditorChangeEventArgs): void {
        if ((e as any).name === 'change') {
            const newRrule: string  = e.value;
            this.rrule              = newRrule;
            this.writeValue();
        }

        console.log(this.recurrenceEditor);
        this.rruleSummary = this.recurrenceEditor.getRuleSummary();
        this.OnRruleSummaryChange.emit(this.rruleSummary);
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (rrule?: string): void {
        if (rrule) {
            this.rrule = rrule;
        }

        this.onChange(this.rrule);
    }

    public onChange: Function = () => {};

}
