import { Component, OnInit, Input, forwardRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IRoomTypeOptions } from '@Features/room/interfaces/room-type-options.interface';
import { Validators, ControlValueAccessor, NG_VALUE_ACCESSOR, ValidatorFn, ValidationErrors } from '@angular/forms';
import { RequiredRoomTypeEnum } from '@Features/room/shared/required-room-type.enum';
import { RoomTypesEnum } from '@Features/room';
import { IRequiredRoomType } from '@Features/room/interfaces/required-room-type.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { RequiredRoomTypeMap } from '@Features/room/shared/required-room-type.map';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@BaseForm()
@Component({
    selector: 'room-type-options-form',
    templateUrl: './room-type-options-form.component.html',
    styleUrls: ['./room-type-options-form.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RoomTypeOptionsFormComponent),
            multi: true
        }
    ]
})
export class RoomTypeOptionsFormComponent implements OnInit, OnDestroy, IBaseForm<Partial<IRoomTypeOptions>>, ControlValueAccessor {

    @Input()  public inputData: Partial<IRoomTypeOptions>;
    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRoomTypeOptions>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IRoomTypeOptions>>;

    public readonly roomTypeEnum: typeof RequiredRoomTypeEnum  = RequiredRoomTypeEnum;
    public readonly requiredRoomTypeArray: IRequiredRoomType[] = [...RequiredRoomTypeMap.values()];

    public valueChangesSubscription: Subscription;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (roomTypeOptionsData: Partial<IRoomTypeOptions>): void {
        this.formGroup = this.formBuilder.group<Partial<IRoomTypeOptions>>({
            type: this.formBuilder.control(
                (roomTypeOptionsData) ? roomTypeOptionsData.type : '',
                [Validators.required]
            ),
            selectedItems: this.formBuilder.control(
                (roomTypeOptionsData) ? roomTypeOptionsData.selectedItems : []
            ),
            description: this.formBuilder.control(
                (roomTypeOptionsData) ? roomTypeOptionsData.description : ''
            )
        });

        this.formGroup.setValidators(this.formGroupValidation());

        this.formChangesObserver();
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (roomTypeOptionsData?: Partial<IRoomTypeOptions>): void {
        if (roomTypeOptionsData) {
            this.inputData = roomTypeOptionsData;
        }

        if (this.formGroup) {
            this.onChange(this.formGroup.value);
        }
    }

    public onChange: Function = () => {};

    private formChangesObserver (): void {
        if (this.valueChangesSubscription) {
            this.valueChangesSubscription.unsubscribe();
        }

        this.valueChangesSubscription = this.formGroup.valueChanges
            .subscribe(() => {
                this.writeValue();
            });
    }

    private formGroupValidation (): ValidatorFn {
        return (formGroup: FormGroupTypeSafe<Partial<IRoomTypeOptions>>): ValidationErrors => {
            this.validateSelectedItems(formGroup);
            this.validateDescription(formGroup);
            return;
        };
    }

    private validateSelectedItems (formGroup: FormGroupTypeSafe<Partial<IRoomTypeOptions>>): void {
        if (
            formGroup.controls.type.value === RequiredRoomTypeEnum.Specific
            &&
            (formGroup.controls.selectedItems.value as RoomTypesEnum[]).length === 0
        ) {
            formGroup.controls.selectedItems.setErrors({ required: true });
        } else {
            formGroup.controls.selectedItems.setErrors(null);
        }
    }

    private validateDescription (formGroup: FormGroupTypeSafe<Partial<IRoomTypeOptions>>): void {
        if (
            formGroup.controls.type.value === RequiredRoomTypeEnum.Special
            &&
            !formGroup.controls.description.value
        ) {
            formGroup.controls.description.setErrors({ required: true });
        } else {
            formGroup.controls.description.setErrors(null);
        }
    }

}
