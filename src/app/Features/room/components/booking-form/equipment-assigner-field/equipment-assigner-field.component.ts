import { Component, OnInit, forwardRef, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material';
import { IEquipment } from '@Features/equipment/interfaces/equipment.interface';
import { timer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { EquipmentService } from '@Features/equipment/equipment.service';

@Component({
    selector: 'equipment-assigner-field',
    templateUrl: './equipment-assigner-field.component.html',
    styleUrls: ['./equipment-assigner-field.component.scss'],
    providers: [
        EquipmentService,
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EquipmentAssignerFieldComponent),
            multi: true
        }
    ]
})
export class EquipmentAssignerFieldComponent implements OnInit, ControlValueAccessor {

    @Input()
    public set equipmentArray (equipment: IEquipment[]) {
        this.equipmentMap = new Map();

        equipment.forEach(currEquipment => {
            this.equipmentMap.set(currEquipment.id, currEquipment);
        });

        this._equipmentArray        = equipment;
        this.equipmentAutocomplete = equipment;
    }

    public get equipmentArray (): IEquipment[] {
        return this._equipmentArray;
    }

    @Output()
    public OnSelectionChanged: EventEmitter<number[]> = new EventEmitter();

    public selectedEquipmentIds: number[]           = [];
    public equipmentMap: Map<number, IEquipment>    = new Map();
    public equipmentAutocomplete: IEquipment[]      = [];

    public readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    @ViewChild('Field', { static: false })
    private fieldComponent;

    @ViewChild(MatAutocompleteTrigger, { static: false })
    private autocompleteTriggerComponent: MatAutocompleteTrigger;

    private _equipmentArray: IEquipment[] = [];

    constructor () {}

    public ngOnInit (): void {}

    public addEquipment (equipmentId: number): void {
        const targetEquipmentIndex: number = this.selectedEquipmentIds.findIndex(currEquipmentId => {
            return currEquipmentId === equipmentId;
        });

        if (targetEquipmentIndex === -1) {
            this.selectedEquipmentIds.push(equipmentId);
        }

        this.clearFieldValue();
        this.writeValue();
    }

    public removeEquipment (equipmentId: number): void {
        const targetEquipmentIndex: number = this.selectedEquipmentIds.findIndex(currEquipmentId => {
            return currEquipmentId === equipmentId;
        });

        if (targetEquipmentIndex !== -1) {
            this.selectedEquipmentIds.splice(targetEquipmentIndex, 1);
        }

        this.writeValue();
    }

    public onChipCloseBtnClick (equipmentId: number): void {
        this.removeEquipment(equipmentId);
        this.fieldComponent.nativeElement.focus();
    }

    public onFocus (): void {
        timer(50)
            .pipe(
                tap(() => {
                    this.filterAutocompleteItems();
                    this.autocompleteTriggerComponent.openPanel();
                })
            )
            .subscribe();
    }

    public onAutocompleteItemSelected (event: MatAutocompleteSelectedEvent): void {
        const equipment: IEquipment = event.option.value;
        this.addEquipment(equipment.id);
    }

    public onKeyDown (e: any): void {
        const searchValue: string = e.target.value;
        this.filterAutocompleteItems(searchValue);
    }

    public filterAutocompleteItems (phrase: string = ''): void {
        this.equipmentAutocomplete = this.equipmentArray.filter(currEquipment => {
            const equipmentName = currEquipment.name.toLowerCase();
            const searchValue   = phrase.toLocaleLowerCase();

            return !this.selectedEquipmentIds.includes(currEquipment.id) && equipmentName.includes(searchValue);
        });
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (selectedEquipmentIds?: number[]): void {
        if (selectedEquipmentIds) {
            this.selectedEquipmentIds = selectedEquipmentIds;
        } else {
            this.onChange(this.selectedEquipmentIds);
            this.OnSelectionChanged.emit(this.selectedEquipmentIds);
        }
    }

    public onChange: Function = () => {};

    private clearFieldValue (): void {
        this.fieldComponent.nativeElement.value = '';
    }

}
