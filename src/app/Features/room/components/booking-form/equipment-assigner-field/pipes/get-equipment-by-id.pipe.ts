import { PipeTransform, Pipe } from '@angular/core';
import { IEquipment } from '@Features/equipment';

@Pipe({
    name: 'getEquipmentById'
})
export class GetEquipmentByIdPipe implements PipeTransform {

    constructor () {}

    public transform (id: number, equipments: Map<number, IEquipment>): IEquipment {
        return equipments.get(id);
    }

}
