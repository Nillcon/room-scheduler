import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { IRoomBookingForm } from '../../interfaces/booking-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { IEquipment } from '@Features/equipment';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IRoomTypeOptions } from '@Features/room/interfaces/room-type-options.interface';
import { DateService } from '@Core/root/date/date.service';

@ClassValidation()
@BaseForm()
@Component({
    selector: 'room-booking-form',
    templateUrl: './booking-form.component.html',
    styleUrls: ['./booking-form.component.scss']
})
export class BookingFormComponent implements OnInit, IBaseForm<Partial<IRoomBookingForm>> {

    @Input() public inputData: Partial<IRoomBookingForm>;

    @Input()
    @IsArray()
    public equipmentArray: IEquipment[] = [];

    @Output()
    public OnRruleChange: EventEmitter<string> = new EventEmitter();

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRoomBookingForm>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>;
    public roomTypeOptionsFormGroup: FormGroupTypeSafe<Partial<IRoomTypeOptions>>;

    private _previousDateFrom: Date;
    private _previousDateTo: Date;

    constructor (
        private formBuilder: FormBuilderTypeSafe,
        private dateService: DateService
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (bookingData: Partial<IRoomBookingForm>): void {
        this.formGroup = this.formBuilder.group<Partial<IRoomBookingForm>>({
            dateFrom: this.formBuilder.control(
                bookingData ? bookingData.dateFrom : undefined,
                [Validators.required]
            ),
            dateTo: this.formBuilder.control(
                bookingData ? bookingData.dateTo : undefined,
                [Validators.required]
            ),
            isAllDayEvent: this.formBuilder.control(
                bookingData ? bookingData.isAllDayEvent : false
            ),
            rrule: this.formBuilder.control(
                bookingData ? bookingData.rrule : ''
            ),
            capacity: this.formBuilder.control(
                bookingData ? bookingData.capacity : '',
                [Validators.required]
            ),
            equipmentIds: this.formBuilder.control(
                bookingData ? bookingData.equipmentIds : []
            ),
            roomLayout: this.formBuilder.control(
                bookingData ? bookingData.roomLayout : undefined,
                [this.validateRoomLayout.bind(this)]
            )
        });

        this.formGroup.setValidators(this.formGroupValidation());
    }

    public onAllDayCheckboxToggle (state: boolean): void {
        const dateFrom: Date    = this.formGroup.controls.dateFrom.value;
        const dateTo: Date      = this.formGroup.controls.dateTo.value;

        if (state) {
            if (dateFrom instanceof Date) {
                this._previousDateFrom = dateFrom;
                this.formGroup.controls.dateFrom.setValue(this.dateService.getDateWithoutTime(dateFrom));
            }

            if (dateTo instanceof Date) {
                this._previousDateTo = dateTo;
                this.formGroup.controls.dateTo.setValue(this.dateService.getDateWithoutTime(dateTo));
            }
        } else {
            if (this._previousDateFrom) {
                this.formGroup.controls.dateFrom.setValue(this._previousDateFrom);
            }

            if (this._previousDateTo) {
                this.formGroup.controls.dateTo.setValue(this._previousDateTo);
            }
        }
    }

    public onRoomTypeFormGroupInit (formGroup: FormGroupTypeSafe<Partial<IRoomTypeOptions>>): void {
        this.roomTypeOptionsFormGroup = formGroup;
    }

    private formGroupValidation (): ValidatorFn {
        return (formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>): ValidationErrors => {
            this.validateDateFrom(formGroup);
            this.validateDateTo(formGroup);
            return;
        };
    }

    private validateDateFrom (formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>): void {
        if (formGroup.controls.dateFrom.value && formGroup.controls.dateTo.value) {
            if (formGroup.controls.dateFrom.value.getTime() > formGroup.controls.dateTo.value.getTime()) {
                formGroup.controls.dateFrom.setErrors({ dateFromIsNotValid: true });
                formGroup.controls.dateFrom.markAsTouched();
            } else {
                formGroup.controls.dateFrom.setErrors(null);
            }
        }
    }

    private validateDateTo (formGroup: FormGroupTypeSafe<Partial<IRoomBookingForm>>): void {
        if (formGroup.controls.dateFrom.value && formGroup.controls.dateTo.value) {
            if (formGroup.controls.dateTo.value.getTime() < formGroup.controls.dateFrom.value.getTime()) {
                formGroup.controls.dateTo.setErrors({ dateToIsNotValid: true });
                formGroup.controls.dateTo.markAsTouched();
            } else {
                formGroup.controls.dateTo.setErrors(null);
            }
        }
    }

    private validateRoomLayout (control: FormControl): { roomLayoutIsNotValid: boolean } {
        if (this.roomTypeOptionsFormGroup && !this.roomTypeOptionsFormGroup.valid) {
            return { roomLayoutIsNotValid: true };
        }

        return null;
    }

}
