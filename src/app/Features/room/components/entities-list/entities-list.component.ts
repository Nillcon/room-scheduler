import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRoom } from '@Features/room/interfaces/room.interface';

@Component({
    selector: 'room-list',
    templateUrl: './entities-list.component.html',
    styleUrls: ['./entities-list.component.scss']
})
export class EntitiesListComponent implements OnInit {

    @Input() public roomArray: IRoom[]      = [];
    @Input() public isSelectable: boolean = false;

    @Output() public OnSelection: EventEmitter<IRoom[]> = new EventEmitter();

    public selectedRoomList: Set<IRoom> = new Set();

    constructor () {}

    public ngOnInit (): void {}

    public onRoomSelectionChange (room: IRoom): void {
        if (this.selectedRoomList.has(room)) {
            this.selectedRoomList.delete(room);
        } else {
            this.selectedRoomList.add(room);
        }

        const selectedRoomAsArray: IRoom[] = [...this.selectedRoomList];
        this.OnSelection.next(selectedRoomAsArray);
    }

}
