import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RoomColorEnum } from '@Features/room/shared/room-color.enum';
import { IRoomColor } from '@Features/room/interfaces/room-color.interface';
import { RoomColorMap } from '@Features/room/shared/room-color.map';

@Component({
  selector: 'room-colors-list',
  templateUrl: './colors-list.component.html',
  styleUrls: ['./colors-list.component.scss']
})
export class ColorsListComponent implements OnInit {

    @Input()  public colorId: RoomColorEnum = RoomColorEnum.Blue;
    @Output() public OnChangeColor: EventEmitter<IRoomColor> = new EventEmitter();

    public colorArray: IRoomColor[] = [...RoomColorMap.values()];

    constructor () {}

    public ngOnInit (): void {}

}
