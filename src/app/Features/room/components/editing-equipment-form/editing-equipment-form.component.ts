import { Component, OnInit, Input } from '@angular/core';
import { IRoomEquipment } from '@Features/room/interfaces/room-equipment.interface';
import { IEquipment } from '@Features/equipment/interfaces/equipment.interface';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IsArray, IsObject } from 'class-validator';

@ClassValidation()
@Component({
    selector: 'room-editing-equipment-form',
    templateUrl: './editing-equipment-form.component.html',
    styleUrls: ['./editing-equipment-form.component.scss']
})
export class EditingEquipmentFormComponent implements OnInit {

    @IsObject()
    @Input()
    public roomEquipment: IRoomEquipment;

    @IsArray()
    @Input()
    public equipment: IEquipment[];

    constructor () {}

    public ngOnInit (): void {}

}
