import { Component, OnInit, Input } from '@angular/core';
import { IRoomEquipment } from '@Features/room/interfaces/room-equipment.interface';
import { IEquipment } from '@Features/equipment';

@Component({
    selector: 'equipment-block',
    templateUrl: './equipment-block.component.html',
    styleUrls: ['./equipment-block.component.scss']
})
export class EquipmentBlockComponent implements OnInit {

    @Input()
    public roomEquipment: IRoomEquipment;

    @Input()
    public equipment: IEquipment;

    constructor () { }

    public ngOnInit (): void {
    }

}
