import { Component, OnInit, Input, ViewChild, Injector } from '@angular/core';
import { IRoomPhoto } from '@Features/room/interfaces/room-photo.interface';
import { NotificationService } from '@Core/root/notification/notification.service';
import { NeedsConfirmation } from '@Modules/UI/confirmation-window/decorators/needs-confirmation.decorator';
import { DxGalleryComponent } from 'devextreme-angular';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IsString } from 'class-validator';
import { RoomService } from '@Features/room/room.service';

@ClassValidation()
@Component({
    selector: 'room-editing-gallery-form',
    templateUrl: './editing-gallery-form.component.html',
    styleUrls: ['./editing-gallery-form.component.scss']
})
export class EditingGalleryFormComponent implements OnInit {

    @Input()
    @IsString()
    public roomId: string;

    @ViewChild('gallery', { static: false })
    public gallery: DxGalleryComponent;

    @ViewChild('photoInput', {static: false})
    public photoInput: any;

    public photos: IRoomPhoto[];

    constructor (
        public injector: Injector,
        private roomService: RoomService,
        private notifyService: NotificationService
    ) {}

    public ngOnInit (): void {
        this.updatePhotos();
    }

    public updatePhotos (): void {
        this.roomService.getPhotos(this.roomId)
            .subscribe(res => {
                this.photos = res;
            });
    }

    public selectPhoto (): void {
        this.photoInput.nativeElement.click();
    }

    public uploadPhoto (event: any): void {
        if (event) {
            const file = event.target.files[0];

            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const encoded = reader.result['replace'](/^data:(.*,)?/, '');

                this.roomService.uploadPhoto(this.roomId, encoded)
                    .subscribe(() => {
                        this.updatePhotos();
                        this.notifyService.success({text: 'Photo has been added'});
                    });
            };
        }
    }

    @NeedsConfirmation()
    public deletePhoto (): void {
        const photo: IRoomPhoto = this.gallery.selectedItem;

        this.roomService.deletePhoto(this.roomId, photo.id)
            .subscribe(() => {
                this.updatePhotos();
                this.notifyService.success({text: 'Photo has been deleted'});
            });
    }

}
