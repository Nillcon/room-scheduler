import { IRoom } from './room.interface';
import { IEquipmentTypeGroup } from '@Features/equipment/interfaces/equipment-type-group.interface';

export interface IOnEquipmentTypeGroupChipClick {
    room: IRoom;
    equipmentTypeGroup: IEquipmentTypeGroup;
}
