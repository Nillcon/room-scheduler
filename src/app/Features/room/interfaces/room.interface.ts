import { IRoomColor } from './room-color.interface';
import { IEquipmentTypeGroup } from '@Features/equipment/interfaces/equipment-type-group.interface';

export interface IRoom {
    id: string;
    name: string;
    email: string;
    color: IRoomColor;
    colorHex?: string;
    photos: string[];
    capacity: number;
    equipmentTypeGroups: IEquipmentTypeGroup[];
}
