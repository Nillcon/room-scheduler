import { IRoom } from './room.interface';
import { IRoomColor } from './room-color.interface';

export interface IOnChangeRoomColorEvent {
    room: IRoom;
    color: IRoomColor;
}
