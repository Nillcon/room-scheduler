import { EquipmentTypeEnum } from '@Features/equipment';

export interface IRoomEquipment {
    count: number;
    canBeAdded: boolean;
    type: EquipmentTypeEnum;
}
