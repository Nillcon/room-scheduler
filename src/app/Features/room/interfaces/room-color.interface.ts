import { RoomColorEnum } from '@Features/room';

export interface IRoomColor {
    id: RoomColorEnum;
    name: string;
    hex: string;
    tint: 'light' | 'dark';
}
