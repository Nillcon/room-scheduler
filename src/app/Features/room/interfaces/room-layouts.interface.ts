export interface IRoomLayouts {
    id: number;
    name: string;
    value: number[];
}
