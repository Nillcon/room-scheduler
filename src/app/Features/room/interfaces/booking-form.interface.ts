import { IRoomTypeOptions } from './room-type-options.interface';

export interface IRoomBookingForm {
    dateFrom: Date;
    dateTo: Date;
    isAllDayEvent: boolean;
    rrule: string;
    capacity: number;
    equipmentIds: number[];
    roomLayout: IRoomTypeOptions;
}
