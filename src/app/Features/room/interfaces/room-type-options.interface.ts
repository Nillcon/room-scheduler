import { RoomTypesEnum } from '@Features/room';
import { RequiredRoomTypeEnum } from '../shared/required-room-type.enum';

export interface IRoomTypeOptions {
    type: RequiredRoomTypeEnum;
    selectedItems: RoomTypesEnum[];
    description: string;
}
