import { RequiredRoomTypeEnum } from '../shared/required-room-type.enum';

export interface IRequiredRoomType {
    id: RequiredRoomTypeEnum;
    name: string;
}
