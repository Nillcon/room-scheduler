import { IUser } from '@Features/user';

export interface IRoomApprover {
    roomId?: string;
    approvers: Partial<IUser>[];
    isAutoApproved: boolean;
}
