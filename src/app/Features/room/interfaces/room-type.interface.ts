import { RoomTypesEnum } from '../shared/room-types.enum';

export interface IRoomType {
    name: string;
    imageUrl: string;
    type: RoomTypesEnum;
}
