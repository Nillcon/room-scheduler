export interface IRoomPhoto {
    id: number;
    photo: string;
}
