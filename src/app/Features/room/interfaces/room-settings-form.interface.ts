export interface IRoomSettingsForm {
    roomId: string;
    isPrivate: boolean;
}
