import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { RoomColorEnum } from './shared/room-color.enum';
import { IRoomLayouts } from './interfaces/room-layouts.interface';
import { IRoomPhoto } from './interfaces/room-photo.interface';
import { IRoom } from './interfaces/room.interface';
import { IRoomEquipment } from './interfaces/room-equipment.interface';
import { IRoomBookingForm } from './interfaces/booking-form.interface';
import { IRoomApprover } from './interfaces/room-approver.interface';
import { IRoomSettingsForm } from './interfaces/room-settings-form.interface';

@Injectable()
export class RoomApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAll (): Observable<IRoom[]> {
        return this.httpService.get<IRoom[]>('/Rooms');
    }

    public getFilteredRooms (bookingData: Partial<IRoomBookingForm>): Observable<IRoom[]> {
        const data: string = JSON.stringify(bookingData);
        return this.httpService.get<IRoom[]>(`/Rooms/SearchByFilter?value=${data}`);
    }

    public changeColor (roomId: string, roomColorId: RoomColorEnum): Observable<any> {
        return this.httpService.put<any>(`/Rooms/${roomId}/SetColor`, {
            value: roomColorId
        });
    }

    public saveLayouts (id: string, roomTypes: number[]): Observable<any> {
        return this.httpService.put<any>(`/Rooms/${id}/Layouts`, {
            value: roomTypes
        });
    }

    public getLayouts (id: string): Observable<IRoomLayouts> {
        return this.httpService.get<IRoomLayouts>(`/Rooms/${id}/Layouts`);
    }

    public getPhotos (id: string): Observable<IRoomPhoto[]> {
        return this.httpService.get<IRoomPhoto[]>(`/Rooms/${id}/Photos`);
    }

    public uploadPhoto (roomId: string, imageBase64: string): Observable<any> {
        return this.httpService.post(`/Rooms/${roomId}/Photos`, {
            value: imageBase64
        });
    }

    public getSettings (roomId: string): Observable<IRoomSettingsForm> {
        return this.httpService.get(`/Rooms/${roomId}/Settings`);
    }

    public editSettings (roomId: string, data: Partial<IRoomSettingsForm>): Observable<IRoomSettingsForm> {
        return this.httpService.put(`/Rooms/${roomId}/Settings`, data);
    }

    public deletePhoto (roomId: string, photoId: number): Observable<any> {
        return this.httpService.delete(`/Rooms/${roomId}/Photos/${photoId}`);
    }

    public getApprovers (roomId: string): Observable<IRoomApprover> {
        return this.httpService.get(`/Rooms/${roomId}/Approvers`);
    }

    public setApprovers (roomId: string, approversInfo: IRoomApprover): Observable<any> {
        return this.httpService.put(`/Rooms/${roomId}/Approvers`, {
            isAutoApproved: approversInfo.isAutoApproved,
            approvers: approversInfo.approvers
        });
    }

    public getEquipment (roomId: string): Observable<IRoomEquipment[]> {
        return this.httpService.get(`/Rooms/${roomId}/Equipment`);
    }

    public setEquipment (roomId: string, equipment: IRoomEquipment[]): Observable<any> {
        return this.httpService.put(`/Rooms/${roomId}/Equipment`, {
            equipment: equipment
        });
    }

    public checkValid (id: string): Observable<boolean> {
        return this.httpService.get(`/Rooms/${id}/isIdValid`);
    }

    public getGroupNamesWithApprovers (id: string): Observable<string[]> {
        return this.httpService.get(`/Rooms/${id}/GroupNamesWithApprovers`);
    }

}
