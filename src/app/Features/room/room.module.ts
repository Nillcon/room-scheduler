import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomApiService } from './room.api.service';
import { SharedModule } from '@App/app-shared.module';
import { EntityCardComponent } from './components/entity-card/entity-card.component';
import { EntityCardsListComponent } from './components/entity-cards-list/entity-cards-list.component';
import { ColorsListComponent } from './components/colors-list/colors-list.component';
import { CardColorChangerComponent } from './components/entity-card/card-color-changer/card-color-changer.component';
import { CardEquipmentComponent } from './components/entity-card/card-equipment/card-equipment.component';
import { SelectorTypeComponent } from './components/selector-type/selector-type.component';
import { EditingGalleryFormComponent } from './components/editing-gallery-form/editing-gallery-form.component';
import { ApproveFieldComponent } from './components/approve-field/approve-field.component';
import { DxGalleryModule, DxDateBoxModule, DxValidatorModule } from 'devextreme-angular';
import { EditingEquipmentFormComponent } from './components/editing-equipment-form/editing-equipment-form.component';

import { EquipmentBlockComponent } from './components/editing-equipment-form/equipment-block/equipment-block.component';
import { BookingFormComponent } from './components/booking-form/booking-form.component';
import { EquipmentAssignerFieldComponent } from './components/booking-form/equipment-assigner-field/equipment-assigner-field.component';
import { RoomTypeOptionsFormComponent } from './components/booking-form/room-type-options-form/room-type-options-form.component';
import { EntitiesListComponent } from './components/entities-list/entities-list.component';
import { RoomService } from './room.service';
import { RecurrenceOptionsFormComponent } from './components/booking-form/recurrence-options-form/recurrence-options-form.component';
import { RecurrenceEditorModule } from '@syncfusion/ej2-angular-schedule';
import { SettingsFormComponent } from './components/settings-form/settings-form.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';
import { GetEquipmentByIdPipe } from './components/booking-form/equipment-assigner-field/pipes/get-equipment-by-id.pipe';


@NgModule({
    declarations: [
        GetEquipmentByIdPipe,

        EntityCardComponent,
        CardColorChangerComponent,
        CardEquipmentComponent,

        BookingFormComponent,
        EquipmentAssignerFieldComponent,
        RoomTypeOptionsFormComponent,

        EntityCardsListComponent,
        ColorsListComponent,
        SelectorTypeComponent,
        EditingGalleryFormComponent,
        ApproveFieldComponent,
        EditingEquipmentFormComponent,
        EquipmentBlockComponent,
        EntitiesListComponent,
        RecurrenceOptionsFormComponent,
        SettingsFormComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule,

        DxGalleryModule,
        DxDateBoxModule,
        DxValidatorModule,
        RecurrenceEditorModule,
    ],
    providers: [
        RoomService,
        RoomApiService
    ],
    exports: [
        BookingFormComponent,
        EntityCardComponent,
        EntityCardsListComponent,
        ColorsListComponent,
        SelectorTypeComponent,
        EditingGalleryFormComponent,
        ApproveFieldComponent,
        EditingEquipmentFormComponent,
        EntitiesListComponent,
        SettingsFormComponent
    ]
})
export class RoomModule { }
