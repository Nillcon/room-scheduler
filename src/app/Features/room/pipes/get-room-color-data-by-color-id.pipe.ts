import { PipeTransform, Pipe } from '@angular/core';
import { RoomColorEnum } from '../shared/room-color.enum';
import { IRoomColor } from '../interfaces/room-color.interface';
import { RoomColorMap } from '../shared/room-color.map';

@Pipe({
    name: 'getRoomColorDataByColorId'
})
export class GetRoomColorDataByColorIdPipe implements PipeTransform {

    constructor () {}

    public transform (type: RoomColorEnum): IRoomColor {
        return RoomColorMap.get(type);
    }

}
