import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTableComponent } from './components/list-table/list-table.component';
import { AuthorizationFormComponent } from './components/authorization-form/authorization-form.component';
import { SharedModule } from '@App/app-shared.module';
import { UserApiService } from './user.api.service';
import { CreationFormComponent } from './components/creation-form/creation-form.component';
import { EditingFormComponent } from './components/editing-form/editing-form.component';
import { ChangePasswordFormComponent } from './components/change-password-form/change-password-form.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListTableComponent,
        AuthorizationFormComponent,
        CreationFormComponent,
        EditingFormComponent,
        ChangePasswordFormComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    exports: [
        AuthorizationFormComponent,
        CreationFormComponent,
        EditingFormComponent,
        ChangePasswordFormComponent,

        ListTableComponent
    ],
    providers: [
        UserApiService
    ]
})
export class UserModule { }
