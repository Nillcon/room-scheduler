import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IUser } from '@Features/user';
import { Validators, FormControl } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EmailValidator } from '@Validators/email.validator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { UserService } from '@Features/user/user.service';

@BaseForm()
@Component({
    selector: 'user-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss']
})
export class CreationFormComponent implements OnInit, IBaseForm<Partial<IUser>> {

    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IUser>>> = new EventEmitter();

    public isShowPassword: boolean  = false;

    public formGroup: FormGroupTypeSafe<Partial<IUser>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe,
        private userService: UserService
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.formBuilder.group<Partial<IUser>>({
            login: this.formBuilder.control(
                '',
                [Validators.required],
                [this.validateLogin.bind(this)]
            ),
            name: this.formBuilder.control(
                '',
                [Validators.required]
            ),
            email: this.formBuilder.control(
                '',
                [
                    Validators.required,
                    EmailValidator.bind(this)
                ]
            ),
            phoneNumber: this.formBuilder.control(
                '',
                [Validators.required]
            ),
            position: this.formBuilder.control(''),
            address: this.formBuilder.control(''),
            password: this.formBuilder.control(
                '',
                [Validators.required]
            ),
            note: this.formBuilder.control(''),
        });
    }

    public changeVisibilityPassword (): void {
        this.isShowPassword = !this.isShowPassword;
    }

    private validateLogin (control: FormControl): Observable<{ loginExist: boolean }> {
        return this.userService.checkIsLoginExist(control.value)
            .pipe(
                map(isLoginExist => {
                    return (isLoginExist) ? { loginExist: true } : null;
                })
            );
    }

}
