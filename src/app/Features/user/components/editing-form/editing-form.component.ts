import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IUser } from '@Features/user';
import { Validators, FormControl } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Observable, of, Subscription } from 'rxjs';
import { EmailValidator } from '@Validators/email.validator';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { UserService } from '@Features/user/user.service';

@BaseForm()
@Component({
    selector: 'user-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss']
})
export class EditingFormComponent implements OnInit, IBaseForm<Partial<IUser>> {

    @Input()
    public inputData: IUser;

    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IUser>>> = new EventEmitter();

    public currentUserLogin: string = null;

    public saveSubscription: Subscription;

    public formGroup: FormGroupTypeSafe<Partial<IUser>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe,
        private userService: UserService
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (user: Partial<IUser> = {}): void {
        this.formGroup = this.formBuilder.group<Partial<IUser>>({
            login: this.formBuilder.control(
                user.login || '',
                [Validators.required],
                [this.validateLogin.bind(this)]
            ),
            name: this.formBuilder.control(user.name || '', [
                Validators.required
            ]),
            email: this.formBuilder.control(user.email || '', [
                Validators.required,
                EmailValidator.bind(this)
            ]),
            phoneNumber: this.formBuilder.control(user.phoneNumber || '', [
                Validators.required
            ]),
            position: this.formBuilder.control(user.position || ''),
            address: this.formBuilder.control(user.address || ''),
            note: this.formBuilder.control(user.note || ''),
        });

        this.currentUserLogin = user.login;
    }

    private validateLogin (control: FormControl): Observable<{ loginExist: boolean }> | null {
        if (control.value !== this.currentUserLogin) {
            return this.userService.checkIsLoginExist(control.value)
                .pipe(
                    map(isLoginExist => {
                        return (isLoginExist) ? { loginExist: true } : null;
                    })
                );
        }

        return of(null);
    }

}
