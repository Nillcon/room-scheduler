import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IUser } from '@Features/user';

@Component({
    selector: 'users-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()  public users: IUser[];
    @Output() public OnChangePasswordButtonClick: EventEmitter<IUser> = new EventEmitter();
    @Output() public OnEditButtonClick: EventEmitter<IUser> = new EventEmitter();
    @Output() public OnDeleteButtonClick: EventEmitter<IUser> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
