import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators, FormControl } from '@angular/forms';
import { IChangePasswordForm } from '@Features/user';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';

@BaseForm()
@Component({
    selector: 'user-change-password-form',
    templateUrl: './change-password-form.component.html',
    styleUrls: ['./change-password-form.component.scss']
})
export class ChangePasswordFormComponent implements OnInit, IBaseForm<IChangePasswordForm> {

    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<IChangePasswordForm>> = new EventEmitter();

    public isShowPassword: boolean = false;
    public isShowConfirmPassword: boolean = false;

    public formGroup: FormGroupTypeSafe<IChangePasswordForm>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.formBuilder.group<IChangePasswordForm>({
            password: this.formBuilder.control(
                '',
                [
                    Validators.required
                ]
            ),
            confirmPassword: this.formBuilder.control(
                '',
                [
                    Validators.required,
                    this.validateConfirmPassword.bind(this)
                ]
            )
        });
    }

    public changeVisibilityPassword (): void {
        this.isShowPassword = !this.isShowPassword;
    }

    public changeVisibilityConfirmPassword (): void {
        this.isShowConfirmPassword = !this.isShowConfirmPassword;
    }

    public validateConfirmPassword (control: FormControl): { passwordsAreDifferent: boolean } {
        if (this.formGroup) {
            return (control.value === this.formGroup.controls.password.value) ? null : { passwordsAreDifferent: true };
        }

        return null;
    }

}
