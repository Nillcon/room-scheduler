import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { FormBuilderTypeSafe, FormGroupTypeSafe } from 'form-type-safe';
import { ISignInForm } from '@Features/user';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';

@BaseForm()
@Component({
    selector: 'user-authorization-form',
    templateUrl: './authorization-form.component.html',
    styleUrls: ['./authorization-form.component.scss']
})
export class AuthorizationFormComponent implements OnInit, IBaseForm<ISignInForm> {

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<ISignInForm>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<ISignInForm>;

    public isShowPassword: boolean = false;
    public authorizeSubscription: Subscription;

    constructor (
        private fb: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.fb.group<ISignInForm>({
            login: this.fb.control('', [
                Validators.required
            ]),
            password: this.fb.control('', [
                Validators.required
            ])
        });

        this.OnFormInit.emit(this.formGroup);
    }

    public changeVisibilityPassword (): void {
        this.isShowPassword = !this.isShowPassword;
    }
}
