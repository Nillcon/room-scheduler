import { Injectable } from '@angular/core';
import { UserApiService } from './user.api.service';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { IUser, ISignInForm } from '@Features/user';
import { tap, catchError, switchMap, filter } from 'rxjs/operators';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';
import { RoleEnum } from '@Features/role';
import { IChangePasswordForm } from './interfaces/change-password-form.interface';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public get Data (): IUser {
        return this._data.getValue();
    }
    public get Data$ (): Observable<IUser> {
        return this._data.asObservable();
    }

    public get IsAuthorized (): boolean {
        return this._isAuthorized.getValue();
    }
    public get IsAuthorized$ (): Observable<boolean> {
        return this._isAuthorized
            .pipe(
                filter(res => res !== null)
            );
    }

    private _isAuthorized: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    private _data: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(null);

    constructor (
        private userApiService: UserApiService,
        private storageService: LocalStorageService
    ) {}

    /** Call authorize function and
     * write token to localStorage,
     * update user data
    */
    public authorize (data: ISignInForm): Observable<any> {
        return this.userApiService.authorize(data)
            .pipe(
                tap((token: string) =>  this.storageService.set(StorageKey.Access_Token, token)),
                switchMap(() => this.update())
            );
    }

    public logout (): void {
        this.storageService.remove(StorageKey.Access_Token);
        this.makeUserUnauthorized();
    }

    /** Update info about user and get new one.
     * It writes data into Data/Data$ fields.
     * It also writes boolean value in IsAuthorized field.
     */
    public update (): Observable<IUser> {
        return this.userApiService.getInfo()
            .pipe(
                catchError(() => {
                    this.makeUserUnauthorized();
                    return throwError(null);
                }),
                tap(data => {
                    if (data) {
                        this.makeUserAuthorized(data);
                    } else {
                        this.makeUserUnauthorized();
                    }
                })
            );
    }

    public includesRole (role: RoleEnum): boolean {
        return this.Data.roles.includes(role);
    }

    public includesOneOfRoles (roles: RoleEnum[]): boolean {
        let isIncludes: boolean = false;

        for (const role of roles) {
            if (this.Data.roles.includes(role)) {
                isIncludes = true;
                break;
            }
        }

        return isIncludes;
    }

    public getAll (): Observable<IUser[]> {
        return this.userApiService.getAll();
    }

    public get (id: number): Observable<IUser> {
        return this.userApiService.get(id);
    }

    /** Get info about user by token in headers */
    public getInfo (): Observable<IUser> {
        return this.userApiService.getInfo();
    }

    public getUsersWithRole (roleId: RoleEnum): Observable<IUser[]> {
        return this.userApiService.getUsersWithRole(roleId);
    }

    public checkIsLoginExist (login: string): Observable<boolean> {
        return this.userApiService.checkIsLoginExist(login);
    }

    /** Search users by name or login
     * @param phrase - Part of username or login to search
     */
    public search (phrase: string): Observable<IUser[]> {
        return this.userApiService.search(phrase);
    }

    public create (data: Partial<IUser>): Observable<any> {
        return this.userApiService.create(data);
    }

    public edit (id: number, data: Partial<IUser>): Observable<any> {
        return this.userApiService.edit(id, data);
    }

    public changePassword (id: number, data: IChangePasswordForm): Observable<boolean> {
        return this.userApiService.changePassword(id, data);
    }

    public delete (id: number): Observable<any> {
        return this.userApiService.delete(id);
    }

    private makeUserUnauthorized (): void {
        this._isAuthorized.next(false);
        this._data.next(null);
    }

    private makeUserAuthorized (data: IUser): void {
        this._data.next(data);
        this._isAuthorized.next(true);
    }
}
