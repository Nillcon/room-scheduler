import { RoleEnum } from '@Features/role';

export interface IUser {
    id?: number;
    login: string;
    name: string;
    salt?: string;
    note?: string;
    email: string;
    photoBase64String: string;
    password?: string;
    phoneNumber: string;
    hashCode?: string;
    position?: string;
    address?: string;
    roles: RoleEnum[];
}
