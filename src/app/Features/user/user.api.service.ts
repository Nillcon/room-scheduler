import { HttpRequestService } from '@App/Core/root/http/http-request.service';
import { ISignInForm, IUser, IChangePasswordForm } from '@Features/user';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RoleEnum } from '@Features/role';

@Injectable()
export class UserApiService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public authorize (data: ISignInForm): Observable<any> {
        return this.httpService.post<any>(`/Users/SignIn`, data);
    }

    public getAll (): Observable<IUser[]> {
        return this.httpService.get<IUser[]>(`/Users`);
    }

    public get (id: number): Observable<IUser> {
        return this.httpService.get<IUser>(`/Users/${id}`);
    }

    /** Get info about user by token in headers */
    public getInfo (): Observable<IUser> {
        return this.httpService.get<IUser>(`/Users/Info`);
    }

    public getUsersWithRole (roleId: RoleEnum): Observable<IUser[]> {
        return this.httpService.get<IUser[]>(`/Users/Roles/${roleId}`);
    }

    public checkIsLoginExist (login: string): Observable<boolean> {
        return this.httpService.get<boolean>(`/Users/IsLoginExist?searchString=${login}`);
    }

    /** Search users by name or login
     * @param phrase - Part of username or login to search
     */
    public search (phrase: string): Observable<IUser[]> {
        return this.httpService.get<IUser[]>(`/Users/Search?searchString=${phrase}`);
    }

    public create (data: Partial<IUser>): Observable<any> {
        return this.httpService.post(`/Users`, data);
    }

    public edit (id: number, data: Partial<IUser>): Observable<any> {
        return this.httpService.put(`/Users/${id}`, data);
    }

    public changePassword (id: number, data: IChangePasswordForm): Observable<boolean> {
        return this.httpService.put<boolean>(`/Users/${id}/ChangePassword`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/Users/${id}`);
    }
}
