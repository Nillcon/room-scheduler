import { TestBed, async } from '@angular/core/testing';
import { UserService } from './user.service';
import { UserApiService } from './user.api.service';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { ISignInForm } from './interfaces/sign-in-form.interface';
import { LocalStorageService } from '@Core/root/storage/local-storage.service';
import { StorageKey } from '@Core/root/storage/shared/storage-key';

describe('UserService', () => {
    const suSignInData: ISignInForm     = { login: 'su', password: 'su2019' };
    const fakeSignInData: ISignInForm   = { login: 'somefakeuser', password: 'somefakepass' };

    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            HttpClientModule
        ],
        providers: [
            UserApiService,
            UserService,
            LocalStorageService
        ]
    }));

    it('should be created', () => {
        const userService: UserService = TestBed.get(UserService);
        expect(userService).toBeTruthy();
    });

    it('should be authorize and unauthorized', async(() => {
        const localStorageService: LocalStorageService = TestBed.get(LocalStorageService);
        const userService: UserService = TestBed.get(UserService);

        userService.authorize(suSignInData)
            .subscribe(result => {
                expect(result).toBeTruthy();
                expect(userService.IsAuthorized).toBe(true);
                expect(userService.Data).toEqual(result);

                expect(localStorageService.get(StorageKey.Access_Token)).toBeTruthy();

                userService.logout();

                expect(localStorageService.get(StorageKey.Access_Token)).toBeNull();
                expect(userService.Data).toBeNull();
                expect(userService.IsAuthorized).toBe(false);
            });
    }));

    it('should be not authorize with fake data', async(() => {
        const userService: UserService = TestBed.get(UserService);
        userService.authorize(fakeSignInData)
            .subscribe(
                () => {},
                (error: HttpErrorResponse) => {
                    expect(error.status).toBe(500, 'status');
                    expect(userService.Data).toBeNull();
                    expect(userService.IsAuthorized).toBeFalsy();
                }
            );
    }));
});
