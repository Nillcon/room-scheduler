import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { IReport } from '@Features/report/interfaces/report.interface';

@Component({
    selector: 'report-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    @Input()
    public reports: IReport[];

    @Output()
    public OnSelected: EventEmitter<IReport> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }
}
