import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IReport } from './interfaces/report.interface';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';
import { ReportApiService } from './report.api.service';
import { DateService } from '@Core/root/date/date.service';

@Injectable()
export class ReportService {

    constructor (
        private reportApiService: ReportApiService,
        private dateService: DateService
    ) {}

    public getAll (): Observable<IReport[]> {
        return this.reportApiService.getAll();
    }

    public get (id: number): Observable<IReport> {
        return this.reportApiService.get(id);
    }

    public create (report: IReport): Observable<any> {
        return this.reportApiService.create(report);
    }

    public edit (report: IReport): Observable<boolean> {
        return this.reportApiService.edit(report);
    }

    public delete (id: number): Observable<boolean> {
        return this.reportApiService.delete(id);
    }

    public download (id: number, dateRange: IDateRange): Observable<Blob> {
        const newData: object = Object.assign({}, dateRange);
        newData['from'] = this.dateService.getLocalJsonTimeString(dateRange.from);
        newData['to']   = this.dateService.getLocalJsonTimeString(dateRange.to);

        return this.reportApiService.download(id, (newData as IDateRange));
    }

}
