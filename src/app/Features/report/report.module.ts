import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportApiService } from './report.api.service';
import { ListComponent } from './components/list/list.component';
import { SharedModule } from '@App/app-shared.module';
import { ReportService } from './report.service';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    providers: [
        ReportApiService,
        ReportService
    ],
    exports: [
        ListComponent
    ]
})
export class ReportModule {}
