import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IReport } from './interfaces/report.interface';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { IDateRange } from '@Core/root/date/interfaces/date-range.interface';
import { HttpFileService } from '@Core/root/http/http-file.service';

@Injectable()
export class ReportApiService {

    constructor (
        private httpService: HttpRequestService,
        private httpFileService: HttpFileService,
    ) {}

    public getAll (): Observable<IReport[]> {
        return this.httpService.get<IReport[]>(`/Reports`);
    }

    public get (id: number): Observable<IReport> {
        return this.httpService.get<IReport>(`/Reports/${id}`);
    }

    public create (report: IReport): Observable<any> {
        return this.httpService.post<any>(`/Reports`, report);
    }

    public edit (report: IReport): Observable<boolean> {
        return this.httpService.put(`/Reports/${report.type}`, report);
    }

    public delete (id: number): Observable<boolean> {
        return this.httpService.delete(`/Reports/${id}`);
    }

    public download (id: number, dateRange: IDateRange): Observable<Blob> {
        return this.httpFileService.downloadFile(
            `/Reports/${id}?from=${dateRange.from}&to=${dateRange.to}`
        );
    }

}
