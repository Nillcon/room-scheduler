import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTableComponent } from './components/list-table/list-table.component';
import { SharedModule } from '@App/app-shared.module';
import { InvoicesApiService } from './invoices.api.service';
import { EditingFormComponent } from './components/editing-form/editing-form.component';
import { CreationFormComponent } from './components/creation-form/creation-form.component';
import { StatusSelectorComponent } from './components/status-selector/status-selector.component';
import { InvoicesService } from './invoices.service';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListTableComponent,
        EditingFormComponent,
        CreationFormComponent,
        StatusSelectorComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    providers: [
        InvoicesApiService,
        InvoicesService
    ],
    exports: [
        ListTableComponent,
        EditingFormComponent,
        CreationFormComponent,
        StatusSelectorComponent
    ]
})
export class InvoicesModule { }
