
import { PipeTransform, Pipe } from '@angular/core';
import { InvoiceStatusesMap } from '../shared/status.map';
import { InvoiceStatusesEnum } from '../shared/status.enum';
import { IInvoiceMapItem } from '../interfaces/invoice-map-item.interface';

@Pipe({
    name: 'getInvoiceStatusDataByStatus'
})
export class GetInvoiceStatusDataByStatusPipe implements PipeTransform {

    constructor () {}

    public transform (type: InvoiceStatusesEnum): IInvoiceMapItem {
        return InvoiceStatusesMap.get(type);
    }

}
