import { InvoiceStatusesEnum} from './status.enum';
import { IInvoiceMapItem } from '../interfaces/invoice-map-item.interface';


export const InvoiceStatusesMap: Map<InvoiceStatusesEnum, IInvoiceMapItem> = new  Map<InvoiceStatusesEnum, IInvoiceMapItem> ()
    .set(
        InvoiceStatusesEnum.Paid,
        {
            id: InvoiceStatusesEnum.Paid,
            name: 'Paid',
            iconClass: 'fas fa-dollar-sign text-success'
        }
    )
    .set(
        InvoiceStatusesEnum.NotPaid,
        {
            id: InvoiceStatusesEnum.NotPaid,
            name: 'Not paid',
            iconClass: 'fab fa-creative-commons-nc text-warning'
        }
    )
;
