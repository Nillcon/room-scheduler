export interface IInvoiceMapItem {
    id: number;
    name: string;
    iconClass: string;
}
