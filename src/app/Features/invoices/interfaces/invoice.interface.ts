import { InvoiceStatusesEnum} from '../shared/status.enum';

export interface IInvoice {
    id: number;
    number: string;
    invoiceDate: string;
    description: string;
    requestEventId: number;
    status: InvoiceStatusesEnum;
}
