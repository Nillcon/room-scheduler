import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { IInvoice } from '@Features/invoices/interfaces/invoice.interface';
import { IInvoiceMapItem } from '@Features/invoices/interfaces/invoice-map-item.interface';
import { InvoiceStatusesMap } from '@Features/invoices/shared/status.map';
import { InvoiceStatusesEnum} from '@Features/invoices/shared/status.enum';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { Validators } from '@angular/forms';
import { IRequestEvent } from '@Features/request-event';

@BaseForm()
@Component({
    selector: 'invoice-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss']
})
export class EditingFormComponent implements OnInit, IBaseForm<Partial<IInvoice>> {

    @Input()
    public inputData: Partial<IInvoice>;

    @Input()
    public requestEvents: IRequestEvent[];

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IInvoice>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IInvoice>>;

    public InvoiceStatusesArray: IInvoiceMapItem[] = [...InvoiceStatusesMap.values()];

    constructor (
        private fb: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (inputData: Partial<IInvoice>): void {
        this.formGroup = this.fb.group<Partial<IInvoice>>({
            number: this.fb.control(inputData.number, [
                Validators.required
            ]),
            requestEventId: this.fb.control(inputData.requestEventId, [
                Validators.required
            ]),
            invoiceDate: this.fb.control(inputData.invoiceDate, [
                Validators.required
            ]),
            description: this.fb.control(inputData.description, [
                Validators.required
            ])
        });
    }

}
