import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IInvoice } from '@Features/invoices/interfaces/invoice.interface';

@Component({
    selector: 'invoice-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()
    public invoices: IInvoice[];

    @Output()
    public OnStatusClick: EventEmitter<IInvoice> = new EventEmitter();

    @Output()
    public OnPDFClick: EventEmitter<IInvoice> = new EventEmitter();

    @Output()
    public OnEditClick: EventEmitter<IInvoice> = new EventEmitter();

    @Output()
    public OnDeleteClick: EventEmitter<IInvoice> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {}

}
