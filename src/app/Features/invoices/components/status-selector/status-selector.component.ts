import { Component, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { InvoiceStatusesEnum} from '@Features/invoices/shared/status.enum';
import { InvoiceStatusesMap } from '@Features/invoices/shared/status.map';
import { IInvoiceMapItem } from '@Features/invoices/interfaces/invoice-map-item.interface';

@Component({
    selector: 'invoice-status-selector',
    templateUrl: './status-selector.component.html',
    styleUrls: ['./status-selector.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: StatusSelectorComponent,
            multi: true
        }
    ]
})
export class StatusSelectorComponent implements OnInit, ControlValueAccessor {

    public status: InvoiceStatusesEnum = InvoiceStatusesEnum.NotPaid;

    public invoiceStatusesArray: IInvoiceMapItem[] = [...InvoiceStatusesMap.values()];

    constructor () { }

    public ngOnInit (): void {
    }

    public registerOnChange (fn: any): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (value: InvoiceStatusesEnum): void {
        value = (Number.isInteger(value)) ? value : InvoiceStatusesEnum.NotPaid;

        this.status = value;

        this.onChange(value);
    }

    public onChange: any = () => {};
}
