import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { IInvoice } from '@Features/invoices/interfaces/invoice.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { InvoiceStatusesEnum} from '@Features/invoices/shared/status.enum';
import { InvoiceStatusesMap } from '@Features/invoices/shared/status.map';
import { IInvoiceMapItem } from '@Features/invoices/interfaces/invoice-map-item.interface';
import { IRequestEvent } from '@Features/request-event';
import { Validators } from '@angular/forms';

@BaseForm()
@Component({
    selector: 'invoice-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss']
})
export class CreationFormComponent implements OnInit, IBaseForm<Partial<IInvoice>> {

    @Input()
    public requestEvents: IRequestEvent[];

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IInvoice>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IInvoice>>;

    public InvoiceStatusesArray: IInvoiceMapItem[] = [...InvoiceStatusesMap.values()];

    constructor (
        private fb: FormBuilderTypeSafe,
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.fb.group<Partial<IInvoice>>({
            number: this.fb.control('', [
                Validators.required
            ]),
            requestEventId: this.fb.control('', [
                Validators.required
            ]),
            invoiceDate: this.fb.control('', [
                Validators.required
            ]),
            description: this.fb.control('', [
                Validators.required
            ]),
            status: this.fb.control(InvoiceStatusesEnum.NotPaid, [
                Validators.required
            ]),
        });
    }
}
