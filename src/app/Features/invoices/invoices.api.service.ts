import { Injectable } from '@angular/core';
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { IInvoice } from './interfaces/invoice.interface';
import { Observable } from 'rxjs';
import { InvoiceStatusesEnum} from './shared/status.enum';
import { HttpFileService } from '@Core/root/http/http-file.service';

@Injectable()
export class InvoicesApiService {

    constructor (
        private httpService: HttpRequestService,
        private httpFileService: HttpFileService
    ) {}

    public get (id: number): Observable<IInvoice> {
        return this.httpService.get<IInvoice>(`/Invoices/${id}`);
    }

    public getAll (): Observable<IInvoice[]> {
        return this.httpService.get<IInvoice[]>(`/Invoices`);
    }

    public create (invoice: Partial<IInvoice>): Observable<any> {
        return this.httpService.post<any>(`/Invoices`, invoice);
    }

    public edit (invoice: Partial<IInvoice>): Observable<any> {
        return this.httpService.put<any>(`/Invoices/${invoice.id}`, invoice);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/Invoices/${id}`);
    }

    public changeStatus (invoice: Partial<IInvoice>, status: InvoiceStatusesEnum): Observable<any> {
        return this.httpService.put<any>(`/Invoices/${invoice.id}/ChangeStatus/${status}`, invoice);
    }

    public downloadPdf (invoice: Partial<IInvoice>): Observable<Blob> {
        return this.httpFileService.downloadFile(`/Invoices/${invoice.id}/pdf`);
    }

}
