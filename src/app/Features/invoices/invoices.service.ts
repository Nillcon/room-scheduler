import { Injectable } from '@angular/core';
import { IInvoice } from './interfaces/invoice.interface';
import { Observable } from 'rxjs';
import { InvoicesApiService } from './invoices.api.service';
import { InvoiceStatusesEnum } from './shared/status.enum';

@Injectable()
export class InvoicesService {

    constructor (
        private invoicesApiService: InvoicesApiService
    ) {}

    public get (id: number): Observable<IInvoice> {
        return this.invoicesApiService.get(id);
    }

    public getAll (): Observable<IInvoice[]> {
        return this.invoicesApiService.getAll();
    }

    public create (invoice: Partial<IInvoice>): Observable<any> {
        return this.invoicesApiService.create(invoice);
    }

    public edit (invoice: Partial<IInvoice>): Observable<any> {
        return this.invoicesApiService.edit(invoice);
    }

    public delete (id: number): Observable<any> {
        return this.invoicesApiService.delete(id);
    }

    public changeStatus (invoice: Partial<IInvoice>, status: InvoiceStatusesEnum): Observable<any> {
        return this.invoicesApiService.changeStatus(invoice, status);
    }

    public downloadPdf (invoice: Partial<IInvoice>): Observable<Blob> {
        return this.invoicesApiService.downloadPdf(invoice);
    }

}
