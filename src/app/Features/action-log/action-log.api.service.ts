import { Injectable } from "@angular/core";
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { Observable } from 'rxjs';
import { IActionLog } from '@Features/action-log';
import DevExpress from 'devextreme/bundles/dx.all';

@Injectable()
export class ActionLogApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAllWithOData (options: DevExpress.data.LoadOptions): Observable<IActionLog[]> {
        return this.httpService.get<IActionLog[]>(`/Logs/OData?loadOptions=${JSON.stringify(options)}`);
    }

}
