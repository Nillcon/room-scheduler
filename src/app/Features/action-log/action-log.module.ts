import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionLogApiService } from './action-log.api.service';
import { ActionLogService } from './action-log.service';
import { ListTableComponent } from './components/list-table/list-table.component';
import { SharedModule } from '@App/app-shared.module';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        ListTableComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    exports: [
        ListTableComponent
    ],
    providers: [
        ActionLogApiService,
        ActionLogService
    ]
})
export class ActionLogModule {}
