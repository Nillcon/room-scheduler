import { Component, OnInit, Input } from '@angular/core';
import { IActionLog } from '@Features/action-log/interfaces/action-log.interface';
import CustomStore from 'devextreme/data/custom_store';
import { TypeHeaderFilterItems } from './data/type-header-filter-items.data';
import { IDxHeaderFilterItem } from '@Interfaces/devextreme-header-filter-item.interface';

@Component({
    selector: 'action-log-list-table',
    templateUrl: './list-table.component.html',
    styleUrls: ['./list-table.component.scss']
})
export class ListTableComponent implements OnInit {

    @Input()
    public logs: IActionLog[];

    @Input()
    public dataSource: CustomStore;

    public typeHeaderFilterItems: IDxHeaderFilterItem[] = TypeHeaderFilterItems;

    constructor () {}

    public ngOnInit (): void {}

}
