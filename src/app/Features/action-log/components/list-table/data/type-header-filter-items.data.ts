import { IDxHeaderFilterItem } from '@Interfaces/devextreme-header-filter-item.interface';
import { ActionLogTypeMap } from '@Features/action-log/shared/action-log-type.map';

export const TypeHeaderFilterItems: IDxHeaderFilterItem[] = [...ActionLogTypeMap.values()]
    .map((logType) => {
        return <IDxHeaderFilterItem>{
            text: logType.name,
            value: logType.id
        };
    });
