import { ActionLogTypeEnum } from '../shared/action-log-type.enum';

export interface IActionLog {
    id: number;
    userName: string;
    ip: string;
    type: ActionLogTypeEnum;
    info: string;
    date: string;
}
