export interface IActionLogType {
    id: number;
    name: string;
}
