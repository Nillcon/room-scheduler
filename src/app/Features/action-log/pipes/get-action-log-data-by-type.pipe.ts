
import { PipeTransform, Pipe } from '@angular/core';
import { ActionLogTypeEnum } from '../shared/action-log-type.enum';
import { ActionLogTypeMap } from '../shared/action-log-type.map';
import { IActionLogType } from '../interfaces/action-log-type.interface';

@Pipe({
    name: 'getActionLogDataByType'
})
export class GetActionLogDataByTypePipe implements PipeTransform {

    constructor () {}

    public transform (type: ActionLogTypeEnum): IActionLogType {
        return ActionLogTypeMap.get(type);
    }

}
