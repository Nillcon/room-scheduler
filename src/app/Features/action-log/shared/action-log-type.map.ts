import { IActionLogType } from '../interfaces/action-log-type.interface';
import { ActionLogTypeEnum } from './action-log-type.enum';

export const ActionLogTypeMap: Map<ActionLogTypeEnum, IActionLogType> = new Map<ActionLogTypeEnum, IActionLogType>()
    .set(
        ActionLogTypeEnum.CreateUser,
        {
            id: ActionLogTypeEnum.CreateUser,
            name: 'Create user'
        }
    )
    .set(
        ActionLogTypeEnum.EditUser,
        {
            id: ActionLogTypeEnum.EditUser,
            name: 'Edit user'
        }
    )
    .set(
        ActionLogTypeEnum.DeleteUser,
        {
            id: ActionLogTypeEnum.DeleteUser,
            name: 'Delete user'
        }
    )
    .set(
        ActionLogTypeEnum.CreateEventRequest,
        {
            id: ActionLogTypeEnum.CreateEventRequest,
            name: 'Create event request'
        }
    )
    .set(
        ActionLogTypeEnum.EditEventRequest,
        {
            id: ActionLogTypeEnum.EditEventRequest,
            name: 'Edit event request'
        }
    )
    .set(
        ActionLogTypeEnum.DeleteEventRequest,
        {
            id: ActionLogTypeEnum.DeleteEventRequest,
            name: 'Delete event request'
        }
    )
    .set(
        ActionLogTypeEnum.Login,
        {
            id: ActionLogTypeEnum.Login,
            name: 'Login'
        }
    );
