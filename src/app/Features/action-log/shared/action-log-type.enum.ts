export enum ActionLogTypeEnum {
    CreateUser,
    EditUser,
    DeleteUser,
    CreateEventRequest,
    EditEventRequest,
    DeleteEventRequest,
    Login
}
