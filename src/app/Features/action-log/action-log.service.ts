import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { IActionLog } from '@Features/action-log';
import DevExpress from 'devextreme/bundles/dx.all';
import { ActionLogApiService } from './action-log.api.service';

@Injectable()
export class ActionLogService {

    constructor (
        private actionLogApiService: ActionLogApiService
    ) {}

    public getAllWithOData (options: DevExpress.data.LoadOptions): Observable<IActionLog[]> {
        return this.actionLogApiService.getAllWithOData(options);
    }

}
