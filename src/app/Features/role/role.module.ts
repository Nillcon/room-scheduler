import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleApiService } from './role.api.service';
import { SharedModule } from '@App/app-shared.module';
import { AssignerComponent } from './components/assigner/assigner.component';
import { RoleService } from './role.service';
import { SvcAssignerComponent } from './components/svc-assigner/svc-assigner.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        AssignerComponent,
        SvcAssignerComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    exports: [
        AssignerComponent,
        SvcAssignerComponent
    ],
    providers: [
        RoleApiService,
        RoleService
    ]
})
export class RoleModule {}
