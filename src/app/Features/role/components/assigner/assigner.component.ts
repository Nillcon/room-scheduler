import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IsInt } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IUser } from '@Features/user';
import { RoleMap } from '@Features/role/shared/role.map';
import { Observable } from 'rxjs';
import { IRole } from '@Features/role/interfaces/role.interface';
import { RoleEnum } from '@Features/role/shared/role.enum';

@ClassValidation()
@Component({
    selector: 'role-assigner',
    templateUrl: './assigner.component.html',
    styleUrls: ['./assigner.component.scss']
})
export class AssignerComponent implements OnInit {

    @Input()
    @IsInt()
    public roleId: number;

    @Input()
    public autocompleteUpdateFunction: (phrase?: string) => Observable<Partial<IUser>[]>;

    @Input()
    public assignedUsers: Partial<IUser>[];

    @Output()
    public OnChangeServicesButtonClick: EventEmitter<IRole> = new EventEmitter();

    @Output()
    public OnUserAssign: EventEmitter<IUser> = new EventEmitter();

    @Output()
    public OnUserDeassign: EventEmitter<IUser> = new EventEmitter();

    @Output()
    public OnClearAssign: EventEmitter<number> = new EventEmitter();

    @Output()
    public OnFocusIn: EventEmitter<any> = new EventEmitter();

    @Output()
    public OnKeyUp: EventEmitter<any> = new EventEmitter();

    public role: IRole;

    public isSvcButtonVisible: boolean = false;

    constructor () {}

    public ngOnInit (): void {
        this.initRole();
    }

    private initRole (): void {
        this.role = RoleMap.get(this.roleId);
    }

}
