import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IsNotEmptyObject, IsArray } from 'class-validator';
import { ISvc } from '@Features/svc';
import { Observable } from 'rxjs';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IRole } from '@Features/role/interfaces/role.interface';

@ClassValidation()
@Component({
    selector: 'role-svc-assigner',
    templateUrl: './svc-assigner.component.html',
    styleUrls: ['./svc-assigner.component.scss']
})
export class SvcAssignerComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public role: IRole;

    @Input()
    @IsArray()
    public autocompleteSvcArray: ISvc[];

    @Input()
    public assignedSvcArray: Partial<number>[];

    @Output()
    public OnSvcAssign: EventEmitter<number> = new EventEmitter();

    @Output()
    public OnSvcDeassign: EventEmitter<number> = new EventEmitter();

    @Output()
    public OnClearAssign: EventEmitter<number> = new EventEmitter();

    @Output()
    public OnFocusIn: EventEmitter<any> = new EventEmitter();

    @Output()
    public OnKeyUp: EventEmitter<any> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
