import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { IUser } from '@Features/user';
import { map } from 'rxjs/operators';
import { RoleEnum } from '@Features/role';
import { RoleApiService } from './role.api.service';

@Injectable()
export class RoleService {

    constructor (
        private roleApiService: RoleApiService
    ) {}

    public getUsersByAllRoles (): Observable<Map<RoleEnum, Partial<IUser>[]>> {
        return this.roleApiService.getUsersByAllRoles()
            .pipe(
                map((roles: object) => {
                    const roleEntries = Object.entries(roles);
                    return new Map<RoleEnum, Partial<IUser>[]>(<any> roleEntries);
                })
            );
    }

    public getServices (roleId: number): Observable<number[]> {
        return this.roleApiService.getServices(roleId);
    }

    public assignUser (roleId: number, user: Partial<IUser>): Observable<any> {
        return this.roleApiService.assignUser(roleId, user);
    }

    public assignService (roleId: number, svcId: number): Observable<any> {
        return this.roleApiService.assignService(roleId, svcId);
    }

    public deassignUser (roleId: number, user: Partial<IUser>): Observable<any> {
        return this.roleApiService.deassignUser(roleId, user);
    }

    public deassignService (roleId: number, svcId: number): Observable<any> {
        return this.roleApiService.deassignService(roleId, svcId);
    }

    public clearUserAssignments (roleId: number): Observable<any> {
        return this.roleApiService.clearUserAssignments(roleId);
    }

    public clearServiceAssignments (roleId: number): Observable<any> {
        return this.roleApiService.clearServiceAssignments(roleId);
    }

}
