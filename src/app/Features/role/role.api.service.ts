import { Injectable } from "@angular/core";
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { Observable } from 'rxjs';
import { IUser } from '@Features/user';
import { RoleEnum } from '@Features/role';

@Injectable()
export class RoleApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getUsersByAllRoles (): Observable<Map<RoleEnum, Partial<IUser>[]>> {
        return this.httpService.get(`/Roles/Users`);
    }

    public getServices (roleId: number): Observable<number[]> {
        return this.httpService.get(`/Roles/${roleId}/Services`);
    }

    public assignUser (roleId: number, user: Partial<IUser>): Observable<any> {
        return this.httpService.put(`/Roles/${roleId}/Users/${user.login}/Assign`, {});
    }

    public assignService (roleId: number, svcId: number): Observable<any> {
        return this.httpService.put(`/Roles/${roleId}/Services/${svcId}/Assign`, {});
    }

    public deassignUser (roleId: number, user: Partial<IUser>): Observable<any> {
        return this.httpService.put(`/Roles/${roleId}/Users/${user.login}/Deassign`, {});
    }

    public deassignService (roleId: number, svcId: number): Observable<any> {
        return this.httpService.put(`/Roles/${roleId}/Services/${svcId}/Deassign`, {});
    }

    public clearUserAssignments (roleId: number): Observable<any> {
        return this.httpService.put(`/Roles/${roleId}/Users/ClearAssignments`, {});
    }

    public clearServiceAssignments (roleId: number): Observable<any> {
        return this.httpService.put(`/Roles/${roleId}/Services/ClearAssignments`, {});
    }

}
