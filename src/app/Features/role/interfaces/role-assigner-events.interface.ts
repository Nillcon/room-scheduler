import { IUser } from '@Features/user';

interface BaseAssignerEvent {
    roleId: number;
    user: IUser;
}

export interface IOnUserAssignEvent extends BaseAssignerEvent {}

export interface IOnUserDeAssignEvent extends BaseAssignerEvent {}
