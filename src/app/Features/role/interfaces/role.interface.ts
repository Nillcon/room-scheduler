import { RoleEnum } from '@Features/role';

export interface IRole {
    id: RoleEnum;
    name: string;
    isSvcAssignAvailable: boolean;
}
