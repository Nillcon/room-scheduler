export enum RoleEnum {
    SuperUser,
    Banned,
    Police,
    Maintanance,
    Approver,
    Facilities,
    FoodService,
    SeePrivateRooms
}
