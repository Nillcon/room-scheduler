import { IRole } from '@Features/role';
import { RoleEnum } from './role.enum';

export const RoleMap: Map<RoleEnum, IRole> = new Map<RoleEnum, IRole>()
    .set(
        RoleEnum.SuperUser,
        {
            id: RoleEnum.SuperUser,
            name: 'Super User',
            isSvcAssignAvailable: true
        }
    )
    .set(
        RoleEnum.Banned,
        {
            id: RoleEnum.Banned,
            name: 'Banned',
            isSvcAssignAvailable: false
        }
    )
    .set(
        RoleEnum.Police,
        {
            id: RoleEnum.Police,
            name: 'Police',
            isSvcAssignAvailable: true
        }
    )
    .set(
        RoleEnum.Maintanance,
        {
            id: RoleEnum.Maintanance,
            name: 'Maintanance',
            isSvcAssignAvailable: true
        }
    )
    .set(
        RoleEnum.Approver,
        {
            id: RoleEnum.Approver,
            name: 'Approver',
            isSvcAssignAvailable: false
        }
    )
    .set(
        RoleEnum.Facilities,
        {
            id: RoleEnum.Facilities,
            name: 'Facilities',
            isSvcAssignAvailable: true
        }
    )
    .set(
        RoleEnum.FoodService,
        {
            id: RoleEnum.FoodService,
            name: 'Food Service',
            isSvcAssignAvailable: true
        }
    )
    .set(
        RoleEnum.SeePrivateRooms,
        {
            id: RoleEnum.SeePrivateRooms,
            name: 'See Private Rooms',
            isSvcAssignAvailable: false
        }
    );
