import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IRoomGroup } from '@Features/room-group';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';
import { IRoom } from '@Features/room/interfaces/room.interface';

@ClassValidation()
@Component({
    selector: 'room-group-assigner',
    templateUrl: './assigner.component.html',
    styleUrls: ['./assigner.component.scss']
})
export class AssignerComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public roomGroup: IRoomGroup;

    @Input()  public autocompleteRooms: string[]    = [];
    @Input()  public assignedRooms: string[]        = [];

    @Output() public OnRoomAssign: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnRoomDeassign: EventEmitter<IRoom> = new EventEmitter();
    @Output() public OnClearAssign: EventEmitter<number> = new EventEmitter();
    @Output() public OnFocusIn: EventEmitter<any> = new EventEmitter();
    @Output() public OnKeyUp: EventEmitter<any> = new EventEmitter();

    @Output() public OnChangeApprovers: EventEmitter<IRoomGroup> = new EventEmitter();
    @Output() public OnEditButtonClick: EventEmitter<IRoomGroup> = new EventEmitter();
    @Output() public OnDeleteButtonClick: EventEmitter<IRoomGroup> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
