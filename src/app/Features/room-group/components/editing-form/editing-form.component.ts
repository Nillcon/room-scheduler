import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IRoomGroup } from '@Features/room-group';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';

@BaseForm()
@Component({
    selector: 'room-group-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss']
})
export class EditingFormComponent implements OnInit, IBaseForm<Partial<IRoomGroup>> {

    @Input()  public inputData: Partial<IRoomGroup>;
    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRoomGroup>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IRoomGroup>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (roomGroup: IRoomGroup): void {
        this.formGroup = this.formBuilder.group<Partial<IRoomGroup>>({
            name: this.formBuilder.control(
                roomGroup.name || '',
                [Validators.required]
            ),
            isAutoApproved: this.formBuilder.control(roomGroup.isAutoApproved || false)
        });
    }

}
