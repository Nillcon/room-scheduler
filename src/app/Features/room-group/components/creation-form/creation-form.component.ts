import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IRoomGroup } from '@Features/room-group';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { IBaseForm } from '@Core/root/base-form/interfaces/base-form.interface';
import { BaseForm } from '@Core/root/base-form/decorators/base-form.decorator';

@BaseForm()
@Component({
    selector: 'room-group-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss']
})
export class CreationFormComponent implements OnInit, IBaseForm<Partial<IRoomGroup>> {

    @Output() public OnFormInit: EventEmitter<FormGroupTypeSafe<Partial<IRoomGroup>>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Partial<IRoomGroup>>;

    constructor (
        private formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public formGroupInit (): void {
        this.formGroup = this.formBuilder.group<Partial<IRoomGroup>>({
            name: this.formBuilder.control(
                '',
                [Validators.required]
            ),
            isAutoApproved: this.formBuilder.control(false)
        });
    }

}
