import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IUser } from '@Features/user';
import { IRoomGroup } from '@Features/room-group/interfaces/room-group.interface';
import { IsNotEmptyObject, IsArray } from 'class-validator';
import { ClassValidation } from '@Decorators/class-validation.decorator';

@ClassValidation()
@Component({
    selector: 'room-group-approver-assigner',
    templateUrl: './approver-assigner.component.html',
    styleUrls: ['./approver-assigner.component.scss']
})
export class ApproverAssignerComponent implements OnInit {

    @Input()
    @IsNotEmptyObject()
    public roomGroup: IRoomGroup;

    @Input()
    @IsArray()
    public assignedApprovers: Partial<IUser>[] = [];

    @Input()
    public autocompleteApprovers: Partial<IUser>[];

    @Input()
    public autocompleteFunction: Function;

    @Output() public OnUserAssign: EventEmitter<IUser> = new EventEmitter();
    @Output() public OnUserDeassign: EventEmitter<IUser> = new EventEmitter();
    @Output() public OnClearAssign: EventEmitter<number> = new EventEmitter();
    @Output() public OnFocusIn: EventEmitter<any> = new EventEmitter();
    @Output() public OnKeyUp: EventEmitter<any> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
