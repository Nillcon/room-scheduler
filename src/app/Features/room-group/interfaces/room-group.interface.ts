import { IUser } from '@Features/user';

export interface IRoomGroup {
    id: number;
    name: string;
    isAutoApproved: boolean;
    rooms: string[];
    approvers: Partial<IUser>[];
}
