import { Injectable } from "@angular/core";
import { HttpRequestService } from '@Core/root/http/http-request.service';
import { Observable } from 'rxjs';
import { IRoomGroup } from '@Features/room-group';

@Injectable()
export class RoomGroupApiService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getAll (): Observable<IRoomGroup[]> {
        return this.httpService.get(`/RoomsGroups`);
    }

    public get (id: number): Observable<IRoomGroup> {
        return this.httpService.get(`/RoomsGroups/${id}`);
    }

    public create (data: Partial<IRoomGroup>): Observable<IRoomGroup> {
        return this.httpService.post(`/RoomsGroups`, data);
    }

    public edit (id: number, data: Partial<IRoomGroup>): Observable<IRoomGroup> {
        return this.httpService.put(`/RoomsGroups/${id}`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/RoomsGroups/${id}`);
    }

    public assignApprover (roomGroupId: number, approverLogin: string): Observable<any> {
        return this.httpService.put(`/RoomsGroups/${roomGroupId}/AssignApprover`, { value: approverLogin });
    }

    public deAssignApprover (roomGroupId: number, approverLogin: string): Observable<any> {
        return this.httpService.put(`/RoomsGroups/${roomGroupId}/DeassignApprover`, { value: approverLogin });
    }

    public clearAllApproverAssigns (roomGroupId: number): Observable<any> {
        return this.httpService.put(`/RoomsGroups/${roomGroupId}/ClearAllApprovers`, {});
    }

    public assignRoom (roomGroupId: number, roomName: string): Observable<any> {
        return this.httpService.put(`/RoomsGroups/${roomGroupId}/AssignRoom`, { value: roomName });
    }

    public deAssignRoom (roomGroupId: number, roomName: string): Observable<any> {
        return this.httpService.put(`/RoomsGroups/${roomGroupId}/DeassignRoom`, { value: roomName });
    }

    public clearAllRoomAssigns (roomGroupId: number): Observable<any> {
        return this.httpService.put(`/RoomsGroups/${roomGroupId}/ClearAllRooms`, {});
    }

}
