import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomGroupApiService } from './room-group.api.service';
import { RoomGroupService } from './room-group.service';
import { AssignerComponent } from './components/assigner/assigner.component';
import { SharedModule } from '@App/app-shared.module';
import { CreationFormComponent } from './components/creation-form/creation-form.component';
import { EditingFormComponent } from './components/editing-form/editing-form.component';
import { ApproverAssignerComponent } from './components/approver-assigner/approver-assigner.component';
import { FeaturesSharedModule } from '@Features/features-shared.module';

@NgModule({
    declarations: [
        AssignerComponent,
        CreationFormComponent,
        EditingFormComponent,
        ApproverAssignerComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        FeaturesSharedModule
    ],
    exports: [
        AssignerComponent,
        ApproverAssignerComponent,
        CreationFormComponent,
        EditingFormComponent
    ],
    providers: [
        RoomGroupApiService,
        RoomGroupService
    ]
})
export class RoomGroupModule { }
