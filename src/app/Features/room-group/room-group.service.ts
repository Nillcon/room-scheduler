import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { IRoomGroup } from '@Features/room-group';
import { RoomGroupApiService } from './room-group.api.service';

@Injectable()
export class RoomGroupService {

    constructor (
        private roomGroupApiService: RoomGroupApiService
    ) {}

    public getAll (): Observable<IRoomGroup[]> {
        return this.roomGroupApiService.getAll();
    }

    public get (id: number): Observable<IRoomGroup> {
        return this.roomGroupApiService.get(id);
    }

    public create (data: Partial<IRoomGroup>): Observable<IRoomGroup> {
        return this.roomGroupApiService.create(data);
    }

    public edit (id: number, data: Partial<IRoomGroup>): Observable<IRoomGroup> {
        return this.roomGroupApiService.edit(id, data);
    }

    public delete (id: number): Observable<any> {
        return this.roomGroupApiService.delete(id);
    }

    public assignApprover (roomGroupId: number, approverLogin: string): Observable<any> {
        return this.roomGroupApiService.assignApprover(roomGroupId, approverLogin);
    }

    public deAssignApprover (roomGroupId: number, approverLogin: string): Observable<any> {
        return this.roomGroupApiService.deAssignApprover(roomGroupId, approverLogin);
    }

    public clearAllApproverAssigns (roomGroupId: number): Observable<any> {
        return this.roomGroupApiService.clearAllApproverAssigns(roomGroupId);
    }

    public assignRoom (roomGroupId: number, roomName: string): Observable<any> {
        return this.roomGroupApiService.assignRoom(roomGroupId, roomName);
    }

    public deAssignRoom (roomGroupId: number, roomName: string): Observable<any> {
        return this.roomGroupApiService.deAssignRoom(roomGroupId, roomName);
    }

    public clearAllRoomAssigns (roomGroupId: number): Observable<any> {
        return this.roomGroupApiService.clearAllRoomAssigns(roomGroupId);
    }

}
