import { validate } from 'class-validator';

export function ClassValidation (): any {

    return function (targetClass: any): any {

        const ngOnInitUnpatched = targetClass.prototype.ngOnInit;

        targetClass.prototype.ngOnInit = function (this): any {

            validate(this).then(errors => {
                errors.forEach(error => {
                    for (const [decoratorName, errorText] of Object.entries(error.constraints)) {
                        console.error(
                            `%c⚠️ClassValidationError \n\n%c@${decoratorName}()%c\n${error.property} %cproperty\n%c${targetClass.name}%c %c${errorText}`,
                            'font-size: 20px; font-weight: 600;',
                            'color: #9c831e; font-size: 16px; font-weight: 600;',
                            'font-size: 16px; font-weight: 800;',
                            'font-weight: 300;',
                            'background: #9c1e1e; color: white; font-size: 16px; font-weight: 600; border-radius: 6px; padding: 2px 7px;',
                            '',
                            'background: #9c551e; font-size: 16px; color: white; border-radius: 6px; padding: 2px 7px;'
                        );
                    }
                });
            });

            return ngOnInitUnpatched.call(this);
        };

    };

}
