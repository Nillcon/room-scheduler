import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { AppConfig } from './app.config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from './app-shared.module';
import { FeaturesSharedModule } from '@App/Features/features-shared.module';
import { FeaturesModule } from '@Features/features.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AngularBootstrapToastsModule } from 'angular-bootstrap-toasts';
import { HttpInterceptorService } from '@Core/root/http/http-interceptor.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,

        SharedModule,
        FeaturesSharedModule,
        FeaturesModule,

        AngularBootstrapToastsModule,

        AppRoutingModule
    ],
    providers: [
        AppConfig,
        HttpInterceptorService,
        { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true },
        {
            provide: HTTP_INTERCEPTORS,
            useExisting: HttpInterceptorService,
            multi: true
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
