import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTooltipModule,
    MatDialogModule,
    MatRippleModule,
    MatListModule,
    MatCheckboxModule,
    MatMenuModule,
    MatChipsModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatBottomSheetModule,
    MatTabsModule
 } from '@angular/material';
import { DxDataGridModule, DxTagBoxModule } from 'devextreme-angular';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FabButtonModule } from '@Modules/UI/fab-button/fab-button.module';
import { ButtonLoaderDirective } from '@Directives/loader-button.directive';
import { ConfirmationWindowModule } from '@Modules/UI/confirmation-window/confirmation-window.module';
import { ContentLoaderModule } from '@Modules/UI/content-loader/content-loader.module';
import { MatDialogHeaderModule } from '@Modules/UI/mat-dialog-header/mat-dialog-header.module';
import { AssignerFieldModule } from '@Modules/UI/assigner-field/assigner-field.module';
import { FormBuilderTypeSafe } from 'form-type-safe';
import { PipesModule } from '@Modules/Pipes/pipes.module';
import { RouterModule } from '@angular/router';

import { AvatarModule, AvatarSource } from 'ngx-avatar';
import { ForbiddenForDirective } from '@Directives/forbidden-for.directive';
import { AvailableForDirective } from '@Directives/available-for.directive';
import { DateRangeModule } from '@Modules/UI/date-range/date-range.module';

const avatarSourcesOrder = [AvatarSource.CUSTOM, AvatarSource.INITIALS];

@NgModule({
    declarations: [
        ForbiddenForDirective,
        AvailableForDirective,
        ButtonLoaderDirective
    ],
    imports: [
        CommonModule,

        PipesModule,
        ReactiveFormsModule,
        RouterModule,

        FabButtonModule,
        DateRangeModule,

        MatMenuModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatDialogModule,
        MatBottomSheetModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatButtonModule,
        MatRippleModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatTabsModule,

        DxDataGridModule,
        DxTagBoxModule,

        AvatarModule.forRoot({
            sourcePriorityOrder: avatarSourcesOrder
        }),

        ConfirmationWindowModule,
        ContentLoaderModule,
        MatDialogHeaderModule,
        AssignerFieldModule
    ],
    exports: [
        ForbiddenForDirective,
        AvailableForDirective,
        ButtonLoaderDirective,

        PipesModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,

        FabButtonModule,
        DateRangeModule,

        MatMenuModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatDialogModule,
        MatBottomSheetModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatButtonModule,
        MatRippleModule,
        MatListModule,
        MatSelectModule,
        MatSlideToggleModule,
        MatTabsModule,

        DxDataGridModule,
        DxTagBoxModule,

        AvatarModule,

        ConfirmationWindowModule,
        ContentLoaderModule,
        MatDialogHeaderModule,
        AssignerFieldModule
    ],
    providers: [
        FormBuilderTypeSafe
    ]
})
export class SharedModule {}
