import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpInterceptorService } from '@Core/root/http/http-interceptor.service';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationService } from '@Core/root/notification/notification.service';

@AutoUnsubscribe()
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

    public httpErrorSubscription: Subscription;

    constructor (
        private httpInterceptorService: HttpInterceptorService,
        private notificationService: NotificationService
    ) {}

    public ngOnInit (): void {
        this.httpErrorObserver();
    }

    public ngOnDestroy (): void {}

    private httpErrorObserver (): void {
        const errorNotifyTitle: string = 'Error';

        this.httpErrorSubscription = this.httpInterceptorService.OnError$
            .pipe(
                tap((error) => {
                    if (error.status === 500) {
                        this.notificationService.error({
                            title: errorNotifyTitle,
                            text: error.error || error.message,
                            duration: 7000
                        });
                    } else if (error.status === 422) {
                        this.notificationService.warning({
                            title: errorNotifyTitle,
                            text: error.error || error.message,
                            duration: 12000
                        });
                    }
                })
            )
            .subscribe();
    }

}
