export enum StorageKey {
    Access_Token   = 'access_token',
    Debug_Host     = 'debug_host',
    Selected_Space = 'selected_space',

    Event_Page_Opened_Tab = 'event_page_opened_tab',
    User_Page_Opened_Tab = 'user_page_opened_tab',
    Rooms_Page_Opened_Tab = 'rooms_page_opened_tab',

    Is_Fixed_Menu = 'is_fixed_menu'
}
