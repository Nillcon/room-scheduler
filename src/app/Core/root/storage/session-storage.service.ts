import { Injectable } from '@angular/core';
import { SessionKey } from '@App/Core/root/storage/shared/session-key';

@Injectable({
    providedIn: 'root'
})
export class SessionStorageService {
    constructor () {}

    public get<T> (key: SessionKey): T {
        const data = sessionStorage.getItem(key);

        return (data) ? JSON.parse(data) : null;
    }

    public set (key: SessionKey, data: any): void {
        const body = JSON.stringify(data);
        sessionStorage.setItem(key, body);
    }

    public remove (key: SessionKey): void {
        sessionStorage.removeItem(key);
    }

    public clear (): void {
        sessionStorage.clear();
    }
}
