import { Injectable } from '@angular/core';
import { StorageKey } from '@App/Core/root/storage/shared/storage-key';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    constructor () {}

    public get<T> (key: StorageKey): T {
        const data = localStorage.getItem(key);

        return (data) ? JSON.parse(data) : null;
    }

    public set (key: StorageKey, data: any): void {
        const body = JSON.stringify(data);
        localStorage.setItem(key, body);
    }

    public remove (key: StorageKey): void {
        localStorage.removeItem(key);
    }

    public clear (): void {
        localStorage.clear();
    }
}
