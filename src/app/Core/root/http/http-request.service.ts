import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from '../storage/local-storage.service';
import { StorageKey } from '@App/Core/root/storage/shared/storage-key';
import { IHttpOptions } from './interfaces/http-options.interface';

@Injectable({
    providedIn: 'root'
})
export class HttpRequestService {

    constructor (
        private httpService: HttpClient,
        private storageService: LocalStorageService
    ) {
        console.log(`HOST: ${this.host}`);
    }

    public get<T> (way: string): Observable<T> {
        return this.httpService.get<T>(
            this.makeCorrectUrl(way),
            this.httpOptions
        );
    }

    public post<T> (way: string, data: object = {}): Observable<T> {
        return this.httpService.post<T>(
            this.makeCorrectUrl(way),
            data,
            this.httpOptions
        );
    }

    public put<T> (way: string, data: object = {}): Observable<T> {
        return this.httpService.put<T>(
            this.makeCorrectUrl(way),
            data,
            this.httpOptions
        );
    }

    public patch<T> (way: string, data: object = {}): Observable<T> {
        return this.httpService.patch<T>(
            this.makeCorrectUrl(way),
            data,
            this.httpOptions
        );
    }

    public delete (way: string): Observable<boolean> {
        return this.httpService.delete<boolean>(
            this.makeCorrectUrl(way),
            this.httpOptions
        );
    }

    public get host (): string {
        const debugHost: string = this.storageService.get<string>(StorageKey.Debug_Host);
        const host: string   = debugHost || environment.host || location.origin;

        return host;
    }

    public get httpOptions (): IHttpOptions {
        return {
            headers: {
                Authorization: this.token
            }
        };
    }

    public get token (): string {
        const accessToken = this.storageService.get(StorageKey.Access_Token);
        return accessToken ? `Bearer ${this.storageService.get(StorageKey.Access_Token)}` : '';
    }

    public makeCorrectUrl (urlOrSubUrl: string): string {
        const baseUrl: string                   = this.host;
        const isWayIncludesProtocol: boolean    = this.checkProtocolInWay(urlOrSubUrl);

        return (isWayIncludesProtocol) ? urlOrSubUrl : `${baseUrl}${environment.apiRoute}${urlOrSubUrl}`;
    }

    public checkProtocolInWay (way: string): boolean {
        const protocols: string[]   = ['http://', 'https://'];
        let isWayIncludesProtocol: boolean = false;

        for (const currProtocol of protocols) {
            const isWayIncludeCurrProtocol = way.includes(currProtocol);

            if (isWayIncludeCurrProtocol) {
                isWayIncludesProtocol = true;
                break;
            }
        }

        return isWayIncludesProtocol;
    }

}
