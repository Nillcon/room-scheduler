import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { IHttpOptions } from './interfaces/http-options.interface';
import { HttpRequestService } from './http-request.service';

@Injectable({
    providedIn: 'root'
})
export class HttpFileService {

    constructor (
        private httpService: HttpClient,
        private httpRequestService: HttpRequestService
    ) {}

    public downloadFile (way: string): Observable<Blob> {
        const httpOptions: IHttpOptions = this.httpRequestService.httpOptions;
        httpOptions.responseType        = 'blob'     as any;
        httpOptions.observe             = 'response' as any;

        return this.httpService.get(
            this.httpRequestService.makeCorrectUrl(way),
            httpOptions
        )
            .pipe(
                tap((response: HttpResponse<Blob>) => {
                    const fileName = this.getFileNameFromHeaders(response.headers);
                    const fileType = this.getFileTypeFromHeaders(response.headers);

                    this.downloadFileInBrowser(
                        response.body,
                        fileName,
                        fileType
                    );
                }),
                map(response => response.body)
            );
    }

    public uploadFile<T> (way: string, files: Blob | Blob[]): Observable<T> {
        const formData: FormData = new FormData();

        if (Array.isArray(files)) {

            files.forEach((file, index) => {
                formData.append(`file-${index}`, file);
            });

        } else {
            formData.append('file-0', files);
        }

        return this.httpService.post<T>(
            this.httpRequestService.makeCorrectUrl(way),
            formData,
            this.httpRequestService.httpOptions
        );
    }

    public downloadFileInBrowser (file: Blob, fileName: string, fileType: string): void {
        if (typeof(window.navigator.msSaveOrOpenBlob) === 'function') {
            // File download in Edge (Maybe also in IE)
            window.navigator.msSaveOrOpenBlob(file, `${fileName}.${fileType}`);
        } else {
            // File download in Chrome, Firefox and other browsers
            const urlAddresponses = window.URL.createObjectURL(file);
            const a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.href = urlAddresponses;
            a.download = `${fileName}.${fileType}`;
            a.click();
            window.URL.revokeObjectURL(urlAddresponses);
            a.remove();
        }
    }

    private getFileNameFromHeaders (headers: HttpHeaders): string {
        const fileName: string = headers.get('x-filename') || 'File';

        return fileName;
    }

    private getFileTypeFromHeaders (headers: HttpHeaders): string {
        const fileTypesDictionary = {
            'text/comma-separated-values': 'csv',
            'text/xml': 'xml',
            'application/pdf': 'pdf',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx'
        };

        const fileTypeFromHeaders: string = headers.get('Content-Type');

        return fileTypesDictionary[fileTypeFromHeaders];
    }

}
