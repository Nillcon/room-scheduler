import { IBaseForm } from '../interfaces/base-form.interface';

export function BaseForm (): any {

    return function (targetClass: any): any {

        const originalNgOnInitMethod      = (targetClass.prototype as IBaseForm<any>).ngOnInit;
        const originalNgOnDestroyMethod   = (targetClass.prototype as IBaseForm<any>)['ngOnDestroy'];
        const originalFormGroupInitMethod = (targetClass.prototype as IBaseForm<any>).formGroupInit;
        let isInputDataIsset: boolean     = false;

        (targetClass.prototype as IBaseForm<any>).ngOnInit = function (...args): any {
            const _this: IBaseForm<any> = this;

            if (!isInputDataIsset) {
                _this.formGroupInit();
            } else {
                isInputDataIsset = false;
            }

            originalNgOnInitMethod.apply(_this, args);
        };

        targetClass.prototype.__defineSetter__('inputData', function (value: any): any {
            const _this: IBaseForm<any> = this;
            if (value) {
                _this.formGroupInit(value);
                isInputDataIsset = true;
            }
        });

        (targetClass.prototype as IBaseForm<any>).formGroupInit = function (...args): any {
            const _this: IBaseForm<any>     = this;
            const originalMethodResult: any = originalFormGroupInitMethod.apply(_this, args);

            if (!_this.formGroup) {
                console.error(
                    `%c⚠️BaseFormError \n\n%cformGroupInit must create FormGroup instance for formGroup property!`,
                    'font-size: 20px; font-weight: 600;',
                    'background: #9c1e1e; color: white; font-size: 16px; font-weight: 600; border-radius: 6px; padding: 2px 7px;'
                );
            }

            _this.OnFormInit.emit(_this.formGroup);

            return originalMethodResult;
        };

        (targetClass.prototype as IBaseForm<any>)['ngOnDestroy'] = function (...args): any {
            const _this: IBaseForm<any> = this;
            isInputDataIsset = false;
            originalNgOnDestroyMethod.apply(_this, args);
        };

    };

}
