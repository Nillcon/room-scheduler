import { EventEmitter } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';

export interface IBaseForm<T> {
    OnFormInit: EventEmitter<FormGroupTypeSafe<T>>;

    inputData?: T;
    formGroup: FormGroupTypeSafe<T>;

    ngOnInit: Function;
    formGroupInit: Function;
}
