import { RoleEnum } from '@App/Features/role';

export interface IPage {
    id: string;
    name: string;
    iconClass: string;
    roles?: RoleEnum[];
}
