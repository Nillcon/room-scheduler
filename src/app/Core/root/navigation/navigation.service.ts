import { Injectable, OnDestroy, NgZone } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { UrlService } from '../url/url.service';

@AutoUnsubscribe()
@Injectable({
    providedIn: 'root'
})
export class NavigationService implements OnDestroy {

    constructor (
        private zone: NgZone,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private urlService: UrlService
    ) {}

    public ngOnDestroy (): void {}

    public getCurrentUrl (): string {
        return this.router.url;
    }

    public navigate (
        subUrl: string,
        relativeToCurrentPath: boolean = true,
        params: Params = null,
    ): Promise<boolean> {
        return this.zone.run(() => {
            return this.router.navigate(
                [subUrl],
                {
                    relativeTo: (relativeToCurrentPath) ? this.activatedRoute : null,
                    queryParams: params || this.urlService.getParameters()
                }
            );
        });
    }
}
