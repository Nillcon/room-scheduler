import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AngularBootstrapToastsService } from 'angular-bootstrap-toasts';
import { ToastMessageParams, ToastMessage } from 'angular-bootstrap-toasts/lib/Models/toast-message.models';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    private readonly defaultParameters: {} = {
        pauseDurationOnMouseEnter: true,
    };

    private defaultDuration: number = 5000;

    constructor (
        private toasts: AngularBootstrapToastsService
    ) {
        this.toasts.changeDefaultDuration(this.defaultDuration);
    }

    public info ({
        title = 'Info',
        text = '',
        duration = this.defaultDuration
    }): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            toastClass: 'advanced-info-toast'
        });
    }

    public infoToast ({
        title = 'Info',
        text = '',
        duration = this.defaultDuration
    }): ToastMessage {
        const parameters = Object.assign(
            <ToastMessageParams>{
                title: title,
                text: text,
                duration: duration,
                toastClass: 'advanced-info-toast'
            },
            this.defaultParameters
        );

        return this.toasts.showConfirmToast(parameters);
    }

    public success ({
        title = 'Success',
        text = '',
        duration = this.defaultDuration
    }): Observable<boolean> {
        return this.makeNotification({
            title: title || 'Success',
            text: text || 'Data successfully updated!',
            duration: duration,
            toastClass: 'advanced-success-toast'
        });
    }

    public warning ({
        title = 'Success',
        text = '',
        duration = this.defaultDuration
    }): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            toastClass: 'advanced-warning-toast'
        });
    }

    public error ({
        title = 'Error',
        text = '',
        duration = this.defaultDuration
    }): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            toastClass: 'advanced-error-toast'
        });
    }

    public confirm ({
        title = 'Confirm action',
        text = '',
        duration = this.defaultDuration
    }): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            closeButtonClass: 'info-close',
            toolbarClass: 'confirm-toolbar',
            toastClass: 'advanced-base-toast advanced-confirm-toast',
            toolbarItems: {
                actionButton: {
                    text: 'Confirm',
                    visible: true,
                    class: 'yes-confirm-button'
                },
                cancelButton: {
                    text: 'Cancel',
                    visible: true,
                    class: 'no-confirm-button'
                }
            }
        });
    }

    private makeNotification (params: ToastMessageParams): Observable<boolean> {
        const parameters = Object.assign(params, this.defaultParameters);
        const toast = this.toasts.showConfirmToast(parameters);

        return toast.ConfirmationResult$;
    }
}
