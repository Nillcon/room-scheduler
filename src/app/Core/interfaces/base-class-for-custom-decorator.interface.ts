import { Injector } from "@angular/core";

export interface BaseClassForCustomDecorator {
    injector: Injector;
    ngOnInit?: Function;
    ngOnDestroy?: Function;
}
